# ============================================================================
# Title          : A dictionary with value interpolation
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2021-07-26
#
# Description    : See documentation string below.
# ============================================================================

'''
Init module for the unit test package of package ``duipoldict``.
'''


# This turns the ``tests`` directory into a Python package.  This eases the
# discovery of tests.


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
