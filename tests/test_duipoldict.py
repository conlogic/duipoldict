# ============================================================================
# Title          : A dictionary with value interpolation
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2021-12-29
#
# Description    : See documentation string below.
# ============================================================================


'''
Unit tests for module :py:mod:`duipoldict`.
'''


import unittest
import json
import pathlib
import string

import dubasiclog

import examples.persons_std as eps
import examples.persons_alt as epa
import examples.persons_fcts as epf

import duipoldict


# Functions and classes
# =====================


def conv_tuple_to_str_key(key_tuple, key_sep):
    '''
    Convert a flat dictionary key given as tuple `key_tuple` to corresponding
    string key using key separator `key_sep`.
    '''
    if isinstance(key_tuple, tuple):
        k_str = ''
        for e in key_tuple:
            if len(k_str) > 0 and key_sep is not None:
                k_str += key_sep
            k_str += str(e)
    else:
        k_str = key_tuple

    return k_str


def conv_nested_to_flat_tuple_dict(nest_dict):
    '''
    :return: nested dictionary `nested_dict` converted to a flat dictionary
             with tuple keys and the same values for corresponding keys.
    '''
    flat_dict = {}
    for k in nest_dict.keys():
        if isinstance(nest_dict[k], dict):
            flat_dict_k = conv_nested_to_flat_tuple_dict(nest_dict[k])
            for flat_k in flat_dict_k.keys():
                new_flat_k = (k,) + flat_k
                flat_dict[new_flat_k] = flat_dict_k[flat_k]
        else:
            flat_dict[(k,)] = nest_dict[k]

    return flat_dict


def conv_nested_to_flat_str_dict(nest_dict, key_sep='.'):
    '''
    :return: nested dictionary `nested_dict` converted to a flat dictionary
             with structured string keys using key separator `key_sep` and the
             same values for corresponding keys.
    '''
    flat_tuple_dict = conv_nested_to_flat_tuple_dict(nest_dict)
    flat_str_dict = {
        conv_tuple_to_str_key(k, key_sep): v
        for (k, v) in flat_tuple_dict.items()
    }

    return flat_str_dict


def as_AbstractValue(dct):
    '''
    If dictionary `dct` represents a :py:class:`AbstractValue` encoded, by
    :py:class:`AbstractValueEncoder`, to be dumped to JSON, `dct` is converted
    back to a respective :py:class:`AbstractValue` object.

    :return: a :py:class:`AbstractValue` encoded by `dct`, if `dct` stems from
             encoding of such an object, and `dct` itself otherwise,
    '''
    if set(dct.keys()) == {'category', 'attrs'}:
        cat_dct = dct['category']
        attrs_dct = dct['attrs']
        if cat_dct == 'Str':
            # Initialization is special for abstract string values.
            obj_for_dct = duipoldict.AbstractStr(attrs_dct['parts'])
        elif cat_dct == 'KeyRef':
            obj_for_dct = duipoldict.AbstractKeyRef(**attrs_dct)
        elif cat_dct == 'FctCallRef':
            obj_for_dct = duipoldict.AbstractFctCallRef(**attrs_dct)
        else:
            obj_for_dct = dct
    else:
        obj_for_dct = dct

    return obj_for_dct


class AbstractValueEncoder(json.JSONEncoder):
    '''
    Variant of :py:class:`JSONEncoder` to handle :py:class:`AbstractValue`s,
    too.
    '''
    def default(self, obj):
        '''
        A variant of :py:meth:`default` extended for
        :py:class:`AbstractValue`s.
        '''
        if isinstance(obj, duipoldict.AbstractValue):
            enc_obj = {'category': obj.category}
            enc_obj['attrs'] = obj.__dict__
        else:
            enc_obj = super().default(obj)

        return enc_obj


def read_json_file(fpath, object_hook=None):
    '''
    Read JSON file with path `fpath` into a dictionary.

    A not-``None`` function `object_hook` is called with the result of any
    object literal decoded as dictionary, and the result of this call will be
    used instead of this literal.

    :return: content of JSON file with path `fpath` read into a dictionary.
    '''
    with open(fpath, 'rt') as fobj:
        content = json.load(fobj, object_hook=object_hook)

    return content


def write_json_file(content, fpath, cls=None, indent=1):
    '''
    Write content `content` to JSON file with path `fpath` using indentation
    level `indent`.

    A not-``None`` class `cls` is used as JSON encoder instead of the standard
    one.
    '''
    dpath = pathlib.Path(fpath).parent
    dpath.mkdir(parents=True, exist_ok=True)

    with open(fpath, 'wt') as fobj:
        json.dump(content, fobj, cls=cls, indent=indent)


def mk_test_data():
    '''
    Create test data for the example.

    :return: list of paths for the created data files.
    '''
    tc = base_duipoldict_TC()
    tc.setUp()
    data_fpaths = []

    # Create test data files.

    # - Test data file with raw values where the respective abstract values
    #   are indexed by their corresponding concrete values in standard syntax.
    con_vals = tc.con_abs_vals_dict.keys()
    parser = duipoldict.StrValueParser(**tc.std_syntax)
    con_abs_vals_dict = {k: parser.parse_value(k, 0) for k in con_vals}
    write_json_file(
        con_abs_vals_dict, tc.con_abs_vals_fpath,
        cls=AbstractValueEncoder)
    data_fpaths.append(tc.con_abs_vals_fpath)

    # - Test data files for the example data files.
    # -- Flat dictionary with concrete raw values.
    nest_con_std_dict = getattr(eps, tc.persons_dict_name)
    flat_con_std_dict = conv_nested_to_flat_str_dict(nest_con_std_dict)
    write_json_file(
        flat_con_std_dict, tc.ex_flat_con_fpath)
    data_fpaths.append(tc.ex_flat_con_fpath)
    # -- Nested dictionary with abstract raw values.
    std_parser = duipoldict.ValueParser(**tc.std_syntax)
    nest_abs_persons_dict = std_parser.parse_value(nest_con_std_dict)
    write_json_file(
        nest_abs_persons_dict, tc.ex_nest_abs_fpath,
        cls=AbstractValueEncoder)
    data_fpaths.append(tc.ex_nest_abs_fpath)
    # -- Flat dictionary with abstract raw values.
    flat_abs_persons_dict = conv_nested_to_flat_str_dict(
        nest_abs_persons_dict)
    write_json_file(
        flat_abs_persons_dict, tc.ex_flat_abs_fpath,
        cls=AbstractValueEncoder)
    data_fpaths.append(tc.ex_nest_abs_fpath)
    # -- Nested dictionary with expanded values.
    ipd = duipoldict.InterpolDict(
        nest_con_std_dict, fcts=epf.PERSONS_FUNCTIONS)
    nest_exp_persons_dict = dict(ipd.expand_value(ipd.data))
    write_json_file(nest_exp_persons_dict, tc.ex_nest_exp_fpath)
    data_fpaths.append(tc.ex_nest_exp_fpath)
    # -- Flat dictionary with expanded values.
    flat_exp_persons_dict = conv_nested_to_flat_str_dict(
        nest_exp_persons_dict)
    write_json_file(flat_exp_persons_dict, tc.ex_flat_exp_fpath)
    data_fpaths.append(tc.ex_flat_exp_fpath)

    # (Re)Write example data files.
    # - Standard syntax.
    tc._write_persons_data(
        nest_abs_persons_dict, tc.ex_std_fpath, 'standard',
        **tc.std_syntax)
    data_fpaths.append(tc.ex_std_fpath)
    # - Alternative syntax.
    tc._write_persons_data(
        nest_abs_persons_dict, tc.ex_alt_fpath, 'alternative',
        **tc.alt_syntax)
    data_fpaths.append(tc.ex_alt_fpath)

    return data_fpaths


class AbstractValue_eq_TC(unittest.TestCase):
    '''
    Tests for the :py:meth:`__eq__` method of :py:class:`AbstractValue`
    objects.
    '''


    def test_oth_type_rhs(self):
        '''
        Test with a different type on the right-hand side.
        '''
        abs_str = duipoldict.AbstractStr('foo')
        self.assertFalse(abs_str == 'foo')


    def test_oth_type_lhs(self):
        '''
        Test with a different type on the left-hand side.
        '''
        abs_str = duipoldict.AbstractStr('foo')
        self.assertFalse('foo' == abs_str)


    def test_diff_abs_val_types(self):
        '''
        Test with different abstract value types.
        '''
        abs_str = duipoldict.AbstractStr('foo')
        abs_keyRef = duipoldict.AbstractKeyRef(['foo'])
        self.assertFalse(abs_str == abs_keyRef)


    def test_diff_vals1(self):
        '''
        Test with both :py:class:`AbstractFctCallRef`, but different function
        names.
        '''
        abs_fctCallRef1 = duipoldict.AbstractFctCallRef('foo', [])
        abs_fctCallRef2 = duipoldict.AbstractFctCallRef('bar', [])
        self.assertFalse(abs_fctCallRef1 == abs_fctCallRef2)


    def test_diff_vals2(self):
        '''
        Test with both :py:class:`AbstractFctCallRef`, but different argument
        lists.
        '''
        abs_fctCallRef1 = duipoldict.AbstractFctCallRef('foo', ['bar'])
        abs_fctCallRef2 = duipoldict.AbstractFctCallRef('foo', ['baz'])
        self.assertFalse(abs_fctCallRef1 == abs_fctCallRef2)


    def test_single_eq(self):
        '''
        Test with equal :py:class:`AbstractFctCallRef`s with single argument.
        '''
        abs_fctCallRef1 = duipoldict.AbstractFctCallRef('foo', ['bar'])
        abs_fctCallRef2 = duipoldict.AbstractFctCallRef('foo', ['bar'])
        self.assertTrue(abs_fctCallRef1 == abs_fctCallRef2)


    def test_flat_lists_eq(self):
        '''
        Test with equal flat lists of :py:class:`AbstractValue`s.
        '''
        abs_str = duipoldict.AbstractStr('foo')
        abs_keyRef = duipoldict.AbstractKeyRef(['foo'])
        abs_fctCallRef1 = duipoldict.AbstractFctCallRef('foo', ['bar'])
        abs_fctCallRef2 = duipoldict.AbstractFctCallRef('foo', ['bar'])
        abs_vals_list1 = [abs_str, abs_keyRef, abs_fctCallRef1]
        abs_vals_list2 = [abs_str, abs_keyRef, abs_fctCallRef2]
        self.assertTrue(abs_vals_list1 == abs_vals_list2)


    def test_flat_dicts_eq(self):
        '''
        Test with equal flat dictionaries of :py:class:`AbstractValue`s.
        '''
        abs_str = duipoldict.AbstractStr('foo')
        abs_keyRef = duipoldict.AbstractKeyRef(['foo'])
        abs_fctCallRef1 = duipoldict.AbstractFctCallRef('foo', ['bar'])
        abs_fctCallRef2 = duipoldict.AbstractFctCallRef('foo', ['bar'])
        abs_vals_dict1 = {'a': abs_str, 'b': abs_keyRef, 'c': abs_fctCallRef1}
        abs_vals_dict2 = {'a': abs_str, 'b': abs_keyRef, 'c': abs_fctCallRef2}
        self.assertTrue(abs_vals_dict1 == abs_vals_dict2)


    def test_nested(self):
        '''
        Test with equal nested :py:class:`AbstractValue`s.
        '''
        abs_str = duipoldict.AbstractStr('foo')
        abs_fctCallRef1 = duipoldict.AbstractFctCallRef('foo', ['bar'])
        abs_fctCallRef2 = duipoldict.AbstractFctCallRef('foo', ['bar'])
        abs_str_all1 = duipoldict.AbstractStr([abs_str, abs_fctCallRef1])
        abs_str_all2 = duipoldict.AbstractStr([abs_str, abs_fctCallRef2])
        self.assertTrue(abs_str_all1 == abs_str_all2)


class AbstractValue_repr_TC(unittest.TestCase):
    '''
    Tests for the :py:meth:`__repr__` method of :py:class:`AbstractValue`
    objects.
    '''


    def test_AbstractStr(self):
        '''
        Test with a :py:class:`AbstractStr`.
        '''
        abs_str_orig = duipoldict.AbstractStr(['foo', 'bar'])
        abs_str_repr = f"duipoldict.{repr(abs_str_orig)}"
        abs_str_new = eval(abs_str_repr)
        self.assertEqual(abs_str_new, abs_str_orig)


    def test_AbstractKeyRef(self):
        '''
        Test with a :py:class:`AbstractKeyRef`.
        '''
        abs_keyRef_orig = duipoldict.AbstractKeyRef(['foo', 'bar'])
        abs_keyRef_repr = f"duipoldict.{repr(abs_keyRef_orig)}"
        abs_keyRef_new = eval(abs_keyRef_repr)
        self.assertEqual(abs_keyRef_new, abs_keyRef_orig)


    def test_AbstractFctCallRef(self):
        '''
        Test with a :py:class:`AbstractFctCallRef`.
        '''
        abs_fctCallRef_orig = duipoldict.AbstractFctCallRef(
            "foo", ['bar', 'baz'])
        abs_fctCallRef_repr = f"duipoldict.{repr(abs_fctCallRef_orig)}"
        abs_fctCallRef_new = eval(abs_fctCallRef_repr)
        self.assertEqual(abs_fctCallRef_new, abs_fctCallRef_orig)


    def test_nested(self):
        '''
        Test with a nested :py:class:`AbstractValue`.
        '''
        abs_str_part = duipoldict.AbstractStr('foo')
        abs_fctCallRef = duipoldict.AbstractFctCallRef('foo', ['bar'])
        abs_str_orig = duipoldict.AbstractStr([abs_str_part, abs_fctCallRef])
        abs_str_repr = repr(abs_str_orig).replace(
            'AbstractStr', 'duipoldict.AbstractStr')
        abs_str_repr = abs_str_repr.replace(
            'AbstractFctCallRef', 'duipoldict.AbstractFctCallRef')
        abs_str_new = eval(abs_str_repr)
        self.assertEqual(abs_str_new, abs_str_orig)


class AbstractValue_add_TC(unittest.TestCase):
    '''
    Tests for the :py:meth:`__add__` method of :py:class:`AbstractValue`
    objects.
    '''
    def test_type_error(self):
        '''
        Test with adding type unknown for :py:class:`AbstractStr`
        initialization.
        '''
        lhs = duipoldict.AbstractStr([])
        rhs = 13
        with self.assertRaises(duipoldict.InterpolDictError):
            lhs + rhs


    def test_AbstractStr_AbstractStr(self):
        '''
        Test with adding a :py:class:`AbstractStr` to a
        :py:class:`AbstractStr`.
        '''
        lhs = duipoldict.AbstractStr('foo')
        rhs = duipoldict.AbstractStr('bar')
        res_val = lhs + rhs
        exp_parts = lhs.parts + rhs.parts
        self.assertSequenceEqual(res_val.parts, exp_parts)


    def test_AbstractStr_AbstractKeyRef(self):
        '''
        Test with adding a :py:class:`AbstractKeyRef` to a
        :py:class:`AbstractStr`.
        '''
        lhs = duipoldict.AbstractStr('foo')
        rhs = duipoldict.AbstractKeyRef(['foo', 'bar'])
        res_val = lhs + rhs
        exp_parts = lhs.parts + [rhs]
        self.assertSequenceEqual(res_val.parts, exp_parts)


    def test_AbstractStr_AbstractFctCallRef(self):
        '''
        Test with adding a :py:class:`AbstractFctCallRef` to a
        :py:class:`AbstractStr`.
        '''
        lhs = duipoldict.AbstractStr('foo')
        rhs = duipoldict.AbstractFctCallRef('foo', ['bar'])
        res_val = lhs + rhs
        exp_parts = lhs.parts + [rhs]
        self.assertSequenceEqual(res_val.parts, exp_parts)


    def test_AbstractStr_str(self):
        '''
        Test with adding a string to a :py:class:`AbstractStr`.
        '''
        lhs = duipoldict.AbstractStr('foo')
        rhs = 'baz'
        res_val = lhs + rhs
        exp_parts = lhs.parts + [rhs]
        self.assertSequenceEqual(res_val.parts, exp_parts)


    def test_AbstractStr_list(self):
        '''
        Test with adding a list to a :py:class:`AbstractStr`.
        '''
        lhs = duipoldict.AbstractStr('foo')
        rhs = [duipoldict.AbstractKeyRef(['foo', 'bar']), 'foo']
        res_val = lhs + rhs
        exp_parts = lhs.parts + rhs
        self.assertSequenceEqual(res_val.parts, exp_parts)


    def test_AbstractKeyRef_AbstractStr(self):
        '''
        Test with adding a :py:class:`AbstractStr` to a
        :py:class:`AbstractKeyRef`.
        '''
        lhs = duipoldict.AbstractKeyRef(['foo'])
        rhs = duipoldict.AbstractStr('bar')
        res_val = lhs + rhs
        exp_parts = [lhs] + rhs.parts
        self.assertSequenceEqual(res_val.parts, exp_parts)


    def test_AbstractKeyRef_AbstractKeyRef(self):
        '''
        Test with adding a :py:class:`AbstractKeyRef` to a
        :py:class:`AbstractKeyRef`.
        '''
        lhs = duipoldict.AbstractKeyRef(['foo'])
        rhs = duipoldict.AbstractKeyRef(['foo', 'bar'])
        res_val = lhs + rhs
        exp_parts = [lhs, rhs]
        self.assertSequenceEqual(res_val.parts, exp_parts)


    def test_AbstractKeyRef_AbstractFctCallRef(self):
        '''
        Test with adding a :py:class:`AbstractFctCallRef` to a
        :py:class:`AbstractKeyRef`.
        '''
        lhs = duipoldict.AbstractKeyRef(['foo'])
        rhs = duipoldict.AbstractFctCallRef('foo', ['bar'])
        res_val = lhs + rhs
        exp_parts = [lhs, rhs]
        self.assertSequenceEqual(res_val.parts, exp_parts)


    def test_AbstractKeyRef_str(self):
        '''
        Test with adding a string to a :py:class:`AbstractKeyRef`.
        '''
        lhs = duipoldict.AbstractKeyRef(['foo'])
        rhs = 'baz'
        res_val = lhs + rhs
        exp_parts = [lhs, rhs]
        self.assertSequenceEqual(res_val.parts, exp_parts)


    def test_AbstractKeyRef_list(self):
        '''
        Test with adding a list to a :py:class:`AbstractKeyRef`.
        '''
        lhs = duipoldict.AbstractKeyRef(['foo'])
        rhs = [duipoldict.AbstractKeyRef(['foo', 'bar']), 'foo']
        res_val = lhs + rhs
        exp_parts = [lhs] + rhs
        self.assertSequenceEqual(res_val.parts, exp_parts)


    def test_AbstractFctCallRef_AbstractStr(self):
        '''
        Test with adding a :py:class:`AbstractStr` to a
        :py:class:`AbstractFctCallRef`.
        '''
        lhs = duipoldict.AbstractFctCallRef('baz', ['foo'])
        rhs = duipoldict.AbstractStr('bar')
        res_val = lhs + rhs
        exp_parts = [lhs] + rhs.parts
        self.assertSequenceEqual(res_val.parts, exp_parts)


    def test_AbstractFctCallRef_AbstractKeyRef(self):
        '''
        Test with adding a :py:class:`AbstractKeyRef` to a
        :py:class:`AbstractFctCallRef`.
        '''
        lhs = duipoldict.AbstractFctCallRef('baz', ['foo'])
        rhs = duipoldict.AbstractKeyRef(['foo', 'bar'])
        res_val = lhs + rhs
        exp_parts = [lhs, rhs]
        self.assertSequenceEqual(res_val.parts, exp_parts)


    def test_AbstractFctCallRef_AbstractFctCallRef(self):
        '''
        Test with adding a :py:class:`AbstractFctCallRef` to a
        :py:class:`AbstractFctCallRef`.
        '''
        lhs = duipoldict.AbstractFctCallRef('baz', ['foo'])
        rhs = duipoldict.AbstractFctCallRef('foo', ['bar'])
        res_val = lhs + rhs
        exp_parts = [lhs, rhs]
        self.assertSequenceEqual(res_val.parts, exp_parts)


    def test_AbstractFctCallRef_str(self):
        '''
        Test with adding a string to a :py:class:`AbstractFctCallRef`.
        '''
        lhs = duipoldict.AbstractFctCallRef('baz', ['foo'])
        rhs = 'baz'
        res_val = lhs + rhs
        exp_parts = [lhs, rhs]
        self.assertSequenceEqual(res_val.parts, exp_parts)


    def test_AbstractFctCallRef_list(self):
        '''
        Test with adding a list to a :py:class:`AbstractFctCallRef`.
        '''
        lhs = duipoldict.AbstractFctCallRef('baz', ['foo'])
        rhs = [duipoldict.AbstractKeyRef(['foo', 'bar']), 'foo']
        res_val = lhs + rhs
        exp_parts = [lhs] + rhs
        self.assertSequenceEqual(res_val.parts, exp_parts)


class AbstractStr_init_TC(unittest.TestCase):
    '''
    Tests for :py:class:`AbstractStr` initialization.
    '''


    def test_type_error(self):
        '''
        Test with a type unknown for initialization.
        '''
        n = 13
        with self.assertRaises(duipoldict.InterpolDictError):
            duipoldict.AbstractStr(n)


    def test_AbstractStr(self):
        '''
        Test with a :py:class:`AbstractStr`.
        '''
        abs_str = duipoldict.AbstractStr('foo')
        res_val = duipoldict.AbstractStr(abs_str)
        self.assertSequenceEqual(res_val.parts, abs_str.parts)


    def test_AbstractKeyRef(self):
        '''
        Test with a :py:class:`AbstractKeyRef`.
        '''
        abs_keyRef = duipoldict.AbstractKeyRef(['foo', 'bar'])
        res_val = duipoldict.AbstractStr(abs_keyRef)
        self.assertSequenceEqual(res_val.parts, [abs_keyRef])


    def test_AbstractFctCallRef(self):
        '''
        Test with a :py:class:`AbstractFctCallRef`.
        '''
        abs_fctCallRef = duipoldict.AbstractFctCallRef('foo', ['bar'])
        res_val = duipoldict.AbstractStr(abs_fctCallRef)
        self.assertSequenceEqual(res_val.parts, [abs_fctCallRef])


    def test_str(self):
        '''
        Test with a string without value references.
        '''
        s = 'foo'
        res_val = duipoldict.AbstractStr(s)
        self.assertSequenceEqual(res_val.parts, [s])


    def test_list(self):
        '''
        Test with a list.
        '''
        l = [duipoldict.AbstractKeyRef(['foo', 'bar']), 'foo']
        res_val = duipoldict.AbstractStr(l)
        self.assertSequenceEqual(res_val.parts, l)


class AbstractStr_iadd_TC(unittest.TestCase):
    '''
    Tests for :py:meth:`__iadd__` of :py:class:`AbstractStr`.
    '''
    def setUp(self):
        super().setUp()

        # A base abstract string value to be extend.
        # - Parts.
        self.base_parts = ['foo', duipoldict.AbstractKeyRef(['baz'])]
        # - Abstract string value with this parts.
        self.base_abs_str = duipoldict.AbstractStr(self.base_parts)


    def test_type_error(self):
        '''
        Test with adding a type unknown for initialization.
        '''
        rhs = 13
        with self.assertRaises(duipoldict.InterpolDictError):
            self.base_abs_str += rhs


    def test_AbstractStr(self):
        '''
        Test with adding a :py:class:`AbstractStr`.
        '''
        rhs = duipoldict.AbstractStr('foo')
        self.base_abs_str += rhs
        exp_parts = self.base_parts + rhs.parts
        self.assertSequenceEqual(self.base_abs_str.parts, exp_parts)


    def test_AbstractKeyRef(self):
        '''
        Test with adding a :py:class:`AbstractKeyRef`.
        '''
        rhs = duipoldict.AbstractKeyRef(['foo', 'bar'])
        self.base_abs_str += rhs
        exp_parts = self.base_parts + [rhs]
        self.assertSequenceEqual(self.base_abs_str.parts, exp_parts)


    def test_AbstractFctCallRef(self):
        '''
        Test with adding a :py:class:`AbstractFctCallRef`.
        '''
        rhs = duipoldict.AbstractFctCallRef('foo', ['bar'])
        self.base_abs_str += rhs
        exp_parts = self.base_parts + [rhs]
        self.assertSequenceEqual(self.base_abs_str.parts, exp_parts)


    def test_str(self):
        '''
        Test with adding a string without value references..
        '''
        rhs = 'foo'
        self.base_abs_str += rhs
        exp_parts = self.base_parts + [rhs]
        self.assertSequenceEqual(self.base_abs_str.parts, exp_parts)


    def test_list(self):
        '''
        Test with adding a list.
        '''
        rhs = [duipoldict.AbstractKeyRef(['foo', 'bar']), 'foo']
        self.base_abs_str += rhs
        exp_parts = self.base_parts + rhs
        self.assertSequenceEqual(self.base_abs_str.parts, exp_parts)


class base_duipoldict_TC(unittest.TestCase):
    '''
    Base class for tests of for module :py:mod:`duipoldict` using either
    example or test data files.
    '''
    def setUp(self):
        this_dpath = pathlib.Path(__file__).parent
        # Path for directory with examples.
        self.example_dpath = this_dpath.parent / 'examples'
        # Path for directory with test data.
        self.test_data_dpath = this_dpath / 'data'
        # Paths for some example and test data files.
        # - Some abstract raw values indexed by corresponding concrete raw
        #   values.
        self.con_abs_vals_fpath = self.test_data_dpath / 'con_abs_vals.json'
        # - Example - nested dictionary with concrete raw values in standard
        #   syntax.
        self.ex_std_fpath = self.example_dpath / 'persons_std.py'
        # - Example - nested dictionary with concrete raw values in an
        #   alternative syntax.
        self.ex_alt_fpath = self.example_dpath / 'persons_alt.py'
        # - Example - nested dictionary with abstract raw values.
        self.ex_nest_abs_fpath = \
            self.test_data_dpath / 'persons_nest_abs.json'
        # - Example - nested dictionary with expanded values.
        self.ex_nest_exp_fpath = \
            self.test_data_dpath / 'persons_nest_exp.json'
        # - Example - flat dictionary with abstract raw values.
        self.ex_flat_abs_fpath = \
            self.test_data_dpath / 'persons_flat_abs.json'
        # - Example - flat dictionary with concrete raw values.
        self.ex_flat_con_fpath = \
            self.test_data_dpath / 'persons_flat_con.json'
        # - Example - flat dictionary with expanded values.
        self.ex_flat_exp_fpath = \
            self.test_data_dpath / 'persons_flat_exp.json'

        # - Example - template file for example files with a dictionary with
        #   concrete raw values.
        self.ex_tpl_fpath = self.test_data_dpath / 'persons_tpl.txt'

        # Name of the dictionary in the persons example.
        self.persons_dict_name = 'PERSONS_INFO'

        # Dictionaries with test data.
        # - Some abstract raw values indexed by corresponding concrete raw
        #   values.
        self.con_abs_vals_dict = read_json_file(
            self.con_abs_vals_fpath, object_hook=as_AbstractValue)
        # - Example - nested dictionary with concrete raw values in standard
        #   syntax.
        self.ex_nest_con_dict = getattr(eps, self.persons_dict_name)
        # - Example - nested dictionary with concrete raw values in an
        #   alternative syntax.
        self.ex_nest_alt_dict = getattr(epa, self.persons_dict_name)
        # - Example - nested dictionary with expanded values.
        if self.ex_nest_exp_fpath.is_file():
            self.ex_nest_exp_dict = read_json_file(
                self.ex_nest_exp_fpath)
        # - Example - nested dictionary with abstract raw values.
        if self.ex_nest_abs_fpath.is_file():
            self.ex_nest_abs_dict = read_json_file(
                self.ex_nest_abs_fpath, object_hook=as_AbstractValue)
        # - Example - flat dictionary with abstract raw values.
        if self.ex_flat_abs_fpath.is_file():
            self.ex_flat_abs_dict = read_json_file(
                self.ex_flat_abs_fpath, object_hook=as_AbstractValue)
        # - Example - flat dictionary with concrete raw values in standard
        #   syntax.
        if self.ex_flat_con_fpath.is_file():
            self.ex_flat_con_dict = read_json_file(self.ex_flat_con_fpath)
        # - Example - flat dictionary with expanded values.
        if self.ex_flat_exp_fpath.is_file():
            self.ex_flat_exp_dict = read_json_file(self.ex_flat_exp_fpath)

        # Parameters for syntax.
        # # - Standard.
        self.std_syntax = duipoldict.InterpolDict({}).syntax
        # - An alternative.
        self.alt_syntax = {
            'key_beg': '{|', 'key_end': '|}', 'key_sep': '::',
            'fct_beg': '[|', 'fct_end': '|]', 'arg_sep': ';'
        }


    def _write_persons_data(self, abs_vals_dict, data_fpath,
                            variant='standard', **kwargs):
        '''
        Write variant `variant` of the persons' example given by dictionary
        `abs_vals_dict` with abstract raw values to example data file at path
        `data_fpath`.  Keyword arguments from `kwargs` are used to initialize
        the :py:class:`ValueFormatter` used for formatting.
        '''
        syntax = self.std_syntax.copy()
        syntax.update(**kwargs)
        formatter = duipoldict.ValueFormatter(**syntax)
        vals_dict = formatter.format_value(abs_vals_dict)

        vals_dict_str = dubasiclog.pretty_format(vals_dict)
        persons_info = f"{self.persons_dict_name} = {vals_dict_str}"
        tpl_repls = {}
        tpl_repls['variant'] = variant
        tpl_repls['persons_info'] = persons_info

        with open(self.ex_tpl_fpath, 'rt') as tpl_fobj:
            tpl_str_lines = tpl_fobj.readlines()
        tpl_str = ''.join(tpl_str_lines)
        tpl = string.Template(tpl_str)
        vals_file_str = tpl.substitute(tpl_repls)

        with open(data_fpath, 'wt') as data_fobj:
            data_fobj.write(vals_file_str)


class ValueParser_parse_value_TC(base_duipoldict_TC):
    '''
    Tests for :py:meth:`parse_value` of :py:class:`ValueParser`.
    '''


    def test_str_missg_key_beg_start(self):
        '''
        Test for a string value with missing key reference begin delimiter
        starting reference delimiters.
        '''
        con_val = 'foo}%'
        parser = duipoldict.ValueParser(**self.std_syntax)
        with self.assertRaises(duipoldict.MissingRefBegError):
            parser.parse_value(con_val)


    def test_str_missg_fct_beg_start(self):
        '''
        Test for a string value with missing function call reference begin
        delimiter starting reference delimiters.
        '''
        con_val = 'foo)%'
        parser = duipoldict.ValueParser(**self.std_syntax)
        with self.assertRaises(duipoldict.MissingRefBegError):
            parser.parse_value(con_val)


    def test_str_missg_key_beg_middle(self):
        '''
        Test for a string value with missing key reference begin delimiter
        with matching reference delimiters before.
        '''
        con_val = '%(foo arg)%bar}%'
        parser = duipoldict.ValueParser(**self.std_syntax)
        with self.assertRaises(duipoldict.MissingRefBegError):
            parser.parse_value(con_val)


    def test_str_missg_fct_beg_middle(self):
        '''
        Test for a string value with missing function call reference begin
        delimiter with matching reference delimiters before.
        '''
        con_val = '%{foo}%bar)%'
        parser = duipoldict.ValueParser(**self.std_syntax)
        with self.assertRaises(duipoldict.MissingRefBegError):
            parser.parse_value(con_val)


    def test_str_missg_key_end(self):
        '''
        Test for a string value with a missing key reference end delimiter.
        '''
        con_val = '%{foo'
        parser = duipoldict.ValueParser(**self.std_syntax)
        with self.assertRaises(duipoldict.MissingRefEndsError):
            parser.parse_value(con_val)


    def test_str_missg_fct_end(self):
        '''
        Test for a string value with a missing function call reference end
        delimiter.
        '''
        con_val = '%(foo bar, baz'
        parser = duipoldict.ValueParser(**self.std_syntax)
        with self.assertRaises(duipoldict.MissingRefEndsError):
            parser.parse_value(con_val)


    def test_str_missg_key_fct_ends(self):
        '''
        Test for a string value with a missing key reference end
        delimiter and a missing function call reference end delimiter.
        '''
        con_val = '%(foo bar, %{baz})'
        parser = duipoldict.ValueParser(**self.std_syntax)
        with self.assertRaises(duipoldict.MissingRefEndsError):
            parser.parse_value(con_val)


    def test_str_missg_fct_ends(self):
        '''
        Test for a string value with multiple missing function call reference
        end delimiters.
        '''
        con_val = '%(foo1 %{bar1}%, baz %(foo2)% %(foo3 %{bar3}%'
        parser = duipoldict.ValueParser(**self.std_syntax)
        with self.assertRaises(duipoldict.MissingRefEndsError):
            parser.parse_value(con_val)


    def test_str_fct_beg_key_end(self):
        '''
        Test for a string value with function call reference begin delimiter
        and a key reference end delimiter.
        '''
        con_val = '%(foo}%'
        parser = duipoldict.ValueParser(**self.std_syntax)
        with self.assertRaises(duipoldict.MismatchRefError):
            parser.parse_value(con_val)


    def test_str_key_beg_fct_end(self):
        '''
        Test for a string value with key reference begin delimiter and a
        function call reference end delimiter.
        '''
        con_val = '%{foo)%'
        parser = duipoldict.ValueParser(**self.std_syntax)
        with self.assertRaises(duipoldict.MismatchRefError):
            parser.parse_value(con_val)


    def test_str_key_beg_key_beg(self):
        '''
        Test for a string value with key reference begin delimiter followed by
        a key reference begin delimiter.
        '''
        con_val = '%{foo%{'
        parser = duipoldict.ValueParser(**self.std_syntax)
        with self.assertRaises(duipoldict.NestedKeyRefError):
            parser.parse_value(con_val)


    def test_str_key_beg_fct_beg(self):
        '''
        Test for a string value with key reference begin delimiter followed by
        a function call reference begin delimiter.
        '''
        con_val = '%{foo%('
        parser = duipoldict.ValueParser(**self.std_syntax)
        with self.assertRaises(duipoldict.NestedKeyRefError):
            parser.parse_value(con_val)


    def test_str(self):
        '''
        Test with a string value without value references.
        '''
        con_val = 'foo'
        parser = duipoldict.ValueParser(**self.std_syntax)
        res_abs_val = parser.parse_value(con_val)
        self.assertEqual(res_abs_val, self.con_abs_vals_dict[con_val])


    def test_keyRef1(self):
        '''
        Test with a string value that is a key reference with 1 component.
        '''
        con_val = '%{foo}%'
        parser = duipoldict.ValueParser(**self.std_syntax)
        res_abs_val = parser.parse_value(con_val)
        self.assertEqual(res_abs_val, self.con_abs_vals_dict[con_val])


    def test_keyRef2(self):
        '''
        Test with a string value that is a key reference with 2 components.
        '''
        con_val = '%{foo.bar}%'
        parser = duipoldict.ValueParser(**self.std_syntax)
        res_abs_val = parser.parse_value(con_val)
        self.assertEqual(res_abs_val, self.con_abs_vals_dict[con_val])


    def test_fctCallRef0(self):
        '''
        Test with a string value that is a function call reference without
        arguments.
        '''
        con_val = '%(foo)%'
        parser = duipoldict.ValueParser(**self.std_syntax)
        res_abs_val = parser.parse_value(con_val)
        self.assertEqual(res_abs_val, self.con_abs_vals_dict[con_val])


    def test_fctCallRef1(self):
        '''
        Test with a string value that is a function call reference with 1
        argument.
        '''
        con_val = '%(foo bar)%'
        parser = duipoldict.ValueParser(**self.std_syntax)
        res_abs_val = parser.parse_value(con_val)
        self.assertEqual(res_abs_val, self.con_abs_vals_dict[con_val])


    def test_fctCallRef2(self):
        '''
        Test with a string value that is a function call reference value with
        2 arguments.
        '''
        con_val = '%(foo bar, baz)%'
        parser = duipoldict.ValueParser(**self.std_syntax)
        res_abs_val = parser.parse_value(con_val)
        self.assertEqual(res_abs_val, self.con_abs_vals_dict[con_val])


    def test_nest_fctCallRef2_with_fctCallRef1_keyRef2(self):
        '''
        Test with a string value that is an outer function call reference with
        a function call reference with 1 argument and a key reference with 2
        components as the 2 arguments.
        '''
        con_val = '%(outer %(inner foo)%, %{bar.baz}%)%'
        parser = duipoldict.ValueParser(**self.std_syntax)
        res_abs_val = parser.parse_value(con_val)
        self.assertEqual(res_abs_val, self.con_abs_vals_dict[con_val])


    def test_nest_fctCallRef1_with_fctCallRef2(self):
        '''
        Test with a string value that is an outer function call reference with
        a function call reference with 2 arguments as the 1 argument.
        '''
        con_val = '%(outer %(inner foo, bar)%)%'
        parser = duipoldict.ValueParser(**self.std_syntax)
        parser.parse_value(con_val)
        res_abs_val = parser.parse_value(con_val)
        self.assertEqual(res_abs_val, self.con_abs_vals_dict[con_val])


    def test_str_keyRef2(self):
        '''
        Test with a string value that is a string without value references
        before a key reference with 2 components.
        '''
        con_val = 'before %{foo.bar}%'
        parser = duipoldict.ValueParser(**self.std_syntax)
        res_abs_val = parser.parse_value(con_val)
        self.assertEqual(res_abs_val, self.con_abs_vals_dict[con_val])


    def test_keyRef2_str(self):
        '''
        Test with a string value that key reference with 2 components before a
        string without value references.
        '''
        con_val = '%{foo.bar}%after'
        parser = duipoldict.ValueParser(**self.std_syntax)
        res_abs_val = parser.parse_value(con_val)
        self.assertEqual(res_abs_val, self.con_abs_vals_dict[con_val])


    def test_str_keyRef2_str(self):
        '''
        Test with a string value that is a string without value references
        before a key reference with 2 components before a string without value
        references.
        '''
        con_val = 'before %{foo.bar}%after'
        parser = duipoldict.ValueParser(**self.std_syntax)
        res_abs_val = parser.parse_value(con_val)
        self.assertEqual(res_abs_val, self.con_abs_vals_dict[con_val])


    def test_str_fctCallRef2(self):
        '''
        Test with a string value that is a string without value references
        before a function call reference with 2 arguments.
        '''
        con_val = 'before%(foo bar, baz)%'
        parser = duipoldict.ValueParser(**self.std_syntax)
        res_abs_val = parser.parse_value(con_val)
        self.assertEqual(res_abs_val, self.con_abs_vals_dict[con_val])


    def test_fctCallRef2_str(self):
        '''
        Test with a string value that is a function call reference with 2
        arguments before a string without value references.
        '''
        con_val = '%(foo bar, baz)% after'
        parser = duipoldict.ValueParser(**self.std_syntax)
        res_abs_val = parser.parse_value(con_val)
        self.assertEqual(res_abs_val, self.con_abs_vals_dict[con_val])


    def test_str_fctCallRef2_str(self):
        '''
        Test with a string value that is a string without value references
        before a function call reference value with 2 arguments before a
        string without value references.
        '''
        con_val = 'before%(foo bar, baz)% after'
        parser = duipoldict.ValueParser(**self.std_syntax)
        res_abs_val = parser.parse_value(con_val)
        self.assertEqual(res_abs_val, self.con_abs_vals_dict[con_val])


    def test_keyRef2_keyRef1(self):
        '''
        Test with a string value that is a key reference with 2 components
        before a key reference with 1 component.
        '''
        con_val = '%{foo.bar}%%{baz}%'
        parser = duipoldict.ValueParser(**self.std_syntax)
        res_abs_val = parser.parse_value(con_val)
        self.assertEqual(res_abs_val, self.con_abs_vals_dict[con_val])


    def test_fctCallRef2_fctCallRef1(self):
        '''
        Test with a string value that is a function call reference with 2
        arguments before a function call reference with 1 argument.
        '''
        con_val = '%(foo bar, baz)%%(next)%'
        parser = duipoldict.ValueParser(**self.std_syntax)
        res_abs_val = parser.parse_value(con_val)
        self.assertEqual(res_abs_val, self.con_abs_vals_dict[con_val])


    def test_keyRef2_fctCallRef2(self):
        '''
        Test with a string value that is a key reference with 2 components
        before a function call reference with 2 arguments.
        '''
        con_val = '%{first.key}%%(foo bar, baz)%'
        parser = duipoldict.ValueParser(**self.std_syntax)
        res_abs_val = parser.parse_value(con_val)
        self.assertEqual(res_abs_val, self.con_abs_vals_dict[con_val])


    def test_fctCallRef2_keyRef2(self):
        '''
        Test with a string value that is function call reference before a key
        reference with 2 arguments.
        '''
        con_val = '%(foo bar, baz)%%{last.key}%'
        parser = duipoldict.ValueParser(**self.std_syntax)
        res_abs_val = parser.parse_value(con_val)
        self.assertEqual(res_abs_val, self.con_abs_vals_dict[con_val])


    def test_str_fctCallRef2_str_keyRef2_str(self):
        '''
        Test with a string value that is a string without value references
        before a function call reference with 2 arguments before a string
        before a key reference with 2 components before a string.
        '''
        con_val = 'before %(foo bar, baz)% between %{last.key}% after'
        parser = duipoldict.ValueParser(**self.std_syntax)
        res_abs_val = parser.parse_value(con_val)
        self.assertEqual(res_abs_val, self.con_abs_vals_dict[con_val])


    def test_dict_value(self):
        '''
        Test with a dictionary value.
        '''
        con_val = self.ex_nest_con_dict['persons']['Rae']
        parser = duipoldict.ValueParser(**self.std_syntax)
        res_abs_val = parser.parse_value(con_val)
        exp_abs_val = self.ex_nest_abs_dict['persons']['Rae']
        self.assertEqual(res_abs_val, exp_abs_val)


    def test_list_of_dicts_value(self):
        '''
        Test with a list of dictionary values.
        '''
        con_val = self.ex_nest_con_dict['persons']['all']
        parser = duipoldict.ValueParser(**self.std_syntax)
        res_abs_val = parser.parse_value(con_val)
        exp_abs_val = self.ex_nest_abs_dict['persons']['all']
        self.assertEqual(res_abs_val, exp_abs_val)


    def test_abs_value(self):
        '''
        Test with an abstract value.
        '''
        parser = duipoldict.ValueParser(**self.std_syntax)
        res_abs_val = parser.parse_value(self.ex_nest_abs_dict)
        self.assertEqual(res_abs_val, self.ex_nest_abs_dict)


    def test_alt_syntax(self):
        '''
        Test with alternative syntax.
        '''
        alt_parser = duipoldict.ValueParser(**self.alt_syntax)
        res_persons_abs = alt_parser.parse_value(self.ex_nest_alt_dict)
        self.assertEqual(res_persons_abs, self.ex_nest_abs_dict)


class ValueFormatter_format_value_TC(base_duipoldict_TC):
    '''
    Tests for :py:meth:`format_value` of :py:class:`ValueFormatter`.
    '''


    def test_no_abstract_val_error(self):
        '''
        Test with a value that is no py:class:`AbstractValue` object.
        '''
        val = 13
        formatter = duipoldict.ValueFormatter(**self.std_syntax)
        with self.assertRaises(duipoldict.InterpolDictError):
            formatter.format_value(val)


    def test_unknown_abstract_val_error(self):
        '''
        Test with a value that is no known py:class:`AbstractValue` object.
        '''
        val = duipoldict.AbstractValue()
        formatter = duipoldict.ValueFormatter(**self.std_syntax)
        with self.assertRaises(duipoldict.InterpolDictError):
            formatter.format_value(val)


    def test_str_part_error(self):
        '''
        Test with an expected :py:class:`AbstractStr` part error.
        '''
        # Create a :py:class:`AbstractStr` object with an erroneous part.
        abstr = duipoldict.AbstractStr('a')
        abstr.parts.append(1)
        formatter = duipoldict.ValueFormatter(**self.std_syntax)
        with self.assertRaises(duipoldict.InterpolDictError):
            formatter.format_value(abstr)


    def test_str(self):
        '''
        Test with a string value without value references.
        '''
        exp_con_val = 'foo'
        formatter = duipoldict.ValueFormatter(**self.std_syntax)
        res_con_val = formatter.format_value(
            self.con_abs_vals_dict[exp_con_val])
        self.assertEqual(res_con_val, exp_con_val)


    def test_keyRef1(self):
        '''
        Test with a key reference value with 1 component.
        '''
        exp_con_val = '%{foo}%'
        formatter = duipoldict.ValueFormatter(**self.std_syntax)
        res_con_val = formatter.format_value(
            self.con_abs_vals_dict[exp_con_val])
        self.assertEqual(res_con_val, exp_con_val)


    def test_keyRef2(self):
        '''
        Test with a key reference value with 2 components.
        '''
        exp_con_val = '%{foo.bar}%'
        formatter = duipoldict.ValueFormatter(**self.std_syntax)
        res_con_val = formatter.format_value(
            self.con_abs_vals_dict[exp_con_val])
        self.assertEqual(res_con_val, exp_con_val)


    def test_fctCallRef0(self):
        '''
        Test with a function call reference value without
        arguments.
        '''
        exp_con_val = '%(foo)%'
        formatter = duipoldict.ValueFormatter(**self.std_syntax)
        res_con_val = formatter.format_value(
            self.con_abs_vals_dict[exp_con_val])
        self.assertEqual(res_con_val, exp_con_val)


    def test_fctCallRef1(self):
        '''
        Test with a function call reference value with 1 argument.
        '''
        exp_con_val = '%(foo bar)%'
        formatter = duipoldict.ValueFormatter(**self.std_syntax)
        res_con_val = formatter.format_value(
            self.con_abs_vals_dict[exp_con_val])
        self.assertEqual(res_con_val, exp_con_val)


    def test_fctCallRef2(self):
        '''
        Test with a function call reference value with 2 arguments.
        '''
        exp_con_val = '%(foo bar, baz)%'
        formatter = duipoldict.ValueFormatter(**self.std_syntax)
        res_con_val = formatter.format_value(
            self.con_abs_vals_dict[exp_con_val])
        self.assertEqual(res_con_val, exp_con_val)


    def test_fctCallRefNested(self):
        '''
        Test with nested function call reference values.
        '''
        exp_con_val = '%(outer %(inner foo)%, %{bar.baz}%)%'
        formatter = duipoldict.ValueFormatter(**self.std_syntax)
        res_con_val = formatter.format_value(
            self.con_abs_vals_dict[exp_con_val])
        self.assertEqual(res_con_val, exp_con_val)


    def test_str_keyRef(self):
        '''
        Test with a string without value references before a key reference
        value.
        '''
        exp_con_val = 'before %{foo.bar}%'
        formatter = duipoldict.ValueFormatter(**self.std_syntax)
        res_con_val = formatter.format_value(
            self.con_abs_vals_dict[exp_con_val])
        self.assertEqual(res_con_val, exp_con_val)


    def test_keyRef_str(self):
        '''
        Test with a string without value references after a key reference
        value.
        '''
        exp_con_val = '%{foo.bar}%after'
        formatter = duipoldict.ValueFormatter(**self.std_syntax)
        res_con_val = formatter.format_value(
            self.con_abs_vals_dict[exp_con_val])
        self.assertEqual(res_con_val, exp_con_val)


    def test_str_keyRef_str(self):
        '''
        Test with a string without value references before and a key
        reference value before a string without value references.
        '''
        exp_con_val = 'before %{foo.bar}%after'
        formatter = duipoldict.ValueFormatter(**self.std_syntax)
        res_con_val = formatter.format_value(
            self.con_abs_vals_dict[exp_con_val])
        self.assertEqual(res_con_val, exp_con_val)


    def test_str_fctCallRef(self):
        '''
        Test with a string without value references before a function call
        reference value.
        '''
        exp_con_val = 'before%(foo bar, baz)%'
        formatter = duipoldict.ValueFormatter(**self.std_syntax)
        res_con_val = formatter.format_value(
            self.con_abs_vals_dict[exp_con_val])
        self.assertEqual(res_con_val, exp_con_val)


    def test_fctCallRef_str(self):
        '''
        Test with a string without value references after a function call
        reference value.
        '''
        exp_con_val = '%(foo bar, baz)% after'
        formatter = duipoldict.ValueFormatter(**self.std_syntax)
        res_con_val = formatter.format_value(
            self.con_abs_vals_dict[exp_con_val])
        self.assertEqual(res_con_val, exp_con_val)


    def test_str_fctCallRef_str(self):
        '''
        Test with a string without value references before a function call
        reference value before a string without value references.
        '''
        exp_con_val = 'before%(foo bar, baz)% after'
        formatter = duipoldict.ValueFormatter(**self.std_syntax)
        res_con_val = formatter.format_value(
            self.con_abs_vals_dict[exp_con_val])
        self.assertEqual(res_con_val, exp_con_val)


    def test_keyRef_keyRef(self):
        '''
        Test with 2 key reference values.
        '''
        exp_con_val = '%{foo.bar}%%{baz}%'
        formatter = duipoldict.ValueFormatter(**self.std_syntax)
        res_con_val = formatter.format_value(
            self.con_abs_vals_dict[exp_con_val])
        self.assertEqual(res_con_val, exp_con_val)


    def test_fctCallRef_fctCallRef(self):
        '''
        Test with 2 function call reference values.
        '''
        exp_con_val = '%(foo bar, baz)%%(next)%'
        formatter = duipoldict.ValueFormatter(**self.std_syntax)
        res_con_val = formatter.format_value(
            self.con_abs_vals_dict[exp_con_val])
        self.assertEqual(res_con_val, exp_con_val)


    def test_keyRef_fctCallRef(self):
        '''
        Test with a key reference and a function call reference
        value.
        '''
        exp_con_val = '%{first.key}%%(foo bar, baz)%'
        formatter = duipoldict.ValueFormatter(**self.std_syntax)
        res_con_val = formatter.format_value(
            self.con_abs_vals_dict[exp_con_val])
        self.assertEqual(res_con_val, exp_con_val)


    def test_fctCallRef_keyRef(self):
        '''
        Test with a function call reference and a key reference
        value.
        '''
        exp_con_val = '%(foo bar, baz)%%{last.key}%'
        formatter = duipoldict.ValueFormatter(**self.std_syntax)
        res_con_val = formatter.format_value(
            self.con_abs_vals_dict[exp_con_val])
        self.assertEqual(res_con_val, exp_con_val)


    def test_str_fctCallRef_str_keyRef_str(self):
        '''
        Test with a string without value references before a function call
        reference, before a string without value references before a key
        reference before a string without value references.
        '''
        exp_con_val = 'before %(foo bar, baz)% between %{last.key}% after'
        formatter = duipoldict.ValueFormatter(**self.std_syntax)
        res_con_val = formatter.format_value(
            self.con_abs_vals_dict[exp_con_val])
        self.assertEqual(res_con_val, exp_con_val)


    def test_dict_value(self):
        '''
        Test with a dictionary value.
        '''
        abs_val = self.ex_nest_abs_dict['persons']['Rae']
        formatter = duipoldict.ValueFormatter(**self.std_syntax)
        res_con_val = formatter.format_value(abs_val)
        exp_con_val = self.ex_nest_con_dict['persons']['Rae']
        self.assertSequenceEqual(res_con_val, exp_con_val)


    def test_list_dict_value(self):
        '''
        Test with a list of dictionary value.
        '''
        abs_val = self.ex_nest_abs_dict['persons']['all']
        formatter = duipoldict.ValueFormatter(**self.std_syntax)
        res_con_val = formatter.format_value(abs_val)
        exp_con_val = self.ex_nest_con_dict['persons']['all']
        self.assertSequenceEqual(res_con_val, exp_con_val)


    def test_alt_syntax(self):
        '''
        Test with alternative syntax.
        '''
        alt_formatter = duipoldict.ValueFormatter(**self.alt_syntax)
        res_con_alt_persons = alt_formatter.format_value(
            self.ex_nest_abs_dict)
        self.assertSequenceEqual(res_con_alt_persons, self.ex_nest_alt_dict)


class InterpolDict_init_TC(base_duipoldict_TC):
    '''
    Tests for :py:class:`InterpolDict` initialization.
    '''


    def test_con_std_data(self):
        '''
        Test with concrete data with standard syntax.
        '''
        ipd_val = duipoldict.InterpolDict(self.ex_nest_con_dict)
        self.assertEqual(ipd_val.data, self.ex_nest_abs_dict)


    def test_abs_data(self):
        '''
        Test with abstract data.
        '''
        ipd_val = duipoldict.InterpolDict(self.ex_nest_abs_dict)
        self.assertEqual(ipd_val.data, self.ex_nest_abs_dict)


    def test_con_alt_data(self):
        '''
        Test with concrete data with alternative syntax.
        '''
        ipd_val = duipoldict.InterpolDict(
            self.ex_nest_alt_dict, **self.alt_syntax)
        self.assertEqual(ipd_val.data, self.ex_nest_abs_dict)


class InterpolDict_get_nested_raw_TC(base_duipoldict_TC):
    '''
    Tests for :py:meth:`_get_nested_raw` of :py:class:`InterpolDict`.
    '''


    def test_key_error(self):
        '''
        Test with missing key from key sequence.
        '''
        data = {'foo': {'bar': 'baz'}}
        ipd_val = duipoldict.InterpolDict(data)
        with self.assertRaises(KeyError):
            ipd_val._get_nested_raw(['foo', 'baz'])


    def test_val_error(self):
        '''
        Test with wrong value type within key sequence.
        '''
        data = {'foo': {'bar': ['baz1', 'baz2']}}
        ipd_val = duipoldict.InterpolDict(data)
        with self.assertRaises(ValueError):
            ipd_val._get_nested_raw(['foo', 'bar', 'baz1'])


    def test_keys0(self):
        '''
        Test with key sequence with 0 components.
        '''
        data = dict()
        ipd_val = duipoldict.InterpolDict(data)
        res_val = ipd_val._get_nested_raw([])
        self.assertIsNone(res_val)


    def test_keys1_to_map(self):
        '''
        Test with key sequence with 1 component and a map value.
        '''
        ipd_val = duipoldict.InterpolDict(self.ex_nest_abs_dict)
        key = 'Rae'
        res_val = ipd_val._get_nested_raw((key,))
        exp_val = self.ex_nest_abs_dict[key]
        self.assertDictEqual(res_val, exp_val)


    def test_keys1_to_FctCallRef(self):
        '''
        Test with key sequence with 1 component and an abstract function call
        reference value.
        '''
        ipd_val = duipoldict.InterpolDict(self.ex_nest_abs_dict)
        key = 'say'
        res_val = ipd_val._get_nested_raw((key,))
        exp_val = self.ex_nest_abs_dict[key]
        self.assertEqual(res_val, exp_val)


    def test_keys2_to_map(self):
        '''
        Test with key sequence with 2 components and a map value.
        '''
        ipd_val = duipoldict.InterpolDict(self.ex_nest_abs_dict)
        key0 = 'persons'
        key1 = 'Rae'
        res_val = ipd_val._get_nested_raw((key0, key1))
        exp_val = self.ex_nest_abs_dict[key0][key1]
        self.assertDictEqual(res_val, exp_val)


    def test_keys2_to_seq(self):
        '''
        Test with key sequence with 2 components and a sequence value.
        '''
        ipd_val = duipoldict.InterpolDict(self.ex_nest_abs_dict)
        key0 = 'persons'
        key1 = 'all'
        res_val = ipd_val._get_nested_raw((key0, key1))
        exp_val = self.ex_nest_abs_dict[key0][key1]
        self.assertSequenceEqual(res_val, exp_val)


    def test_keys3_to_Str(self):
        '''
        Test with key sequence with 3 components and an abstract string value.
        '''
        ipd_val = duipoldict.InterpolDict(self.ex_nest_abs_dict)
        key0 = 'persons'
        key1 = 'Rae'
        key2 = 'id'
        res_val = ipd_val._get_nested_raw((key0, key1, key2))
        exp_val = self.ex_nest_abs_dict[key0][key1][key2]
        self.assertEqual(res_val, exp_val)


class InterpolDict_read_dict_TC(base_duipoldict_TC):
    '''
    Tests for :py:meth:`read_dict` of :py:class:`InterpolDict`.
    '''


    def test_con_std_data(self):
        '''
        Test with concrete data with standard syntax.
        '''
        ipd_dict = duipoldict.InterpolDict({})
        ipd_dict.read_dict(self.ex_nest_con_dict)
        self.assertEqual(ipd_dict.data, self.ex_nest_abs_dict)


    def test_abs_data(self):
        '''
        Test with abstract data.
        '''
        ipd_dict = duipoldict.InterpolDict({})
        ipd_dict.read_dict(self.ex_nest_abs_dict)
        self.assertEqual(ipd_dict.data, self.ex_nest_abs_dict)


class InterpolDict_write_dict_TC(base_duipoldict_TC):
    '''
    Tests for :py:meth:`write_dict` of :py:class:`InterpolDict`.
    '''


    def test_std_std(self):
        '''
        Test with default initialization, with :py:meth:`write_dict`
        defaults.
        '''
        ipd_dict = duipoldict.InterpolDict(self.ex_nest_abs_dict)
        res_dict = ipd_dict.write_dict()
        self.assertDictEqual(res_dict, eps.PERSONS_INFO)


    def test_std_abs(self):
        '''
        Test with default initialization, with :py:meth:`write_dict`
        forcing abstract values.
        '''
        ipd_dict = duipoldict.InterpolDict(self.ex_nest_abs_dict)
        res_dict = ipd_dict.write_dict(con=False)
        self.assertDictEqual(res_dict, self.ex_nest_abs_dict)


    def test_std_con(self):
        '''
        Test with default initialization, with :py:meth:`write_dict`
        forcing concrete values.
        '''
        ipd_dict = duipoldict.InterpolDict(self.ex_nest_abs_dict)
        res_dict = ipd_dict.write_dict(con=True)
        self.assertDictEqual(res_dict, eps.PERSONS_INFO)


    def test_con_std(self):
        '''
        Test with default concrete values, with :py:meth:`write_dict`
        defaults.
        '''
        ipd_dict = duipoldict.InterpolDict(self.ex_nest_abs_dict, con=True)
        res_dict = ipd_dict.write_dict()
        self.assertDictEqual(res_dict, eps.PERSONS_INFO)


    def test_con_abs(self):
        '''
        Test with default concrete values, with :py:meth:`write_dict`
        forcing abstract values.
        '''
        ipd_dict = duipoldict.InterpolDict(self.ex_nest_abs_dict, con=True)
        res_dict = ipd_dict.write_dict(con=False)
        self.assertDictEqual(res_dict, self.ex_nest_abs_dict)


    def test_abs_std(self):
        '''
        Test with default abstract values, with :py:meth:`write_dict`
        defaults.
        '''
        ipd_dict = duipoldict.InterpolDict(self.ex_nest_abs_dict, con=False)
        res_dict = ipd_dict.write_dict()
        self.assertDictEqual(res_dict, self.ex_nest_abs_dict)


    def test_abs_con(self):
        '''
        Test with default abstract values, with :py:meth:`write_dict`
        forcing concrete values.
        '''
        ipd_dict = duipoldict.InterpolDict(self.ex_nest_abs_dict, con=False)
        res_dict = ipd_dict.write_dict(con=True)
        self.assertDictEqual(res_dict, eps.PERSONS_INFO)


class InterpolDict_expand_value_TC(base_duipoldict_TC):
    '''
    Tests for :py:meth:`expand_value` of :py:class:`InterpolDict`.
    '''


    def test_parse_error(self):
        '''
        Test with a raw concatenate value causing a parse error.
        '''
        val = '%{paths.exe'
        ipd = duipoldict.InterpolDict(
            self.ex_nest_con_dict, fcts=epf.PERSONS_FUNCTIONS)
        with self.assertRaises(duipoldict.InterpolDictError):
            ipd.expand_value(val)


    def test_key_error(self):
        '''
        Test with a raw concatenate value causing a key error.
        '''
        val = '%{paths.foo}%'
        ipd = duipoldict.InterpolDict(
            self.ex_nest_con_dict, fcts=epf.PERSONS_FUNCTIONS)
        with self.assertRaises(KeyError):
            ipd.expand_value(val)


    def test_val_error(self):
        '''
        Test with a raw concatenate value causing a value error.
        '''
        val = '%{foo.bar.baz1}%'
        data = {'foo': {'bar': ['baz1', 'baz2']}, 'key': val}
        ipd = duipoldict.InterpolDict(data)
        with self.assertRaises(ValueError):
            ipd.expand_value(val)


    def test_rec_error(self):
        '''
        Test with a raw concatenate value causing an recursion error.
        '''
        val = '%{foo}%'
        data = {'foo': '%{bar}%', 'bar': '%{foo}%'}
        ipd = duipoldict.InterpolDict(data)
        with self.assertRaises(duipoldict.InterpolDictError):
            ipd.expand_value(val)


    def test_missg_fct(self):
        '''
        Test with a function call reference with a missing function.
        '''
        key = 'Rae.name'
        ipd = duipoldict.InterpolDict(self.ex_nest_con_dict, fcts={})
        raw_val = self.ex_flat_abs_dict[key]
        with self.assertRaises(duipoldict.InterpolDictError):
            ipd.expand_value(raw_val)


    def test_wrong_fct_args_nr(self):
        '''
        Test with a function call reference using a wrong number of arguments
        for the called function.
        '''
        val = '%(make_greeting %{tpl}%)%'
        ipd = duipoldict.InterpolDict(
            self.ex_nest_con_dict, fcts=epf.PERSONS_FUNCTIONS)
        with self.assertRaises(duipoldict.InterpolDictError):
            ipd.expand_value(val)


    def test_str_no_ref(self):
        '''
        Test with an raw abstract value without value references.
        '''
        key = 'tpl'
        ipd = duipoldict.InterpolDict(
            self.ex_nest_con_dict, fcts=epf.PERSONS_FUNCTIONS)
        raw_val = self.ex_flat_abs_dict[key]
        res_val = ipd.expand_value(raw_val)
        exp_val = self.ex_flat_exp_dict[key]
        self.assertSequenceEqual(res_val, exp_val)


    def test_list_key_ref(self):
        '''
        Test with an raw abstract list value with key references.
        '''
        key = 'persons.all'
        ipd = duipoldict.InterpolDict(
            self.ex_nest_con_dict, fcts=epf.PERSONS_FUNCTIONS)
        raw_val = self.ex_flat_abs_dict[key]
        res_val = ipd.expand_value(raw_val)
        exp_val = self.ex_flat_exp_dict[key]
        self.assertSequenceEqual(res_val, exp_val)


    def test_str_key_fct_ref(self):
        '''
        Test with an raw abstract string value with key and function call
        references.
        '''
        key = 'Rae.name'
        ipd = duipoldict.InterpolDict(
            self.ex_nest_con_dict, fcts=epf.PERSONS_FUNCTIONS)
        raw_val = self.ex_flat_abs_dict[key]
        res_val = ipd.expand_value(raw_val)
        exp_val = self.ex_flat_exp_dict[key]
        self.assertSequenceEqual(res_val, exp_val)


    def test_dict_key_ref(self):
        '''
        Test with an raw abstract dictionary value with key reference.
        '''
        key = 'persons'
        ipd = duipoldict.InterpolDict(
            self.ex_nest_con_dict, fcts=epf.PERSONS_FUNCTIONS)
        raw_val = self.ex_nest_con_dict[key]
        res_val = ipd.expand_value(raw_val)
        exp_val = self.ex_nest_exp_dict[key]
        self.assertSequenceEqual(res_val, exp_val)


    def test_dict_key_fct_ref(self):
        '''
        Test with an raw abstract dictionary value with key and function call
        references.
        '''
        key = 'Rae'
        ipd = duipoldict.InterpolDict(
            self.ex_nest_con_dict, fcts=epf.PERSONS_FUNCTIONS)
        raw_val = self.ex_nest_con_dict[key]
        res_val = ipd.expand_value(raw_val)
        exp_val = self.ex_nest_exp_dict[key]
        self.assertSequenceEqual(res_val, exp_val)


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
