# ============================================================================
# Title          : Value interpolation for mappings
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2021-05-12
#
# Description    : See documentation string below.
# ============================================================================

'''
Initialization for the ``examples`` subpackage.
'''


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
