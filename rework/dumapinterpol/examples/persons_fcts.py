'''
A simple example how to use ``dumapinterpol``:

Some simple information about persons defined given by a dictionary.

Functions available to work with this info.
'''


def full_name(person):
    '''
    :return: a string representing the full name of the person given by person
             info `person`.
    '''

    return f"{person['forename']} {person['surname']}"


def full_names(persons):
    '''
    :return: list of strings representing the full names of the persons given
             by list `persons` of person info.
    '''

    return [full_name(p) for p in persons]


def make_greeting(tpl, name):
    '''
    :return: a string for a greeting message based on template `tpl` for name
             `name`.
    '''

    return tpl.format(name)


def make_greetings(tpl, names):
    '''
    :return: a list of strings for greeting messages based on template `tpl`
    for every name in list `names`.
    '''

    return [make_greeting(tpl, n) for n in names]


# List of all available functions for persons.
PERSONS_FUNCTIONS = [
    full_name,
    full_names,
    make_greeting,
    make_greetings
]
