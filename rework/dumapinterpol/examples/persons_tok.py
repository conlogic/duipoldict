'''
A simple example how to use ``dumapinterpol``:

Some simple information about persons defined given by a dictionary.

This is the tokenized variant of the data.
'''

import dumapinterpol as dm


# Some tokens used for the example.
persons_erika_tok = dm.KeyRefToken(['persons', 'Erika'])
erika_name_tok = dm.KeyRefToken(['Erika', 'name'])
full_name_erika_tok = dm.FctCallRefToken(
    'full_name',
    [persons_erika_tok]
)
hello_erica_tok = dm.FctCallRefToken(
    'make_greeting',
    [dm.KeyRefToken(['tpl']), erika_name_tok]
)
full_names_tok = dm.FctCallRefToken(
    'full_names',
    [dm.KeyRefToken(['persons', 'all'])]
)
greetings_tok = dm.FctCallRefToken(
    'make_greetings',
    [dm.KeyRefToken(['tpl']), full_names_tok]
)

# Dictionary with persons' information.
PERSONS_INFO = {
    'persons': {
        'Erika': {
            'forename': dm.StrToken(['Erika']),
            'surname': dm.StrToken(['Mustermann']),
            'id': dm.StrToken(['30003'])
        },
        'all': [
            {'forename': dm.StrToken(['Jane']),
             'surname': dm.StrToken(['Doe']),
             'id': dm.StrToken(['10001'])},
            {'forename': dm.StrToken(['Richard']),
             'surname': dm.StrToken(['Roe']),
             'id': dm.StrToken(['20002'])},
            persons_erika_tok
        ]
    },

    'Erika': {
        'name': full_name_erika_tok,
        'hello': hello_erica_tok
    },

    'tpl': dm.StrToken(['Hello, dear {}!']),

    'greetings': greetings_tok
}


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
