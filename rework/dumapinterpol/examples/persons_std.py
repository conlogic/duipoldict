'''
A simple example how to use ``dumapinterpol``:

Some simple information about persons defined given by a dictionary.

This is a variant using the default values for the parameters of
:py:class:`dumapinterpol.MapInterpolation`.
'''


# Dictionary with persons' information.
PERSONS_INFO = {
    'persons': {
        'Erika': {
            'forename': 'Erika',
            'surname': 'Mustermann',
            'id': '30003'
        },
        'all': [
            {'forename': 'Jane',
             'surname': 'Doe',
             'id': '10001'},
            {'forename': 'Richard',
             'surname': 'Roe',
             'id': '20002'},
            "%{persons.Erika}%"
        ]
    },

    'Erika': {
        'name': "%(full_name %{persons.Erika}%)%",
        'hello': "%(make_greeting %{tpl}%, %{Erika.name}%)%"
    },

    'tpl': 'Hello, dear {}!',

    'greetings': "%(make_greetings %{tpl}%, %(full_names %{persons.all}%)%)%"
}


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:

#  LocalWords:  Mustermann
