# ============================================================================
# Title          : Value interpolation for mappings
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2021-07-24
#
# Description    : See documentation string below.
# ============================================================================

'''
Unit tests for module :py:mod:`dumapinterpol`.
'''


import unittest

import examples.persons_fcts as epf
import examples.persons_std as eps
import examples.persons_alt as epa
import examples.persons_tok as ept

from dumapinterpol import (
    MapInterpolation, MapValueTokenizer, InterpolationError)


# Functions and classes
# =====================


def list_value(nr_args):
    '''
    :return: a list with `nr_args` many elements.
    '''

    return [str(i) for i in range(int(nr_args))]


def dict_value(nr_items):
    '''
    :return: a dictionary with `nr_items` many items.
    '''

    return {str(i): str(i + 1) for i in range(int(nr_items))}


class base_dumapinterpol_TC(unittest.TestCase):
    '''
    Base class for tests of :py:mod:`dumapinterpol`.
    '''
    def setUp(self):
        # Alternative values for syntax parameters.
        self.alt_syntax = {
            'key_beg': '{|', 'key_end': '|}', 'key_sep': '::',
            'fct_beg': '[|', 'fct_end': '|]', 'args_sep': ';'
        }

        # Some dictionaries use as mapping test data.
        # - Matching the standard :py:class:`MapInterpolation` object.
        self.std_map = eps.PERSONS_INFO
        # - Matching the alternative :py:class:`MapInterpolation` object.
        self.alt_map = epa.PERSONS_INFO


class base_MapInterpolation_TC(base_dumapinterpol_TC):
    '''
    Base class for tests related to :py:class:`MapInterpolation`.
    '''
    def setUp(self):
        super().setUp()

        # Some :py:class:`MapInterpolation` objects.
        # - Using standard values for its parameters.
        self.StdMapInterpol = MapInterpolation(fcts=epf.PERSONS_FUNCTIONS)
        # - Using alternative values for its parameters.
        self.AltMapInterpol = MapInterpolation(
            fcts=epf.PERSONS_FUNCTIONS, **self.alt_syntax)

        # Dictionary with fully expanded values.
        self.exp_map = {
            'persons': {
                'Erika': {'forename': 'Erika',
                          'surname': 'Mustermann',
                          'id': '30003'},
                'all': [
                    {'forename': 'Jane',
                     'surname': 'Doe',
                     'id': '10001'},
                    {'forename': 'Richard',
                     'surname': 'Roe',
                     'id': '20002'},
                    {'forename': 'Erika',
                     'surname': 'Mustermann',
                     'id': '30003'}
                ]
            },
            'Erika': {
                'name': 'Erika Mustermann',
                'hello': 'Hello, dear Erika Mustermann!'},
            'tpl': 'Hello, dear {}!',
            'greetings': [
                'Hello, dear Jane Doe!',
                'Hello, dear Richard Roe!',
                'Hello, dear Erika Mustermann!'
            ]
        }


class MapInterpolation_interpolate_value_TC(base_MapInterpolation_TC):
    '''
    Tests for method :py:meth:`interpolate_value` of
    :py:class:`MapInterpolation`.
    '''


    def test_key_list_part_error(self):
        val = "beg%{foo}%end"
        mapg = {
            'foo': ["a", "b"],
            'bar': val
        }
        with self.assertRaises(InterpolationError):
            self.StdMapInterpol.interpolate_value(val, mapg)


    def test_key_dict_part_error(self):
        val = "beg%{foo}%end"
        mapg = {
            'foo': {"a": "1", "b": "2"},
            'bar': val
        }
        with self.assertRaises(InterpolationError):
            self.StdMapInterpol.interpolate_value(val, mapg)


    def test_fct_list_part_error(self):
        mi = MapInterpolation(fcts=[list_value])
        val = "beg%(list_value 2)%end"
        mapg = {
            'bar': val
        }
        with self.assertRaises(InterpolationError):
            mi.interpolate_value(val, mapg)


    def test_fct_dict_part_error(self):
        mi = MapInterpolation(fcts=[dict_value])
        val = "beg%(dict_value 2)%end"
        mapg = {
            'bar': val
        }
        with self.assertRaises(InterpolationError):
            mi.interpolate_value(val, mapg)


    def test_depth_error(self):
        val = "beg%{foo}%end"
        mapg = {
            'foo': "%{bar}%",
            'bar': val
        }
        with self.assertRaises(InterpolationError):
            self.StdMapInterpol.interpolate_value(val, mapg)


    def test_fct_call_error(self):
        mi = MapInterpolation(fcts=[list_value])
        val = "beg%(list_value2)%end"
        mapg = {
            'bar': val
        }
        with self.assertRaises(InterpolationError):
            mi.interpolate_value(val, mapg)


    def test_missing_fct_error(self):
        val = "beg%(list_value 2)%end"
        mapg = {
            'bar': val
        }
        with self.assertRaises(InterpolationError):
            self.StdMapInterpol.interpolate_value(val, mapg)


    def test_key_ref_error(self):
        mi = MapInterpolation()
        val = "beg%{foo}%end"
        mapg = {
            'bar': val
        }
        with self.assertRaises(KeyError):
            mi.interpolate_value(val, mapg)


    def test_no_interpol(self):
        val = self.std_map['tpl']
        val_res = self.StdMapInterpol.interpolate_value(val, self.std_map)
        self.assertSequenceEqual(val_res, val)


    def test_key_interpol(self):
        val = self.std_map['persons']['all']
        val_res = self.StdMapInterpol.interpolate_value(val, self.std_map)
        val_exp = self.exp_map['persons']['all']
        self.assertSequenceEqual(val_res, val_exp)


    def test_fct_1arg_interpol(self):
        val = self.std_map['Erika']['name']
        val_res = self.StdMapInterpol.interpolate_value(val, self.std_map)
        val_exp = self.exp_map['Erika']['name']
        self.assertSequenceEqual(val_res, val_exp)


    def test_fct_2args_interpol(self):
        val = self.std_map['Erika']['hello']
        val_res = self.StdMapInterpol.interpolate_value(val, self.std_map)
        val_exp = self.exp_map['Erika']['hello']
        self.assertSequenceEqual(val_res, val_exp)


    def test_fct_nested_interpol(self):
        val = self.std_map['greetings']
        val_res = self.StdMapInterpol.interpolate_value(val, self.std_map)
        val_exp = self.exp_map['greetings']
        self.assertSequenceEqual(val_res, val_exp)


    def test_alt_params_interpol(self):
        val = self.alt_map['greetings']
        val_res = self.AltMapInterpol.interpolate_value(val, self.alt_map)
        val_exp = self.exp_map['greetings']
        self.assertSequenceEqual(val_res, val_exp)


class MapInterpolation_get_TC(base_MapInterpolation_TC):
    '''
    Tests for method :py:meth:`get` of :py:class:`MapInterpolation`.
    '''
    def test_interpol_error(self):
        mapg = {
            'foo': ["a", "b"],
            'bar': "beg%{foo}%end"
        }
        with self.assertRaises(InterpolationError):
            self.StdMapInterpol.get(mapg, 'bar', raw=False)


    def test_raw_no_interpol_error(self):
        mapg = {
            'foo': ["a", "b"],
            'bar': "beg%{foo}%end"
        }
        val_exp = mapg['bar']
        val_res = self.StdMapInterpol.get(mapg, 'bar', raw=True)
        self.assertSequenceEqual(val_res, val_exp)


    def test_raw_val(self):
        key = 'greetings'
        val_res = self.StdMapInterpol.get(self.std_map, key, raw=True)
        val_exp = self.std_map[key]
        self.assertSequenceEqual(val_res, val_exp)


    def test_interpol_val(self):
        key = 'greetings'
        val_res = self.StdMapInterpol.get(self.std_map, key)
        val_exp = self.exp_map[key]
        self.assertSequenceEqual(val_res, val_exp)


class base_MapValueTokenizer_TC(base_dumapinterpol_TC):
    '''
    Base class for :py:class:`MapValueTokenizer`-related tests.
    '''
    def setUp(self):
        super().setUp()

        # Some :py:class:`MapInterpolation` objects.
        # - Using standard values for its parameters.
        self.StdMapInterpol = MapValueTokenizer(fcts=epf.PERSONS_FUNCTIONS)
        # - Using alternative values for its parameters.
        self.AltMapInterpol = MapValueTokenizer(
            fcts=epf.PERSONS_FUNCTIONS, **self.alt_syntax)

        # Tokenized example mapping.
        self.tok_map = ept.PERSONS_INFO


class MapValueTokenizer_tokenize_value_TC(base_MapValueTokenizer_TC):
    '''
    Tests for method :py:meth:`tokenize_value` of
    :py:class:`MapValueTokenizer`.
    '''


    def test_std_syntax(self):
        res_toks = self.StdMapInterpol.tokenize_value(
            self.std_map, self.std_map)
        self.assertEqual(res_toks, self.tok_map)


    def test_alt_syntax(self):
        res_toks = self.AltMapInterpol.tokenize_value(
            self.alt_map, self.alt_map)
        self.assertEqual(res_toks, self.tok_map)


class MapValueTokenizer_format_value_TC(base_MapValueTokenizer_TC):
    '''
    Tests for method :py:meth:`format_value` of
    :py:class:`MapValueTokenizer`.
    '''


    def test_std_syntax(self):
        res_vals = self.StdMapInterpol.format_value(self.tok_map)
        self.assertEqual(res_vals, self.std_map)


    def test_alt_syntax(self):
        res_vals = self.AltMapInterpol.format_value(self.tok_map)
        self.assertEqual(res_vals, self.alt_map)


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
