# ============================================================================
# Title          : Value interpolation for mappings
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2021-07-25
#
# Description    : See documentation string below.
#
# License        : LGPL3 <https://www.gnu.org/licenses/lgpl-3.0.en.html>.
#
# Copyright      : 2021 Dirk Ullrich <dirk.ullrich@posteo.de>
# ============================================================================

'''
Single module for package ``dumapinterpol``.
'''


import abc
import re
import collections as colls

import dubasiclog


# Parameters
# ==========

# Version of this package.
version = '1.0.5'

# Maximal interpolation depth.
MAX_INTERPOLATION_DEPTH = 10


# Functions and classes
# =====================


# For uniform logging we use the module-global logging facilities from package
# ``dubasiclog``.
enable_debug_log = dubasiclog.enable_debug_log
debug_log = dubasiclog.debug_log


class MapValueTransformBase(abc.ABC):
    '''
    Abstract base class for transformation of the values of
    :py:class:`Mapping` objects using references for interpolation.  The
    initialization of such an object accepts the following keyword arguments:

    + Not-``None`` strings `key_beg` and `key_end` are used as begin and end
      delimiters for key references.  For such a key reference the value
      associated with the key in the respective key reference is interpolated.
      Such interpolation is used only if both `key_beg` and `key_end` are not
      ``None``.

    + A string `key_sep` is used as separator between keys to concatenate
      single keys to structured keys for referring to values within nested
      mappings.

    + Not-``None`` strings `fct_beg` and `fct_end` are used as begin and end
      delimiters for function call references.  For such a function call
      reference the value produced by the function call in the respective
      function call reference is interpolated.

    + A not-``None`` string `fct_arg_sep` is used as separator between the
      arguments within a function call.  If at least one of these parameters
      is ``None`` no function interpolation is done at all.

    + All functions that can be used in function call references are given
      by list `fcts`.  Every function in `fcts` should be have at least one
      argument.

    The values for `key_beg`, `key_end`, `key_sep`, `fct_beg`. `fct_end` and
    `arg_sep` should be pairwise different.
     '''
    def __init__(self, key_beg='%{', key_end='}%', key_sep='.',
                 fct_beg='%(', fct_end=')%', args_sep=',', fcts=[]):
        '''
        Initialize an interpolation for Mappings.
        '''
        self.key_beg = key_beg
        self.key_end = key_end
        self.key_sep = key_sep
        self.fct_beg = fct_beg
        self.fct_end = fct_end
        self.args_sep = args_sep
        self.fcts = fcts

        # Calculate the regexp to search for key references.
        if key_beg is not None and key_end is not None:
            # We need a non-greedy expression to get the shortest possible
            # string for the key reference itself.
            key_rx = re.escape(self.key_beg)
            key_rx += '(.*?)'
            key_rx += re.escape(self.key_end)
            self._key_rx = re.compile(key_rx)
        else:
            # No interpolation for key references wanted.
            self._key_rx = None

        # Calculate a regexp to search for function call references.  It is
        # only used to identify the next function call begin delimiter, and
        # that somewhere behind at least a function call end delimiter exists,
        # too.
        if self.fct_beg is not None and self.fct_end is not None:
            fct_rx = re.escape(self.fct_beg)
            fct_rx += '.*'
            fct_rx += re.escape(self.fct_end)
            self._fct_rx = re.compile(fct_rx)
        else:
            # No interpolation for function call references wanted.
            self._fct_rx = None

        # Calculate the regexp to separate function arguments with function
        # call references.
        if self.args_sep is not None:
            args_sep_rx = r'\s*'
            args_sep_rx += re.escape(self.args_sep)
            args_sep_rx += r'\s*'
            self._args_sep_rx = re.compile(args_sep_rx)
        else:
            self._args_sep_rx = None
            # For a ``None`` function argument separator the regexp to search
            # for function calls is reset, too.  This way we can use the
            # latter one alone to test whether interpolation for function call
            # references is used or not.
            self._fct_rx = None

        # We also index the functions available for function call references
        # by their names for convenience.
        self._fcts_by_names = {f.__name__: f for f in self.fcts}


    def _transform_value(self, val, mapg, depth):
        '''
        Transform value `val` using mapping `mapg`.

        Number `depth` is the current transformation depth.

        :return: the transformed value.

        :raise: an :py:exc:`InterpolationError` if transformation fails.
        '''
        debug_log(f"---{'--' * depth}raw value---")
        debug_log(str(val))
        debug_log(f"---{'--' * depth}---")
        # Here we distinguish the various types of values we can transform.
        if isinstance(val, str):
            debug_log('->String value.')
            done_val = self._transform_str_value(val, mapg, depth)
        elif isinstance(val, colls.abc.Sequence):
            debug_log('->Sequence value.')
            done_val = self._transform_seq_value(val, mapg, depth)
        elif isinstance(val, colls.abc.Mapping):
            debug_log('->Mapping value.')
            done_val = self._transform_map_value(val, mapg, depth)
        else:
            # Values of other types are not transformed.
            debug_log('->Raw value.')
            done_val = val
        debug_log(f"---{'--' * depth}done value---")
        debug_log(str(done_val))
        debug_log(f"---{'--' * depth}---")

        return done_val


    def _transform_seq_value(self, val, mapg, depth):
        '''
        Transform raw non-string sequence value `val` using mapping `mapg`.

        Number `depth` is the current transformation depth.

        :return: the transformed value.

        :raise: an :py:exc:`InterpolationError` if transformation fails.
        '''
        # For a non-string sequence value we transform its elements in turn,
        # and put them into a list.
        done_val = [self._transform_value(v, mapg, depth) for v in val]

        return done_val


    def _transform_map_value(self, val, mapg, depth):
        '''
        Transform raw mapping value `val` using mapping `mapg`.

        Number `depth` is the current transformation depth.

        :return: the transformed value.

        :raise: an :py:exc:`InterpolationError` if transformation fails.
        '''
        # For a mapping value we transform its values in turn, and associate
        # these transformed values in a dictionary with the original keys.
        done_val = {
            k: self._transform_value(val[k], mapg, depth)
            for k in val.keys()
        }

        return done_val


    def _transform_str_value(self, val, mapg, depth, val_acc=None):
        '''
        Transform raw string value `val` using mapping `mapg`.

        Number `depth` is the current transformation depth.

        Object `val_acc` is used as accumulator for already transformed
        parts of a string value.

        :return: the transformed value.

        :raise: an :py:exc:`InterpolationError` if transformation fails.
        '''
        # Search for the next key reference and function call reference, if
        # the respective reference type is used.
        debug_log(f"-->Accumulated value:\n   {val_acc}")
        if self._key_rx is not None:
            key_ref = self._next_key_ref(val)
        else:
            key_ref = None
        if self._fct_rx is not None:
            fct_ref = self._next_fct_call_ref(val)
        else:
            fct_ref = None

        # Determine the next type of reference to be transformed, if any.
        if key_ref is None and fct_ref is None:
            # Neither reference type found or used -> return value unchanged.
            debug_log('-->Rest done.')
            next_type = None
        elif fct_ref is None:
            # No function call but a key reference found -> next reference to
            # be transformed is a key reference.
            debug_log('-->Key ref next.')
            next_type = 'key'
        elif key_ref is None:
            # No key but a function call reference found -> next reference to
            # be transformed is a function call reference.
            debug_log('-->Function call ref next.')
            next_type = 'fct'
        elif key_ref['beg'] < fct_ref['beg']:
            # Both a key and a function call references found, but the key
            # reference comes first -> next reference to be transformed is a
            # key reference.
            debug_log('-->Key before function call ref next.')
            next_type = 'key'
        elif key_ref['beg'] > fct_ref['beg']:
            # Both a key and a function call references found, but the
            # function call reference comes first -> next reference to be
            # transformed is a function call reference.
            debug_log('-->Function call before key ref next.')
            next_type = 'fct'

        # Depending on the type of next reference - if there is one - isolate
        # the respective reference to be transformed, and do it.
        if next_type is None:
            # No next reference is found in `val` -> transformation is done.
            done_val = self._add_transformed_val(val, val_acc)
        elif next_type == 'key':
            # Next reference found in `val` is a key reference -> interpolate
            # its value in `mapg`.
            next_key = key_ref['key']
            next_val = self._transform_key_ref(next_key, mapg, depth)
            # The reference is enhanced by the transformed value, and the
            # transformation of `val` is continued.
            key_ref['val'] = next_val
            done_val = self._transform_ref_in_str_val(
                val, mapg, key_ref, val_acc, depth)
        elif next_type == 'fct':
            # Next reference found in `val` is a function call reference ->
            # transform the function call's value.
            next_call = fct_ref['call']
            next_val = self._transform_fct_ref(next_call, mapg, depth)
            # The reference is enhanced by the transformed value, and the
            # transformation of `val` is continued.
            fct_ref['val'] = next_val
            done_val = self._transform_ref_in_str_val(
                val, mapg, fct_ref, val_acc, depth)

        return done_val


    def _next_key_ref(self, val):
        '''
        Search next key reference within raw value `val`.

        :return: dictionary characterizing the next key reference found in
                 `val` with the following items:

                 + key ``'beg'`` with the begin index in `rest` of this key
                   reference;
                 + key ``'end'`` with the end index in `rest` of this key
                   reference;
                 + key ``'key'`` with this key reference without delimiters;

                 or ``None`` if no key reference was found in `val`.
        '''
        key_match = re.search(self._key_rx, val)
        if key_match is not None:
            key_found = {
                'beg': key_match.start(),
                'end': key_match.end(),
                'key': key_match.group(1)
            }
        else:
            key_found = None

        return key_found


    def _next_fct_call_ref(self, val):
        '''
        Search next function call reference within raw value `val`.

        :return: dictionary characterizing the next function call reference
                 found in `val` with the following items:

                 + key ``'beg'`` with the begin index in `val` of this
                   function call reference;
                 + key ``'end'`` with the end index in `val` of this function
                   call reference;
                 + key ``'call'`` with this function call string without
                   delimiters;

                or ``None`` if no function call reference was found in `val`.
        '''
        # First we look for a function call reference candidate.  If this
        # succeeds we get the exact function call reference by counting
        # function call begin delimiters not closed yet.
        call_found = None
        fct_match = re.search(self._fct_rx, val)
        if fct_match is not None:
            # We start right after the 1st begin delimiter.
            call_beg = fct_match.start() + len(self.fct_beg)
            val_rest = val[call_beg:]
            # Number of open function call begin delimiters.
            open_beg_count = 1
            while not val_rest == '':
                beg_idx = val_rest.find(self.fct_beg)
                end_idx = val_rest.find(self.fct_end)
                if beg_idx >= 0 and (beg_idx < end_idx or end_idx == -1):
                    # Next function call delimiter is a begin delimiter.
                    open_beg_count += 1
                    val_rest = val_rest[beg_idx + len(self.fct_beg):]
                elif end_idx >= 0 and (end_idx < beg_idx or beg_idx == -1):
                    # Next function call delimiter is an end delimiter.
                    open_beg_count -= 1
                    val_rest = val_rest[end_idx + len(self.fct_end):]
                else:
                    # No function call delimiter found at all.
                    break
                if open_beg_count == 0:
                    # If the function call begin and end delimiters are
                    # balanced we are done.
                    break
                elif open_beg_count < 0:
                    # If the number of function call end delimiters exceeds
                    # the number of function call begin delimiters, the call
                    # is syntactically incorrect.
                    break

            if open_beg_count == 0:
                ref_end = len(val) - len(val_rest)
                call_end = ref_end - len(self.fct_end)
                call_found = {
                    'beg': fct_match.start(),
                    'end': ref_end,
                    'call': val[call_beg:call_end]
                }

        return call_found


    def _transform_ref_in_str_val(self, val, mapg, ref, val_acc, depth):
        '''
        Within the transformation of string value `val` using mapping `mapg`,
        add transformed value up to and including reference `ref` where `ref`
        occurs in `val` and `val_acc` accumulates the already transformed
        value for a part of `val` before that occurrence of `ref` in `val`.

        Number `depth` is the current transformation depth.

        :return: the transformed value.

        :raise: an :py:exc:`InterpolationError` if  fails.
        '''
        if ref['end'] - ref['beg'] == len(val):
            # The whole value `val` consists of the reference `ref` only ->
            # the transformed value of `val` is the transformed value of
            # `ref` reference, and it can be of any type.
            done_val = ref['val']
        else:
            # Otherwise we about to transform a part of a string value -> the
            # value of reference `ref` must be a string, and we add to the
            # already transformed value parts `val_acc` of `val` the new ones
            # from `ref`.
            # - Part of `val` before `ref` not yet accumulated.
            str_before = val[:ref['beg']]
            val_acc = self._add_transformed_val(str_before, val_acc)
            # - Transformed value of `ref`.
            if not isinstance(ref['val'], str):
                raise InterpolationError(
                    f"String value required for reference\n  {ref}")
            val_acc = self._add_transformed_val(ref['val'], val_acc)
            # - Part of `val` after `ref` has yet to be transformed.
            val_after = val[ref['end']:]
            done_val = self._transform_str_value(
                val_after, mapg, depth, val_acc)

        return done_val


    @abc.abstractmethod
    def _add_transformed_val(self, part_val, val_acc):
        '''
        Within the transformation of a string value, add transformed value
        `part_val` of a part of this string to the already transformed value
        for the part the string value before this part accumulated in
        `val_acc`.

        :return: the accumulated transformed value, enhanced by `part_val`.
        '''

        return val_acc


    def _transform_key_ref(self, key, mapg, depth):
        '''
        Transform value referred to by key `key` from mapping `mapg`.
        Number `depth` is the current transformation depth.

        :return: the transformed value.

        :raise: an :py:exc:`InterpolationError` if  fails.
        '''
        debug_log("-->Key '{key}'")
        # Increase depth, and check it.
        depth += 1
        if depth > MAX_INTERPOLATION_DEPTH:
            raise InterpolationError(
                f"Interpolation depth for key reference '{key}' \
                > {MAX_INTERPOLATION_DEPTH}")

        done_val = self._transform_key_val(key, mapg, depth)

        return done_val


    @abc.abstractmethod
    def _transform_key_val(self, key, mapg, depth):
        '''
        Transform value for key `key` from mapping `mapping`.

        Number `depth` is the current transformation depth.

        :return: transformed value for key `key` from mapping `mapping`.
        '''

        return mapg[key]


    def _transform_fct_ref(self, call, mapg, depth):
        '''
        Transform function call reference for function call given by string
        `call` from mapping `mapg`.

        Number `depth` is the current transformation depth.

        :return: the transformed value.

        :raise: an :py:exc:`InterpolationError` if  fails.
        '''
        debug_log(f"-->Function call:\n   {call}")
        # First we split the function call string into the function name and
        # the argument string.
        call_matcher = re.search(r'\s+', call)
        if call_matcher is None:
            raise InterpolationError(
                f"No valid function call '{call}'.")
        fct_str = call[:call_matcher.start()]
        args_str = call[call_matcher.end():]

        # The function to be called must be allowed.
        call_toks = {}
        if fct_str in self._fcts_by_names.keys():
            call_toks['name'] = fct_str
        else:
            # We add the names of available functions to the error message.
            msg = f"Function '{fct_str}' must be one of the following ones:"
            for f in self._fcts_by_names.keys():
                msg += f"\n- '{f}'"
            raise InterpolationError(msg)

        # Next we get the argument expressions.
        call_toks['args'] = re.split(self._args_sep_rx, args_str)

        # Finally we transform the tokenized function call.
        done_val = self._transform_fct_call_token(call_toks, mapg, depth)

        return done_val


    @abc.abstractmethod
    def _transform_fct_call_token(self, call_toks, mapg, depth):
        '''
        Transform function call given by token `call_toks` from mapping
        `mapg`.  Such a tokenized function call is a dictionary with the
        following items:

        + key ``'name'`` with the name of the function;
        + key ``'args'`` with the list of argument expressions given by
          strings.

        Number `depth` is the current transformation depth.

        :return: the transformed value of the function call token.
        '''

        return None


class InterpolationError(Exception):
    '''
    Errors specific for interpolation.
    '''
    def __init__(self, msg, loc=None):
        '''
        Initialize a :py:class:`InterpolationError` with corresponding message
        `msg` and an optional location information `loc`.
        '''
        self.msg = msg
        self.loc = loc


    def __str__(self):
        '''
        :return: printable string representation of the exception.
        '''
        # Since we want to have full control whether the printable string for
        # the exception ends with a ``'.'`` or not we ensure first that there
        # is no ``'.'`` at the end.
        if self.msg.endswith('.'):
            ex_str = self.msg[:-1]
        else:
            ex_str = self.msg
        # When a location is given, we start a new line for it, but do not
        # want a ``'.'`` at the end.  Otherwise we ensure one.
        if self.loc is not None:
            ex_str += f" at:\n  {self.loc}"
        else:
            ex_str += '.'

        return ex_str


class MapInterpolation(MapValueTransformBase):
    '''
    Interpolation for :py:class:`Mapping` objects.

    The initialization of such an object accepts the following keyword
    arguments:

    + Not-``None`` strings `key_beg` and `key_end` are used as begin and end
      delimiters for key references.  For such a key reference the value
      associated with the key in the respective key reference is interpolated.
      Such interpolation is used only if both `key_beg` and `key_end` are not
      ``None``.

    + A string `key_sep` is used as separator between keys to concatenate
      single keys to structured keys for referring to values within nested
      mappings.

    + Not-``None`` strings `fct_beg` and `fct_end` are used as begin and end
      delimiters for function call references.  For such a function call
      reference the value produced by the function call in the respective
      function call reference is interpolated.

    + A not-``None`` string `fct_arg_sep` is used as separator between the
      arguments within a function call.  If at least one of these parameters
      is ``None`` no function interpolation is done at all.

    + All functions that can be used in function call references are given
      by list `fcts`.  Every function in `fcts` should be have at least one
      argument.

    The values for `key_beg`, `key_end`, `key_sep`, `fct_beg`. `fct_end` and
    `arg_sep` should be pairwise different.
    '''


    def get(self, mapg, key, raw=False):
        '''
        Get the value in mapping `mapg` for key `key`.  For a not-``None`` key
        separator of ``self``, `key` can be a nested one.

        :return: the value for `key` in `mapg`, where this value is the raw
                 one if `raw` is ``True``, and interpolated otherwise.

        :raise: a :py:exc:`KeyError` if `key` is no key in `mapg`.

        :raise: an :py:exc:`InterpolationError` if interpolation fails if
                `raw` is ``False``.
        '''
        val = self._get(mapg, key)
        if raw:
            done_val = val
        else:
            done_val = self.interpolate_value(val, mapg)

        return done_val


    def _get(self, mapg, key):
        '''
        Get the value in mapping `mapg` for key `key`.  For a not-``None`` key
        separator of ``self``, `key` can be a nested one.

        :return: the raw value for `key` in `mapg`.

        :raise: a :py:exc:`KeyError` if `key` is no key in `mapg`.
        '''
        if self.key_sep is None:
            val = mapg[key]
        else:
            key_parts = key.split(self.key_sep)
            last_key = key_parts[-1]
            keys_before = key_parts[:-1]
            last_mapg = mapg
            for k in keys_before:
                last_mapg = last_mapg[k]
            val = last_mapg[last_key]

        return val


    def interpolate_value(self, val, mapg):
        '''
        Interpolate raw value `val` using mapping `mapg`.

        :return: the interpolated value, if interpolation is enabled, or the
                 raw value otherwise.

        :raise: an :py:exc:`InterpolationError` if interpolation fails.
        '''
        val = self._transform_value(val, mapg, 0)

        return val


    def _add_transformed_val(self, part_val, val_acc):
        '''
        Add interpolated value `part_val` of a part to the already accumulated
        interpolated value `val_acc`.

        :return: the accumulated interpolated value, enhanced by `part_val`.
        '''
        if val_acc is None:
            val_acc = part_val
        else:
            val_acc += part_val

        return val_acc


    def _transform_key_val(self, key, mapg, depth):
        '''
        Interpolate value for key `key` im mapping `mapg`.

        Number `depth` is the current interpolation depth.

        :return: the interpolated value.
        '''
        # First we determine the raw value for `key`, and then this value
        # itself must be interpolated.
        raw_val = self._get(mapg, key)
        done_val = self._transform_value(raw_val, mapg, depth)

        return done_val


    def _transform_fct_call_token(self, call_toks, mapg, depth):
        '''
        Interpolate the value for function call given tokenized by `call_toks`
        using mapping `mapg`.

        Number `depth` is the current interpolation depth.

        :return: the interpolated value.

        :raise: an :py:exc:`InterpolationError` if interpolation fails.
        '''
        # We get function by its name.
        fct_name = call_toks['name']
        fct = self._fcts_by_names[fct_name]
        # Next we get list of interpolated argument values.
        args_strs = call_toks['args']
        args_vals = self._transform_seq_value(args_strs, mapg, depth)
        # Finally we apply the function to the interpolated argument values.
        done_val = fct(*args_vals)

        return done_val


class MapValueTokenizer(MapValueTransformBase):
    '''
    Tokenizer for the interpolation value of a :py:class:`Mapping`.  The value
    for the initialization are the same like for :py:class:`MapInterpolation`.
    '''
    def tokenize_value(self, val, mapg):
        '''
        Tokenize raw value `val` using mapping `mapg`.

        :return: the tokenized value, i.e. the string components in `val`
                 replaced by the corresponding :py:class:`MapValueToken`
                 element.

        :raise: an :py:exc:`InterpolationError` if tokenization fails.
        '''
        tok = self._transform_value(val, mapg, 0)

        return tok


    def _add_transformed_val(self, tok, acc_tok):
        '''
        Add token `tok` to already accumulated :py:class:`StrToken`
        `acc_toks`, or start a new one, if acc_tok is ``None``.

        :return: the accumulated tokens enhanced by `tok`.
        '''
        if acc_tok is None:
            acc_tok = StrToken([tok])
        else:
            acc_tok.parts.append(tok)

        return acc_tok


    def _transform_key_val(self, key, mapg, depth):
        '''
        Tokenize key `key` for mapping `mapg`.

        Number `depth` is the current interpolation depth.

        :return: a :py:class:`KeyRefToken` representing `key`.
        '''
        # The key is split into its components.
        key_components = key.split(self.key_sep)

        # Now an appropriate key reference token is produced.
        key_tok = KeyRefToken(key_components)

        return key_tok


    def _transform_fct_call_token(self, call_toks, mapg, depth):
        '''
        Tokenize the function call given by token `call_toks`.

        Number `depth` is the current interpolation depth.

        :return: a :py:class:`FctCallRefToken` representing `call_toks`.
        '''
        # The arguments of `call_toks` are tokenized first.
        args_toks = self._transform_value(call_toks['args'], mapg, depth)

        # Now an appropriate function call reference token is created.
        call_tok = FctCallRefToken(call_toks['name'], args_toks)

        return call_tok


    def format_value(self, val_tok):
        '''
        Format tokenized mapping value `val_tok` , where a tokenized mapping
        value is a value of a mapping whose string components are replaced
        by the corresponding :py:class:`MapValueToken` element.

        :return: the raw represented by `val_tok`.

        :raise: an :py:exc:`InterpolationError` if formatting fails.
        '''
        debug_log('---Tokenized value---')
        debug_log(str(val_tok))
        debug_log('---------------------')
        if isinstance(val_tok, colls.abc.Sequence):
            debug_log('->Tokenized sequence value.')
            raw_val = self._format_seq_value(val_tok)
        elif isinstance(val_tok, colls.abc.Mapping):
            debug_log('->Tokenized mapping value.')
            raw_val = self._format_map_value(val_tok)
        elif isinstance(val_tok, MapValueToken):
            debug_log('->Value token.')
            raw_val = self._format_tok_value(val_tok)
        else:
            raise InterpolationError(
                f"No mapping value token\n  {val_tok}")
        debug_log('---Raw value---')
        debug_log(str(raw_val))
        debug_log('---------------')

        return raw_val


    def _format_seq_value(self, val_tok):
        '''
        Format a tokenized mapping value `val_tok` that is a sequence.

        :return: the sequence of raw mapping values whose ``n``-th element is
                 the raw value represented by the ``n``-th `val_tok`.

        :raise: an :py:exc:`InterpolationError` if formatting fails.
        '''
        # For a tokenized sequence value we get the represented raw
        # values of the sequence elements, and put them into a list.
        raw_val = [self.format_value(t) for t in val_tok]

        return raw_val


    def _format_map_value(self, val_tok):
        '''
        Format a tokenized mapping value `val_tok`that is a mapping.

        :return: the mapping with same keys like `val_tok` were each key ``k``
                 is associated with the raw value represented by the `val_tok`
                 value associated with ``k``.

        :raise: an :py:exc:`InterpolationError` if formatting fails.
        '''
        # For a tokenized mapping value we format its values in turn, and
        # associate these formatted values in a dictionary with the original
        # keys.
        raw_val = {k: self.format_value(val_tok[k]) for k in val_tok.keys()}

        return raw_val


    def _format_tok_value(self, val_tok):
        '''
        Format a tokenized value `val_tok` that is itself a value token.

        :return: the raw value represented by `val_tok`.

        :raise: an :py:exc:`InterpolationError` if formatting fails.
        '''
        if isinstance(val_tok, StrToken):
            debug_log('->String value token.')
            raw_val = self._format_StrToken_value(val_tok)
        elif isinstance(val_tok, KeyRefToken):
            debug_log('->Key reference token.')
            raw_val = self._format_KeyRefToken_value(val_tok)
        elif isinstance(val_tok, FctCallRefToken):
            debug_log('->Function call reference token.')
            raw_val = self._format_FctCallRefToken_value(val_tok)
        else:
            raise InterpolationError(
                f"No known value token\n  {val_tok}")

        return raw_val


    def _format_StrToken_value(self, val_tok):
        '''
        Format a :py:class:`StrToken` token `val_tok`.

        :return: the raw value represented by `val_tok`.

        :raise: an :py:exc:`InterpolationError` if formatting fails.
        '''
        # The parts of the string token are formatted and concatenated.
        raw_str = ''
        debug_log('---StrToken----------')
        for p in val_tok.parts:
            if isinstance(p, MapValueToken):
                # A tokenized value must be formatted first.
                debug_log(f"->Add value token\n  {p}")
                raw_str_p = self.format_value(p)
                debug_log(f" with raw value\n  {raw_str_p}")
                raw_str += raw_str_p
            elif isinstance(p, str):
                # A plain string is literally added.
                debug_log(f"-> Add raw string\n  {p}")
                raw_str += p
            else:
                raise InterpolationError(
                    f"Expected a string for\n  {p}", val_tok)
        debug_log('---------------------')

        return raw_str


    def _format_KeyRefToken_value(self, val_tok):
        '''
        Format a :py:class:`KeyRefToken` token `val_tok`.

        :return: the raw value of `mapg` represented by `val_tok`.

        :raise: an :py:exc:`InterpolationError` if formatting fails.
        '''
        # The components of a key reference token are put together to a key,
        # and a key reference string is built of it.
        debug_log('---KeyRefToken-------')
        # - Concatenate the key components.
        key = self.key_sep.join(val_tok.components)
        debug_log(f"->Key '{key}'")
        # - Put the delimiters for key reference string around.
        key_ref_str = self.key_beg + key + self.key_end
        debug_log('---------------------')

        return key_ref_str


    def _format_FctCallRefToken_value(self, val_tok):
        '''
        Format a :py:class:`FctCallRefToken` token `val_tok`.

        :return: the raw value represented by `val_tok`.

        :raise: an :py:exc:`InterpolationError` if formatting fails.
        '''
        # The function call reference string represented by a function call
        # reference token is reconstructed.
        debug_log('---FctCallRefToken---')
        # - Reconstruct the sequence of raw argument strings.
        raw_args_strs = [self.format_value(a) for a in val_tok.args]
        debug_log(f"->Argument strings\n  {raw_args_strs}")
        # - Combine it using the argument separator.
        raw_args_str = f"{self.args_sep} ".join(raw_args_strs)
        # - Put the function name in front of it.
        call_str = f"{val_tok.name} " + raw_args_str
        debug_log(f"->Function call string:\n  {call_str}")
        # Enclose the function call string into the function call reference
        # delimiters.
        call_ref_str = self.fct_beg + call_str + self.fct_end
        debug_log('---------------------')

        return call_ref_str


class MapValueToken(abc.ABC):
    '''
    Base class for tokens produced during tokenization of values in
    :py:call:`Mapping`s that can be interpolated.
    '''
    # Category for this sort of token.
    category = None


    def __str__(self):
        this_str = self.category

        return this_str


    def __eq__(self, t):
        # Intended ``==``: map value token objects are equal if the have the
        # same category, and all their components / parts are equal.
        if not isinstance(t, MapValueToken):
            is_eq = False
        elif not self.category == t.category:
            is_eq = False
        else:
            is_eq = self.__dict__ == t.__dict__

        return is_eq


class StrToken(MapValueToken):
    '''
    Token representing a string for value interpolation.
    '''
    category = 'Str'

    def __init__(self, str_parts):
        '''
        Initialize a string token consisting of the sequence `str_parts` of
        parts.
        '''
        self.parts = str_parts



class KeyRefToken(MapValueToken):
    '''
    Token representing a key reference for value interpolation.
    '''
    category = 'KeyRef'


    def __init__(self, key_components):
        '''
        Initialize a key reference token for key consisting of the components
        `key_components`.
        '''
        self.components = tuple(key_components)


class FctCallRefToken(MapValueToken):
    '''
    Token representing a function call reference for value interpolation.
    '''
    category = 'FctCallRef'


    def __init__(self, fct_name, fct_args):
        '''
        Initialize a function call reference for a function call using
        function with name `fct_name` and sequence `fct_args` of argument
        expressions.
        '''
        self.name = fct_name
        self.args = tuple(fct_args)


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
