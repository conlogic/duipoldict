..
 =============================================================================
 Title          : Value interpolation for mappings

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2021-07-24

 Description    : Description of the user interface.
 =============================================================================


===================
 The user interface
===================


Loading *dumapinterpol*
=======================

This package ``dumapinterpol`` is used by importing its single module,
:py:mod:`dumapinterpol`.


Value interpolation for mappings by *dumapinterpol*
===================================================

As already mentioned in the read me, package ``dumapinterpol`` provides tools
for value interpolation for :py:class:`Mapping` objects, i.e. to reuse other
values within values by using appropriate references to the former within the
latter.  This interpolation from ``dumapinterpol`` can be used for
:py:class:`Mapping` objects built from strings using list and mapping
containers.  Let us give a simple example:

.. literalinclude:: ../examples/persons_std.py
   :language: python
   :lines: 11-38

The dictionary ``PERSONS_INFO`` defined by this code snippet is a
:py:class:`Mapping` objects that can be used with ``dumapinterpol``.  Some of
its values are examples for the two forms of value interpolation provided by
``dumapinterpol``:

+ Item interpolation using key references, i.e. referring to the value in the
  :py:class:`Mapping` object for key referred to.  For instance,

  .. literalinclude:: ../examples/persons_std.py
     :language: python
     :lines: 26

  refers, by using compound key ``'persons.Erika'`` within the key reference
  ``'%{persons.Erika}%'`` to the value of the following item:

  .. literalinclude:: ../examples/persons_std.py
     :language: python
     :lines: 14-18

  By the way, a compound key is used to refer to values nested within the
  :py:class:`Mapping` object: so ``'persons.Erika'`` refers to the value of
  ``'Erika'`` in the dictionary that is the value of ``'persons'`` within the
  :py:class:`Mapping` object.

+ Function call interpolation using function call references, i.e. referring
  to the value returned by the function call that is named by the reference.
  For instance, the value in item

  .. literalinclude:: ../examples/persons_std.py
     :language: python
     :lines: 30

  refers, by using string ``'full_name %{persons.Erika}%'`` within the
  function call reference ``'%(full_name %{persons.Erika}%'%)`` to the value
  of function call:

  .. code-block:: python

     full_name(
         {
             'forename': 'Erika',
             'surname': 'Mustermann',
             'id': '30003'
         }
     )

  since the single argument name ``'%{persons.Erika}%'`` in the above function
  call reference is itself a key reference that is replaced by the value
  referred to.

Interpolation for :py:class:`Mapping` object values is done by
:py:class:`MapInterpolation` objects:

.. autoclass:: dumapinterpol.MapInterpolation

The parameters for the initialization of these objects control the fixed parts
in strings related to value interpolation.  Please note, that beside for the
value `fcts` for the list of all functions available for function call
interpolation, the example above uses the parameters' standard values.  For
the list `fcts` of functions available for function call interpolation the
following list ``PERSONS_FUNCTIONS`` is used:

  .. literalinclude:: ../examples/persons_fcts.py
     :language: python
     :lines: 46-52

The functions that are the elements of this list can be, for instance, defined
by:

  .. literalinclude:: ../examples/persons_fcts.py
     :language: python
     :lines: 10-43

Coming back to the example from above, we could thus define a suitable
:py:class:`MapInterpolation` object by:

.. code-block:: python

   mi = MapInterpolation(fcts=PERSONS_FUNCTIONS)

To do interpolation for :py:class:`Mapping` values based on a suitable
:py:class:`MapInterpolation` object like the just defined ``mi`` two methods
of latter objects are intended to be used.  The first one interpolates a
selected raw value, i.e. a value as stored in the mapping:

.. automethod:: dumapinterpol.MapInterpolation.interpolate_value

To provide an example based on the data from above:

.. code-block:: python

   mi.interpolate_value(
       '"%(make_greeting %{tpl}%, %{Erika.name}%)%"', PERSONS_INFO
   )

would return ``"Hello, dear Erika Mustermann!"``.

The other method is to get a -- either raw or interpolated -- value for a
given key:

.. automethod:: dumapinterpol.MapInterpolation.get

Therefore both calls

.. code-block:: python

   mi.get(PERSONS_INFO, 'greetings')

.. code-block:: python

   mi.get(PERSONS_INFO, 'greetings', raw=False)

return the interpolated value for key ``'greetings'`` in mapping
``PERSONS_INFO``, namely:

.. code-block:: python

   [
       'Hello, dear Jane Doe!',
       'Hello, dear Richard Roe!',
       'Hello, dear Erika Mustermann!'
   ]

On the other hand, the call

.. code-block:: python

   mi.get(PERSONS_INFO, 'greetings', raw=False)

returns the value for key ``'greetings'`` in mapping ``PERSONS_INFO``
unchanged, i.e.

.. code-block:: python

   "%(make_greetings %{tpl}%, %(full_names %{persons.all}%)%)%"

Please note that this raw value shows that function call interpolation can be
nested.

Finally we give an example where all parameters to control
:py:class:`MapInterpolation` use non-default values:

.. code-block:: python

   MapInterpolation(
       key_beg='{|', key_end='|}', key_sep='::',
       fct_beg='[|', fct_end='|]', args_sep=';',
       fcts=epf.PERSONS_FUNCTIONS
   )

This :py:class:`MapInterpolation` could be used with the following variant of
``PERSONS_INFO``:

.. literalinclude:: ../examples/persons_alt.py
   :language: python
   :lines: 11-38

and would provide the same interpolated values like ``mi`` for the original
``PERSONS_INFO``.

..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:

..  LocalWords:  Mustermann
