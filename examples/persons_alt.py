'''
A simple example how to use ``duipoldict``:

Some simple information about persons defined given by a dictionary.

This is a variant using the alternative values for the parameters of
:py:class:`duipoldict.ValueParser`.
'''


# Dictionary with persons' information.
PERSONS_INFO = {
    'Rae': {
        'hi': 'Said: [|make_greeting {|tpl|}; {|Rae::name|}|], by Jane.',
        'name': '[|full_name {|persons::Rae|}|]'
    },
    'persons': {
        'Rae': {
            'forename': 'Rae',
            'id': '303',
            'surname': 'Mayer'
        },
        'all': [
            {
                'forename': 'Jane',
                'id': '101',
                'surname': 'Doe'
            },
            {
                'forename': 'Rick',
                'id': '202',
                'surname': 'Roe'
            },
            '{|persons::Rae|}'
        ]
    },
    'say': '[|make_greetings {|tpl|}; [|full_names {|persons::all|}|]|]',
    'tpl': 'Hello, dear {}!'
}


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
