# ============================================================================
# Title          : A dictionary with value interpolation
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2021-07-27
#
# Description    : See documentation string below.
# ============================================================================

'''
Initialization for the ``examples`` subpackage.
'''


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
