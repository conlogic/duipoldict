..
 =============================================================================
 Title          : A dictionary with value interpolation

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2021-07-26

 Description    : Master file for the documentation.
 =============================================================================


================================
Documentation for ``duipoldict``
================================

.. toctree::
   :maxdepth: 2

   overview
   usage

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
