..
 =============================================================================
 Title          : A dictionary with value interpolation

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2021-07-26

 Description    : Change log for the package.
 =============================================================================


.. include:: ../CHANGELOG.rst


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
