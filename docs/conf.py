# ============================================================================
# Title          : A dictionary with value interpolation
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2021-07-26
#
# Description    : See documentation string below.
# ============================================================================

'''
Configuration file for the Sphinx documentation builder.
'''


import pathlib
import re
import sys
import setuptools as st

# For auto documentation and to import :py:mod:`setup`, the package's base
# directory path must be added to ``sys.path``.
this_dpath = pathlib.Path(__file__).parent
base_dpath = this_dpath.parent
sys.path.insert(0, str(base_dpath))


# Allow to get, for instance, attributes from :py:mod:`setup`
import setup as setup_mod


def extract_pkg_lifetime(fpath_changelog):
    '''
    Extract from the reST change log file with path `fpath_changelog` the
    lifetime of the corresponding Python distribution package.

    :return: a string showing the lifetime -- either as range with the begin
    and end year if the lifetime span multiple years, or as single year for
    single-year lifetime, or ``None`` if no version was found within the
    change log file.
    '''
    # Regexp to match a version header.
    version_header_rx = r'^Version \S+ \((\d\d\d\d)-\d\d-\d\d\)\s*$'

    years = []
    with open(fpath_changelog, 'rt') as fobj_changelog:
        for l in fobj_changelog:
            matcher_l = re.match(version_header_rx, l)
            if matcher_l is not None:
                year_l = matcher_l.group(1)
                years.append(year_l)

    if years == []:
        lifetime = None
    else:
        end_year = years[0]
        beg_year = years[-1]
        if beg_year == end_year:
            lifetime = str(beg_year)
        elif int(beg_year) + 1 == int(end_year):
            lifetime = f"{beg_year},{end_year}"
        else:
            lifetime = f"{beg_year}-{end_year}"

    return lifetime


# -- Project information -----------------------------------------------------

# Make package metadata available.
setup_cfg_fpath = base_dpath / 'setup.cfg'
pkg_cfg = st.config.read_configuration(setup_cfg_fpath)

project = setup_mod.dist_pkg
author = pkg_cfg['metadata']['author']
# The full version, including alpha/beta/rc tags
release = pkg_cfg['metadata']['version']

# Read the package's lifetime from its change log file.
changelog_fpath = base_dpath / 'CHANGELOG.rst'
lifetime = extract_pkg_lifetime(changelog_fpath)

copyright = f"{lifetime}, {author}"


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = ['sphinx.ext.autodoc']

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'pyramid'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']


# Configure the sidebars.
html_sidebars = {
    '**': ['localtoc.html', 'relations.html', 'sourcelink.html']
}


# -- Options for LaTeX output ------------------------------------------------

latex_elements = {
    # The paper size ('letterpaper' or 'a4paper').
    #
    'papersize': 'a4paper',

    # The font size ('10pt', '11pt' or '12pt').
    #
    # 'pointsize': '10pt',

    # Additional stuff for the LaTeX preamble.
    #
    # 'preamble': '',

    # Latex figure (float) alignment
    #
    # 'figure_align': 'htbp',
}


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
