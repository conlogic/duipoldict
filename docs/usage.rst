..
 =============================================================================
 Title          : A dictionary with value interpolation

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2021-12-27

 Description    : Description of the user interface.
 =============================================================================


===================
 The user interface
===================


Loading ``duipoldict``
======================

This package ``duipoldict`` is used by importing its single module,
:py:mod:`duipoldict`.


Value interpolation in ``duipoldict`` dictionaries
==================================================


Variants of value interpolation available in ``duipoldict``
-----------------------------------------------------------

As already mentioned in the read me, the module :py:mod:`duipoldict` of the
``duipoldict`` package defines a user dictionary :py:class:`InterpolDict`
whose values permit value interpolation.  The term "value interpolation"
within :py:class:`InterpolDict` values means that we can refer within
:py:class:`InterpolDict` values to other values to reuse them.  This is
achieved by using certain variants of references within
:py:class:`InterpolDict` values.  Such value references are strings with a
characteristic syntax.

Two variants of such value references are available:

+ Key references: a key reference is referring to the value nested within the
  :py:class:`InterpolDict` dictionary navigated to, within the
  :py:class:`InterpolDict` dictionary, by the sequence of referred keys.  An
  interpolation of the value for a key reference is called "item
  interpolation" since  the value that replaces a key reference during
  expansion stems from an item in the dictionary.

+ Function call references: a function call reference refers to the value of
  the respective function call.  An interpolation if the value for a function
  call reference will be called "function call interpolation".

If a :py:class:`InterpolDict` value is expanded that contains value
references, the value referred to is filled in ("interpolated") for the
respective value reference.

Let us give a simple example:

.. literalinclude:: ../examples/persons_std.py
   :language: python
   :lines: 11-39

The dictionary ``PERSONS_INFO`` defined by this code snippet is a
representation of a :py:class:`InterpolDict` dictionary using concrete values.
Some of its values are examples for the two variants of value interpolation
provided by ``duipoldict``:

+ Item interpolation: let us take value

  .. literalinclude:: ../examples/persons_std.py
     :language: python
     :lines: 23-35
     :emphasize-lines: 12

  nested in ``PERSONS_INFO``.  (Actually, this value is element with index
  ``2`` of the list that is, in turn, given by the key sequence ``('persons',
  'all')`` in ``PERSONS_INFO``.)  It is a string that solely consists of the
  key reference ``'%{persons.Rae}%'``.  This key reference refers, via the
  sequence ``('persons', 'Rae')`` of keys, to the value of the following
  nested item in ``PERSONS_INFO``:

  .. literalinclude:: ../examples/persons_std.py
     :language: python
     :lines: 17-22
     :emphasize-lines: 2-

  Therefore, for the key reference ``'%{persons.Rae}%'`` the value

  .. code-block:: python

     {
         'forename': 'Rae',
         'id': '303',
         'surname': 'Mayer'
     }

  is interpolated.


+ Function call interpolation: let us first take the value of item

  .. literalinclude:: ../examples/persons_std.py
     :language: python
     :lines: 13-16
     :emphasize-lines: 3

  nested in ``PERSONS_INFO``.  It solely consists of function call reference
  ``'%(full_name %{persons.Rae}%)%'``.  It refers to the value of unary
  function :py:func:`full_name` for the argument that is the value the key
  reference ``'%{persons.Rae}%'`` expands to.  Thus for this function call
  reference the value of function call

  .. code-block:: python

     full_name(
         {
             'forename': 'Ray,
             'id': '303',
             'surname': 'Mayer'
         }
     )

  is finally interpolated.

  By the way, the functions referred by their name in function call references
  can have any number of arguments, and the function call references can be
  nested.

  Let us take another example of a function call reference, namely the value
  of item

  .. literalinclude:: ../examples/persons_std.py
     :language: python
     :lines: 37-38
     :emphasize-lines: 1

  that is an item of ``PERSONS_INFO`` with key ``'say'``.  This value is a
  nested function call reference: it refers to calling function
  :py:func:`make_greetings` with the interpolated value of the key reference
  ``'%{tpl}%'`` as 1st argument, the interpolated value of another function
  call reference ``'%(full_names %{persons.all}%)%'`` as 2nd argument.

Let us highlight some import aspects of value references:

+ Value references itself are always (specific kinds of) strings.  Therefore
  they can only part of string values or of string elements in containers.

+ On the other hand, the values interpolated for value references are not
  necessarily strings.  For instance, in our example, for the key reference
  ``'%{persons.Rae}%'`` mentioned above a dictionary is interpolated.

+ If a value reference is a proper part of a string, the value interpolated
  for this value reference has, of course, to be a string, too.  For instance,
  in our example, the nested value in ``PERSONS_INFO`` for key sequence
  ``('Ray', 'hi')``

  .. literalinclude:: ../examples/persons_std.py
     :language: python
     :lines: 13-16
     :emphasize-lines: 2

  is a string with function call reference ``'%(make_greeting %{tpl}%,
  %{Rae.name}%)%'`` being a proper part of it.  Therefore, for this function
  reference a string value has to be interpolated.

+ The functions allowed for function call references are restricted: their
  list has to be given by list during :py:class:`InterpolDict`
  initialization.  For our example, the available functions are defined in a
  Python module, with their list ``PERSONS_FUNCTIONS`` highlighted:

  .. literalinclude:: ../examples/persons_fcts.py
     :language: python
     :lines: 11-53
     :emphasize-lines: 38-43


Syntax of value references for ``duipoldict``
---------------------------------------------

Value references have a characteristic syntax.  It is determined by a set of
syntax parameters that have, by default, standard values.  In the following
explanations, these standard syntax is used - like in our running example.

+ Key references: they are delimited by a begin delimiter and an end delimiter
  (``'%{'`` and ``'}%'`` in standard syntax).  Its content either consists of
  a single key, or multiple keys separated by the key separator (``'.'`` in
  standard) syntax.  For instance, in our example, ``'%{tpl}%'`` is key
  reference referring to single key ``'tpl'``, but ``'%{persons.Rae}%'`` is a
  key reference referring to the keys sequence ``('Ray', 'hi')``.

+ Function call references: they are delimited by a begin delimiter and an end
  delimiter (``'%('`` and ``')%'`` in standard syntax).  Its content always
  starts with the name of the function to be called.  If the function to be
  called has arguments, the string of the arguments is separated from the
  function name by whitespace.  Multiple arguments are separated by the
  argument separator (``','`` in standard syntax) that may optionally
  surrounded by whitespace.  Back to our example, ``'%(full_name
  %{persons.Rae}%)%'`` is an example for a function call reference with an
  function with 1 argument, while ``'%(make_greeting %{tpl}%,
  %{Rae.name}%)%'`` is a function call reference using a function with 2
  arguments.  A function call reference for a function named, say, ``'foo'``
  would look like ``'%(foo)%'``.

The values for the 6 parameters that controls the syntax of value references
can, of course, all given other values than the standard ones.  This is done
during  :py:class:`InterpolDict` initialization. To give a example for this,
let us repeat our example with an alternative syntax for value references:

.. literalinclude:: ../examples/persons_alt.py
   :language: python
   :lines: 11-39

This time we use ``'{|'`` / ``'|}'`` as begin / end delimiters for key
references, ``'::'`` as key separator, ``'[|'`` / ``'|]'`` as begin / end
delimiters for function call references, and ``';'`` as argument separator for
the them.


Working with ``duipoldict`` dictionaries
========================================

Module :py:mod:`duipoldict` of the ``duipoldict`` package defines a user
dictionary class :py:class:`InterpolDict`:

.. autoclass:: duipoldict.InterpolDict

Let us show this for our persons example from above:

.. code-block:: python

   # Import the module with persons data (standard syntax).
   import examples.persons_std as std
   # Import the functions used for value interpolation.
   from examples.persons_std import PERSONS_FUNCTIONS

   import duipoldict

   # Create a :py:class:`InterpolDict` object ``persons_std`` (standard
   # syntax).
   persons_std = duipoldict.InterpolDict(
       std.PERSONS_INFO, fcts=PERSONS_FUNCTIONS)

The raw (i.e. not expanded) values for :py:class:`InterpolDict` dictionary
internally use an abstract syntax representation for the value references that
is independent of the concrete syntax used.

During :py:class:`InterpolDict` initialization we can use the parameters that
control the concrete syntax for value references to choose a different syntax
for them.  For instance, if we want the alternative syntax mentioned above for
our persons example, we could modify the code just given like that:

.. code-block:: python

   # Import the module with persons data (alternative syntax).
   import examples.persons_alt as alt
   # Import the functions used for value interpolation.
   from examples.persons_std import PERSONS_FUNCTIONS

   import duipoldict

   # Create a :py:class:`InterpolDict` object ``persons_alt`` (alternative
   # syntax).
   persons_alt = duipoldict.InterpolDict(
       alt.PERSONS_INFO, fcts=PERSONS_FUNCTIONS,
       key_beg='{|', key_end='|}', key_sep='::',
       fct_beg='[|', fct_end='|]', arg_sep=';')

There is a method :py:meth:`read_dict` to import the content of a
:py:class:`InterpolDict` dictionary from any dictionary that stores the raw
values either in concrete or abstract syntax:

.. automethod:: duipoldict.InterpolDict.read_dict

Therefore we could create the :py:class:`InterpolDict` ``persons_std`` from
above alternatively by

.. code-block:: python

   # Create an empty :py:class:`InterpolDict` dictionary (standard syntax).
   persons_std = duipoldict.InterpolDict({}, fcts=PERSONS_FUNCTIONS)
   # Import data from ``PERSONS_INFO`` (standard syntax).
   persons_std.read_dict(std.PERSONS_INFO)

Please note that the dictionary passed as argument to :py:meth:`read_dict` can
be itself a :py:class:`InterpolDict`.  In this case the content of the latter
is simply copied.

On the other hand we can also export the content of a py:class:`InterpolDict`
dictionary to an ordinary dictionary storing the raw values:

.. automethod:: duipoldict.InterpolDict.write_dict

This way we can create a dictionary, say ``persons_info_std``, that is equal
to the original ``PERSONS_INFO`` of our persons example in standard syntax:

.. code-block:: python

   # Create a copy ``persons_info_std`` of original ``std.PERSONS_INFO``.
   persons_info_std = persons_std.write_dict()

Please note that ``persons_info`` uses, like original ``PERSONS_INFO``,
standard concrete syntax to store its values.  This is caused by not using the
``con`` keyword argument both during ``persons`` creation and in the
:py:meth:`write_dict` call.  This causes to use concrete syntax for raw values
by default.  If we want abstract raw values by default, we should initialize
``persons`` with ``con=False`` instead.  Or we can force abstract raw value
during export to ``persons_info_std`` by

.. code-block:: python

   # Use abstract raw values.
   persons_info_std = persons_std.write_dict(con=False)

One use case for exporting with abstract raw values is to change the concrete
syntax that is used for these values.  We could, for instance, convert the
dictionary of the persons example from standard to the alternative concrete
syntax by

.. code-block:: python

   # Export from standard syntax using abstract raw values.
   persons_abs = persons_std.write_dict(con=False)
   # Create a py:class:`InterpolDict` dictionary with concrete raw values in
   # alternative syntax from the dictionary with abstract raw values.
   persons_alt = duipoldict.InterpolDict(
       persons_abs, fcts=PERSONS_FUNCTIONS,
       key_beg='{|', key_end='|}', key_sep='::',
       fct_beg='[|', fct_end='|]', arg_sep=';')


The method :py:meth:`__getitem__` underlying the ``[]`` operator for mappings
is adapted to the value variants of a py:class:`InterpolDict`:

.. automethod:: duipoldict.InterpolDict.__getitem__

For instance, we get the value for key ``'say'`` in the dictionary
py:class:`InterpolDict` with the data of our persons example:

.. code-block:: python

   persons_std['say']

Please note that there is no way to control the variant of a value for
concrete :py:meth:`__getitem__` call: it always use the default value variant
of the py:class:`InterpolDict` object.  Thus, if one wants values not as raw
concrete values, one should use the ``'raw'`` or ``'con'`` switch during
py:class:`InterpolDict` initialization to change this.  For instance, if we
want to get the value for key ``'say'`` in the persons example, but in
expanded form:

.. code-block:: python

   # Create a :py:class:`InterpolDict` ``persons`` (standard syntax), with
   # expanded values by default.
   persons_exp = duipoldict.InterpolDict(
       std.PERSONS_INFO, fcts=PERSONS_FUNCTIONS, raw=False)
   # Get the value for ``'say'`` - this time expanded.
   persons_exp['say']

Other than for standard dictionaries, the :py:meth:`__getitem__` of
:py:class:`InterpolDict` can also access value nested in the dictionary in one
step.  Let us show this using the persons example the list of all persons:

.. code-block:: python

   # List of all persons using a key sequence.
   persons_std(('persons', 'all'))
   # List of all persons using a compound key.
   persons_std('persons.all')

Please note that the compound key variant is exactly the syntax used for keys
in key value references within py:class:`InterpolDict` values.

There is another :py:class:`InterpolDict` method, :py:meth:`get` to get values
from a :py:class:`InterpolDict` dictionary.  Since :py:meth:`get` is a
non-operator method, it can have its own keyword arguments to control the
value variant used:

.. automethod:: duipoldict.InterpolDict.get

Using :py:meth:`get`, we could get the expanded value for key ``'say'`` in the
persons example also a :py:class:`InterpolDict` that uses raw concrete values
by default:

.. code-block:: python

    persons_std.get('say', raw=False)

Finally, there is a :py:class:`InterpolDict` method to expand a given raw
value:

.. automethod:: duipoldict.InterpolDict.expand_value

Its main use is to work with already given raw values - either in concrete or
abstract syntax.  Please note that the value to expand do *not* need to be
value in the respective :py:class:`InterpolDict` dictionary as long as all
value references in the value can be resolved for the dictionary.  For
instance, for the persons example

.. code-block:: python

   persons_std.expand_value('The full name of Rae is %{Rae.name}%.')

returns ``'The full name of Rae is Rae Mayer.'``.


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
