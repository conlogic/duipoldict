..
 =============================================================================
 Title          : A dictionary with value interpolation

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2021-07-26

 Description    : Read me file for the package.
 =============================================================================


.. include:: ../README.rst


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
