#!/usr/bin/env python3
# ============================================================================
# Title          : A dictionary with value interpolation
#
# Classification : Python script
#
# Author         : Dirk Ullrich
#
# Date           : 2021-09-27
#
# Description    : See documentation string below.
# ============================================================================

'''
Helper to generate all test data.
'''


import pathlib
import sys
this_dpath = pathlib.Path(__file__).parent
pkg_dpath = this_dpath.parent.parent
# Inserting before the standard Python module paths is important since there
# may exist other Python packages named ``tests`` or ``examples`` in one of
# these paths.
sys.path.insert(0, str(pkg_dpath))

import tests.test_duipoldict


# Functions
# =========


def main():
    print("Generating test data for 'duipoldict'...")
    fpaths = tests.test_duipoldict.mk_test_data()
    print("Generating test data for 'duipoldict' generated:")
    for f in fpaths:
        print(f"  {f}")


# Doing
# =====


if __name__ == '__main__':
    main()


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
