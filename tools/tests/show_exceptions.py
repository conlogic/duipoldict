#!/usr/bin/env python3
# ============================================================================
# Title          : A dictionary with value interpolation
#
# Classification : Python script
#
# Author         : Dirk Ullrich
#
# Date           : 2021-09-27
#
# Description    : See documentation string below.
# ============================================================================

'''
Helper to show (the messages of) some ``duipoldict``-specific exceptions.
'''


import pathlib
import sys
this_dpath = pathlib.Path(__file__).parent
pkg_dpath = this_dpath.parent.parent
# Inserting before the standard Python module paths is important since there
# may exist other Python packages named ``tests`` or ``examples`` in one of
# these paths.
sys.path.insert(0, str(pkg_dpath))

import duipoldict
import examples.persons_std as eps
import examples.persons_fcts as epf


# Classes and functions
# =====================


class show_exception(object):
    '''
    Context manager to show an :py:exc:`InterpolationError` exception.
    '''
    def __enter__(self):
        '''
        Method to enter this context.
        '''
        # Nothing to do when entering the context.
        pass


    def __exit__(self, exc_type, exc_obj, traceback):
        '''
        Method to exit this context, where for not-``None`` type `exc_type` is
        the type of raised exception, a not-``None`` `exc_obj` is the
        exception raised, and a not-``None`` `traceback` is the associated
        trace back.
        '''
        if exc_type is None:
            # We cannot pass ``None`` to ``issubclass``.
            pass
        elif issubclass(exc_type, duipoldict.InterpolDictError):
            print('------------------------')
            print(exc_obj)
        print('------------------------')

        return True


def show_str_parse_exception(con_str):
    '''
    Show an :py:exc:`InterpolationError` when parsing concrete string value
    `con_str`.
    '''
    with show_exception():
        print(f"Parsing '{con_str}'...")
        ipd = duipoldict.InterpolDict({})
        parser = duipoldict.StrValueParser(**ipd.syntax)
        parser.parse_value(con_str, 1)


def show_expand_value_exception(val, data, fcts=[]):
    '''
    Show an :py:exc:`InterpolationError` when expanding value `val` for an
    :py:class:`InterpolDict` dictionary initialized with data `data` and
    functions `fcts` for function call interpolation.
    '''
    with show_exception():
        print(f"Expanding value '{val}' for data\n  {data}")
        ipd = duipoldict.InterpolDict(data, fcts=fcts)
        ipd.expand_value(val)


def show_get_value_exception(key, data, fcts=[]):
    '''
    Show an :py:exc:`InterpolationError` when getting value for key `key` and
    an :py:class:`InterpolDict` dictionary initialized with data `data` and
    functions `fcts` for function call interpolation.
    '''
    with show_exception():
        print(f"Getting value for key(s) '{key}' for data\n  {data}")
        ipd = duipoldict.InterpolDict(data, fcts=fcts, raw=False)
        ipd.get(key)


def main():
    # Errors when parsing concrete string values.
    # - :py:exc:`MissingRefBegError`.
    show_str_parse_exception('foo}%')
    # - :py:exc:`MissingRefEndsError`.
    show_str_parse_exception('%(foo bar, baz')
    show_str_parse_exception('%(foo bar, %{baz})')
    # - :py:exc:`MismatchRefError`.
    show_str_parse_exception('%{foo)%')
    # - :py:class:`NestedKeyRefError`.
    show_str_parse_exception('%{foo%{')

    # Errors when expanding values.
    # - Parse error.
    val = '%{paths.exe'
    show_expand_value_exception(val, eps.PERSONS_INFO, epf.PERSONS_FUNCTIONS)
    # - Recursion error.
    val = '%{foo}%'
    data = {'foo': '%{bar}%', 'bar': '%{foo}%'}
    show_expand_value_exception(val, data)
    # - Parse type error.
    val = 13
    show_expand_value_exception(val, eps.PERSONS_INFO, epf.PERSONS_FUNCTIONS)

    # Errors when getting values.
    # - Parse error.
    key = 'foo'
    data = {'foo': '%(foo bar, %{baz})'}
    show_get_value_exception(key, data)
    # - Recursion error.
    key = 'foo'
    data = {'foo': '%{bar}%', 'bar': '%{foo}%'}
    show_get_value_exception(key, data)


# Doing
# =====


if __name__ == '__main__':
    main()


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
