# ============================================================================
# Title          : A dictionary with value interpolation
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2021-12-29
#
# Description    : See documentation string below.
# ============================================================================

'''
Single module for package ``duipoldict``.
'''


import collections as colls
import re
import copy

import dubasiclog


# Parameters
# ==========

# Version of the very package.
version = '0.5.3'


# Maximal line length.
MAX_LINE_LENGTH = 78


# Functions and classes
# =====================


# For uniform logging we use the module-global logging facilities from package
# ``dubasiclog``.
log = dubasiclog.log
enable_debug_log = dubasiclog.enable_debug_log
disable_debug_log = dubasiclog.disable_debug_log
debug_is_enabled = dubasiclog.debug_is_enabled
debug_log = dubasiclog.debug_log


def recursive_debug_log(depth, msg, **kwargs):
    '''
    A variant of :py:func:`debug_log` for recursion: call ``debug_log(rec_msg,
    **kwargs)`` where ``rec_msg`` is formed from message string `msg` by
    adding a ``'+'`` prefix whose length depends on recursion depth.

    Number `depth` measures the recursion depth.
    '''
    rec_msg = ('+' * depth) + msg
    debug_log(rec_msg, **kwargs)


def pretty_value_debug_log(val, **kwargs):
    '''
    A variant of :py:func:`debug_log` to pretty-print a value `val`, where
    keyword arguments in `kwargs` are passed to :py:func:`debug_log`.
    '''
    if debug_is_enabled():
        pp_val = dubasiclog.pretty_format(val)
        debug_log(pp_val, **kwargs)


def calc_key_components(key, key_sep=None):
    '''
    Calculate the components of key `key`.

    Key `key` can be consist of multiple components to address nested values,
    and has to be given in one of the following forms:

    + as a none-string sequence of its components;

    + as string built of its components separated by the key separator
      `key_sep`.  If `key_sep` is ``None``, `key` is always treated as
    plain key without multiple components.

    :return: the components of `key` given as tuple.
    '''
    if isinstance(key, str) and key_sep is not None:
        # Key is a string and we have value interpolation -> key is split at
        # the key separator occurrences.
        key_components = key.split(key_sep)
    elif isinstance(key, str):
        # Key is a string and we have no value interpolation -> key itself is
        # its only component.
        key_components = (key,)
    elif isinstance(key, colls.abc.Sequence):
        # Key is a none-string sequence -> use its list as key components.
        key_components = tuple(key)
    else:
        raise KeyError(f"Illegal form of key '{key}'.")

    return key_components


class InterpolDict(colls.UserDict):
    '''
    Dictionary with string keys and interpolation within its values, where the
    values are strings, or composed of strings using lists or dictionaries.
    Value interpolation allows to refer, within values of the dictionary, to
    other values by using specific kinds of value references.  Such value
    references are resolved by interpolating, i.e filling in, the referred
    value for the respective value reference, when a raw dictionary value that
    contains value references is expanded.

    The following variants of value references are available:

    + Key references - these are references to other values within the
      dictionary or subdirectories using the, possible nested, keys for the
      values referred to.  Interpolation of the value referred to by a key
      reference in a raw dictionary value is called "item interpolation".

    + Function call references - they are referring to the value of a function
      call, where for these calls functions from the list `fcts` are allowed.
      Function call references can also be nested within function call
      references.  Interpolation of the value referred to by a function call
      reference in a raw dictionary value is called "function call
      interpolation".

    For a ``True`` switch `raw` raw dictionary values are used by default
    instead of expanded ones.

    For a ``True`` switch `con` raw dictionary values are concrete by default
    instead of being abstract like in the internal representation of the
    :py:class:`InterpolDict` data that is independent of the concrete syntax
    used for value interpolation.

    The remaining keyword arguments control the concrete syntax used for value
    references:

    + Strings `key_beg`, `key_end` and `key_sep` used in key references for
      item interpolation in values.  Here `key_beg` / `key_end` delimits a key
      reference at the begin / end, and a `key_sep` separates the keys in such
      a reference to refer to nested values.  If one of these parameters is
      ``None``, item interpolation is disabled.
    + Strings `fct_beg`, `fct_end` and `arg_sep`  used in function call
      references for function call interpolation in values.  Here `fct_beg` /
      `fct_end` delimits a function call reference at the begin / end, and
      `arg_sep` separates arguments from each other within such a reference.
      If one of these parameters is ``None``, function call interpolation is
      disabled.

    Mapping `data` is used to initialize the data for this dictionary.

    :raise: a :py:exc:`InterpolDictError` if parsing during reading
            `data` fails.
    '''
    def __init__(self, data, fcts=[], raw=True, con=True,
                 key_beg='%{', key_end='}%', key_sep='.',
                 fct_beg='%(', fct_end=')%', arg_sep=','):
        self.fcts = fcts
        self.raw = raw
        self.con = con

        # Check whether one type or all value interpolation is disabled.
        self.syntax = {}
        if None in [key_beg, key_end, key_sep]:
            log(1, 'Item interpolation is disabled.')
        else:
            self.syntax.update({
                'key_beg': key_beg, 'key_end': key_end, 'key_sep': key_sep
            })

        if None in [fct_beg, fct_end, arg_sep]:
            log(1, 'Function call interpolation is disabled.')
        else:
            self.syntax.update({
                'fct_beg': fct_beg, 'fct_end': fct_end, 'arg_sep': arg_sep
            })

        # We create a :py:class:`ValueParser` to have abstract values if
        # needed.
        self._parser = ValueParser(**self.syntax)
        # We create a :py:class:`ValueFormatter`, too.  At the moment it
        # allows to use concrete syntax instead of abstract one in logging and
        # error messages.
        self._formatter = ValueFormatter(**self.syntax)

        # We also index the functions available for function call references
        # by their names for convenience.
        self._fcts_by_names = {f.__name__: f for f in self.fcts}

        # We read data `data` as abstract values.
        self.read_dict(data)


    def _parse_value(self, val, supress_debug=True):
        '''
        Variant of :py:meth:`parse_value` of :py:class:`ValueParser` where
        debug logging is temporarily disabled iff `supress_debug` is
        ``True``.

        :return: an abstract variant of `val`.

        :raise: a :py:exc:`InterpolDictError` if parsing fails.
        '''
        re_enable_debug = supress_debug and debug_is_enabled()
        if re_enable_debug:
            disable_debug_log()
        abs_val = self._parser.parse_value(val)
        if re_enable_debug:
            enable_debug_log()

        return abs_val


    def _format_value(self, val, supress_debug=True):
        '''
        Variant of :py:meth:`format_value` of :py:class:`ValueFormatter` where
        debug logging is temporarily disabled iff `supress_debug` is
        ``True``.

        :return: an concrete variant of `val`.

        :raise: a :py:exc:`InterpolDictError` if formatting fails.
        '''
        re_enable_debug = supress_debug and debug_is_enabled()
        if re_enable_debug:
            disable_debug_log()
        con_val = self._formatter.format_value(val)
        if re_enable_debug:
            enable_debug_log()

        return con_val


    def read_dict(self, data):
        '''
        Read data from mapping `data`.

        :raise: a :py:exc:`InterpolDictError` if parsing during reading `data`
                fails.
        '''
        self.data = self._parse_value(data, supress_debug=False)


    def write_dict(self, con=None):
        '''
        Write current data as dictionary.

        A not-``None`` switch `con` forces whether this dictionary has
        concrete values (for a ``True`` value) or abstract ones (for a
        ``False`` value).  For a ``None`` value of `con` the default is used.

        :return: current data as dictionary.

        :raise: a :py:exc:`InterpolDictError` if formatting of the values for
                current concrete syntax fails.
        '''
        if con is None:
            con = self.con

        if con:
            data = self._format_value(self.data, supress_debug=False)
        else:
            data = self.data

        return data


    def get(self, key, raw=None, con=None):
        '''
        Get the value for `key`.

        Beside being a single key, `key` can also consist of multiple
        components to address nested values.  For details please consult the
        :py:meth:`__getitem__` documentation.

        A not-``None`` switch `raw` forces whether the value is raw (for a
        ``True`` value) or expanded (for a ``False`` value).  For a
        ``None`` value of `raw` the default is used.

        A not-``None`` switch `con` forces, for a raw value, whether it is
        concrete (for a ``True`` value) or abstract (for a ``False`` value).
        For a ``None`` value of `con` the default is used.

        :return: the value for `key`.

        :raise: a :py:exc:`InterpolationError` if value expansion fails.
        :raise: a :py:exc:`KeyError` if `key` has none of the forms just
                described, or a key needed to interpolate a value for a key
                reference cannot be found at the needed place.
        :raise: a :py:exc:`ValueError` if for one of the keys in `keys` that
                is not the last one, the value is no mapping.
        '''
        if raw is None:
            raw = self.raw
        if con is None:
            con = self.con

        # Calculate the components of `key`.
        key_sep = self._parser.syntax.get('key_sep')
        key_components = calc_key_components(key, key_sep)

        # Get the raw abstract value for `key`.
        raw_abs_val = self._get_nested_raw(key_components)

        # Get the value for `key` in the variant wanted.
        if raw and not con:
            # Raw abstract value wanted -> we are done.
            val = raw_abs_val
        elif raw:
            # Raw concrete value wanted -> format the value.
            val = self._format_value(raw_abs_val, supress_debug=False)
        else:
            # Expanded value wanted -> expand the value.
            val = self._expand_value(raw_abs_val)

        return val


    def __getitem__(self, key):
        '''
        Implementation of the :py:meth:`__getitem__` method required for any
        :py:class:`Mapping` object.

        Beside being a single key, `key` can also consist of multiple
        components to address nested values that is given either:

        + as a none-string sequence of its components, or

        + as string built of its components separated by the key separator.
          If item interpolation is not supported `key` is always treated as
          plain key without multiple components.

        :return: the value for `key`.

        :raise: a :py:exc:`InterpolationError` if value expansion fails.
        :raise: a :py:exc:`KeyError` if `key` has none of the forms just
                described, or a key needed to interpolate a value for a key
                reference cannot be found at the needed place.
        :raise: a :py:exc:`ValueError` if for one of the keys in `keys` that
                is not the last one, the value is no mapping.
        '''

        return self.get(key)


    def _get_nested_raw(self, keys):
        '''
        Get the raw nested value for the sequence `keys` of keys.

        :return: the value associated with `keys`, or ``None`` if `keys` is
                 empty.

        :raise: a :py:exc:`KeyError` if one of the keys in `keys` cannot be
                found at the needed place.
        :raise: a :py:exc:`ValueError` if for one of the keys in `keys` that
                is not the last one, the value is no mapping.
        '''
        map_now = self.data
        keys_now = []
        val = None
        for k in keys:
            keys_now.append(k)
            try:
                val_k = map_now[k]
            except KeyError:
                raise KeyError(
                    f"Missing value for sequence '{keys_now}' of keys.")
            if k == keys[-1]:
                val = val_k
            elif isinstance(val_k, colls.abc.Mapping):
                map_now = val_k
            else:
                raise ValueError(
                    f"No mapping value for sequence '{keys_now}' of keys.")

        return val


    def expand_value(self, val):
        '''
        Expand raw value `val`, i.e. interpolate for all value references
        within `val` the respective values referred to.

        :return: expanded value for `val`.

        :raise: a :py:exc:`InterpolDictError` if expansion fails.
        :raise: a :py:exc:`KeyError` if a key used in key reference cannot be
                found at the needed place.
        :raise: a :py:exc:`ValueError` if for one of the keys use in key
                reference that is not the last one, the value is no mapping.
        '''
        # Ensure an abstract variant of the raw value to be expanded.
        abs_val = self._parse_value(val, supress_debug=True)

        # Expand the abstract value.
        exp_val = self._expand_value(abs_val)

        return exp_val


    def _expand_value(self, abs_val):
        '''
        Expand abstract raw value `abs_val`, i.e. interpolate for all
        references within `abs_val` the respective values referred to.

        :return: expanded value for `abs_val`.

        :raise: a :py:exc:`InterpolDictError` if expansion fails.
        :raise: a :py:exc:`KeyError` if a key used in key reference cannot be
                found at the needed place.
        :raise: a :py:exc:`ValueError` if for one of the keys use in key
                reference that is not the last one, the value is no mapping.
        '''
        # Start the value expansion recursion.
        try:
            exp_val = self._expand_any_value(abs_val, 0)
        except RecursionError:
            con_val = self._format_value(abs_val, supress_debug=True)
            raise InterpolDictError('Expansion loops', val=con_val)

        return exp_val


    def _expand_any_value(self, abs_val, depth):
        '''
        A recursion step during expansion: expand abstract value `abs_val` of
        any kind for this dictionary.

        Number `depth` measures the recursion depth.

        :return: expanded value for `abs_val`.

        :raise: a :py:exc:`InterpolDictError` if expansion fails.
        :raise: a :py:exc:`KeyError` if a key used in key reference cannot be
                found at the needed place.
        :raise: a :py:exc:`ValueError` if for one of the keys use in key
                reference that is not the last one, the value is no mapping.
        '''
        con_val = self._format_value(abs_val, supress_debug=True)
        recursive_debug_log(depth, '-Abstract raw value-------')
        pretty_value_debug_log(abs_val)
        recursive_debug_log(depth, '-Concrete raw value-------')
        pretty_value_debug_log(con_val)
        recursive_debug_log(depth, '--------------------------')

        if isinstance(abs_val, colls.abc.Sequence):
            # Note that we do not need to distinguish :py:class:`str` values
            # since `abs_val` is abstract.
            recursive_debug_log(depth, '->Sequence value.')
            exp_val = self._expand_seq_value(abs_val, depth)
        elif isinstance(abs_val, colls.abc.Mapping):
            recursive_debug_log(depth, '->Mapping value.')
            exp_val = self._expand_map_value(abs_val, depth)
        elif isinstance(abs_val, AbstractStr):
            recursive_debug_log(depth, '->String value.')
            exp_val = self._expand_AbstractStr_value(abs_val, depth)
        elif isinstance(abs_val, AbstractKeyRef):
            recursive_debug_log(depth, '->Key reference value.')
            exp_val = self._expand_AbstractKeyRef_value(abs_val, depth)
        elif isinstance(abs_val, AbstractFctCallRef):
            recursive_debug_log(depth, '->Function call reference value.')
            exp_val = self._expand_AbstractFctCallRef_value(abs_val, depth)
        else:
            raise InterpolDictError('No known expansion', val=con_val)

        recursive_debug_log(depth, '-Expanded value-----------')
        pretty_value_debug_log(exp_val)
        recursive_debug_log(depth, '--------------------------')

        return exp_val


    def _expand_seq_value(self, abs_val, depth):
        '''
        Expand abstract sequence value `abs_val`.

        Number `depth` measures the recursion depth.

        :return: expanded value for `abs_val`.

        :raise: a :py:exc:`InterpolDictError` if expansion fails.
        :raise: a :py:exc:`KeyError` if a key used in key reference cannot be
                found at the needed place.
        :raise: a :py:exc:`ValueError` if for one of the keys use in key
                reference that is not the last one, the value is no mapping.
        '''
        recursive_debug_log(depth, '--B expanding sequence----')
        pretty_value_debug_log(abs_val)

        # For an abstract sequence value `abs_val` we expand the elements of
        # the sequence: the expanded sequence has the same number of the
        # elements like `abs_val`, and the value for index ``i`` in the former
        # is the expanded value for ``i`` in `abs_val`.
        exp_val = [self._expand_any_value(e, depth + 1) for e in abs_val]

        recursive_debug_log(depth, '--E expanding sequence----')
        pretty_value_debug_log(abs_val)

        return exp_val


    def _expand_map_value(self, abs_val, depth):
        '''
        Expand abstract mapping value `abs_val`

        Number `depth` measures the recursion depth.

        :return: expanded value for `abs_val`.

        :raise: a :py:exc:`InterpolDictError` if expansion fails.
        :raise: a :py:exc:`KeyError` if a key used in key reference cannot be
                found at the needed place.
        :raise: a :py:exc:`ValueError` if for one of the keys use in key
                reference that is not the last one, the value is no mapping.
        '''
        recursive_debug_log(depth, '--B expanding mapping-----')
        pretty_value_debug_log(abs_val)

        # For an abstract mapping value `abs_val` we expand the values of the
        # mapping: the expanded dictionary has the same keys like `abs_val`,
        # and the value for key ``k`` in the former is the expanded value for
        # ``k`` in `abs_val`.
        exp_val = {k: self._expand_any_value(v, depth + 1)
                   for (k, v) in abs_val.items()}

        recursive_debug_log(depth, '--E expanding mapping-----')
        pretty_value_debug_log(abs_val)

        return exp_val


    def _expand_AbstractStr_value(self, abs_val, depth):
        '''
        Expand value for :py:class:`AbstractStr` `abs_val`

        Number `depth` measures the recursion depth.

        :return: expanded value for `abs_val`.

        :raise: a :py:exc:`InterpolDictError` if expansion fails.
        :raise: a :py:exc:`KeyError` if a key used in key reference cannot be
                found at the needed place.
        :raise: a :py:exc:`ValueError` if for one of the keys use in key
                reference that is not the last one, the value is no mapping.
        '''
        recursive_debug_log(depth, f"--B expanding Str---------\n  {abs_val}")

        # For a string value `abs_val` we expand its parts in turn, and
        # concatenate the expanded values of these parts.
        raise_err = False
        exp_parts_vals = []
        for p in abs_val.parts:
            recursive_debug_log(depth, f"-->Part\n  {p}")
            if isinstance(p, str):
                # ``p`` is a plain string -> no expansion needed, i.e. ``p``
                # is added itself.
                recursive_debug_log(depth, '-->Plain string.')
                exp_val_p = p
            elif isinstance(p, AbstractKeyRef):
                # ``p`` is a key reference value -> ``p`` is expanded itself,
                # and the result must be a string.
                recursive_debug_log(depth, '-->Key reference value.')
                exp_val_p = self._expand_AbstractKeyRef_value(p, depth)
                if not isinstance(exp_val_p, str):
                    raise_err = True
            elif isinstance(p, AbstractFctCallRef):
                # ``p`` is a function call reference value -> ``p`` is
                # expanded itself, and the result must be a string.
                recursive_debug_log(
                    depth, '-->Function call reference value.')
                exp_val_p = self._expand_AbstractFctCallRef_value(p, depth)
                if not isinstance(exp_val_p, str):
                    raise_err = True
            else:
                fmt_p = self._format_value(p, supress_debug=True)
                msg = f"Illegal string value part\n  {fmt_p}"
                raise InterpolDictError(msg)
            if raise_err:
                fmt_p = self._format_value(p, supress_debug=True)
                msg = 'f"Expect expanded string value for\n'
                msg += f"  {fmt_p}"
                raise InterpolDictError(msg)
            else:
                exp_parts_vals.append(exp_val_p)
            recursive_debug_log(depth, f"-->Expanded part\n  {exp_val_p}")
        exp_val = ''.join(exp_parts_vals)

        recursive_debug_log(depth, f"--E expanding Str---------\n  {abs_val}")

        return exp_val


    def _expand_AbstractKeyRef_value(self, abs_val, depth):
        '''
        Expand value for :py:class:`AbstractKeyRef` `abs_val`

        Number `depth` measures the recursion depth.

        :return: expanded value for `abs_val`.

        :raise: a :py:exc:`InterpolDictError` if expansion fails.
        :raise: a :py:exc:`KeyError` if a key used in key reference cannot be
                found at the needed place.
        :raise: a :py:exc:`ValueError` if for one of the keys use in key
                reference that is not the last one, the value is no mapping.
        '''
        recursive_debug_log(
            depth, f"---B expanding KeyRef-----\n  {abs_val}")

        # We lookup the raw abstract value for the sequence of keys in the key
        # reference, and expand this value in turn.
        ref_abs_val = self._get_nested_raw(abs_val.keys)
        recursive_debug_log(depth, f"--->Abstract value\n  {ref_abs_val}")
        exp_val = self._expand_any_value(ref_abs_val, depth + 1)

        recursive_debug_log(
            depth, f"---E expanding KeyRef-----\n  {abs_val}")

        return exp_val


    def _expand_AbstractFctCallRef_value(self, abs_val, depth):
        '''
        Expand value for :py:class:`AbstractFctCallRef` `abs_val`

        Number `depth` measures the recursion depth.

        :return: expanded value for `abs_val`.

        :raise: a :py:exc:`InterpolDictError` if expansion fails.
        :raise: a :py:exc:`KeyError` if a key used in key reference cannot be
                found at the needed place.
        :raise: a :py:exc:`ValueError` if for one of the keys use in key
                reference that is not the last one, the value is no mapping.
        '''
        recursive_debug_log(
            depth, f"---B expanding FctCallRef-\n  {abs_val}")

        # First we get the referred function.  If it is not one of those
        # available for function call interpolation, we signal an error.
        fct_name = abs_val.name
        try:
            fct = self._fcts_by_names[fct_name]
        except KeyError:
            con_val = self._format_value(abs_val, supress_debug=True)
            # We add the names of available functions to the error message.
            msg = f"Function '{fct_name}' must be one of the following ones:"
            for f in self._fcts_by_names.keys():
                msg += f"\n- '{f}'"
            raise InterpolDictError(msg, val=con_val)

        # Next we have to expand all raw abstract argument values in turn.
        exp_args_vals = [
            self._expand_any_value(a, depth + 1) for a in abs_val.args]
        recursive_debug_log(
            depth, f"--->List of expanded arguments\n  {exp_args_vals}")

        # Finally we do the referred function call.
        try:
            exp_val = fct(*exp_args_vals)
        except TypeError as e:
            con_val = self._format_value(abs_val, supress_debug=True)
            con_args_vals = [
                self._format_value(a, supress_debug=True)
                for a in abs_val.args]
            msg = f"The following type error has occurred:\n  {e}\n"
            msg += f"when calling function '{fct_name}' with arguments:"
            for a in con_args_vals:
                msg += f"\n- '{a}'"
            msg += '\n'
            raise InterpolDictError(msg, val=con_val)

        recursive_debug_log(
            depth, f"---E expanding FctCallRef-\n  {abs_val}")

        return exp_val


class ValueParser(object):
    '''
    Parser to transform concrete values into abstract ones.

    The keyword arguments that can be given here as `**syntax` controls the
    concrete syntax for value references.  For a description of these syntax
    parameters please consult the documentation of :py:class:`InterpolDict`.
    '''
    def __init__(self, **syntax):
        '''
        Initialize a value parser.
        '''
        self.syntax = syntax


    def parse_value(self, val):
        '''
        Get an abstract variant of value `val`: if `val` is a concrete value,
        it will be parsed, but if `val` is already abstract, it will be only
        copied.

        :return: abstract variant of `val`.

        :raise: a :py:exc:`InterpolDictError` if parsing fails.
        '''
        abs_val = self._parse_any_value(val, 0)

        return abs_val


    def _parse_any_value(self, val, depth):
        '''
        A recursion step during parsing: get an abstract variant of value
        `val` of any kind.

        Number `depth` measures the recursion depth.

        :return: abstract variant of `val`.

        :raise: a :py:exc:`InterpolDictError` if parsing fails.
        '''
        recursive_debug_log(depth, '-Input value---------------')
        pretty_value_debug_log(val)
        recursive_debug_log(depth, '---------------------------')

        # Here we distinguish the various types of values we can parse.
        if isinstance(val, AbstractValue):
            recursive_debug_log(depth, '->Abstract value.')
            abs_val = copy.deepcopy(val)
        elif isinstance(val, str):
            recursive_debug_log(depth, '->Concrete string value.')
            abs_val = self._parse_str_value(val, depth)
        elif isinstance(val, colls.abc.Sequence):
            recursive_debug_log(depth, '->Sequence value.')
            abs_val = self._parse_seq_value(val, depth)
        elif isinstance(val, colls.abc.Mapping):
            recursive_debug_log(depth, '->Mapping value.')
            abs_val = self._parse_map_value(val, depth)
        else:
            # Other types of concrete values are unknown.
            raise InterpolDictError('No known parsing', val=val)

        recursive_debug_log(depth, '-Abstract value------------')
        pretty_value_debug_log(abs_val)
        recursive_debug_log(depth, '---------------------------')

        return abs_val


    def _parse_seq_value(self, seq_val, depth):
        '''
        Get an abstract variant of value `seq_val` that is a
        :py:class:`Sequence` object but not a string.

        Number `depth` measures the recursion depth.

        :return: an abstract variant of `seq_val`.

        :raise: a :py:exc:`InterpolDictError` if parsing fails.
        '''
        # For a none-string sequence value `seq_val` the abstract variant of
        # `seq_val` is a list whose ``n``-th element is the abstract variant
        # of the ``n``-th element of `seq_val`.
        recursive_debug_log(depth, '--B parsing sequence-------')
        pretty_value_debug_log(seq_val)
        abs_seq_val = [self._parse_any_value(e, depth + 1) for e in seq_val]
        recursive_debug_log(depth, '--E parsing sequence-------')

        return abs_seq_val


    def _parse_map_value(self, map_val, depth):
        '''
        Get an abstract variant of value `map_val` that is a
        :py:class:`Mapping` object.

        Number `depth` measures the recursion depth.

        :return: an abstract variant of `map_val`.

        :raise: a :py:exc:`InterpolDictError` if parsing fails.
        '''
        # For mapping value `map_val` the abstract variant of `map_val` has
        # the same keys like `map_val`, and its value for key ``k`` is the
        # abstract variant of the value for ``k`` in `map_val`.
        recursive_debug_log(depth, '--B parsing map------------')
        pretty_value_debug_log(map_val)
        abs_map_val = {
            k: self._parse_any_value(v, depth + 1)
            for (k, v) in map_val.items()}
        recursive_debug_log(depth, '--E parsing map------------')

        return abs_map_val


    def _parse_str_value(self, con_str_val, depth):
        '''
        Parse concrete value `con_str_val` that is a string.

        Number `depth` measures the recursion depth.

        :return: an abstract variant of `con_str_val`.

        :raise: a :py:exc:`InterpolDictError` if parsing fails.
        '''
        str_reader = StrValueParser(**self.syntax)
        abs_str_val = str_reader.parse_value(con_str_val, depth)

        return abs_str_val


class StrValueParser(object):
    '''
    Parser to transform concrete string values into abstract ones.

    The keyword arguments that can be given here as `**syntax` controls the
    concrete syntax for value references.  For a description of these syntax
    parameters please consult the documentation of :py:class:`InterpolDict`.
    '''
    def __init__(self, **syntax):
        '''
        Initialize a string value parser.
        '''
        self.syntax = syntax

        # List of all reference delimiters.
        refs_delims = [
            s for (c, s) in self.syntax.items() if not c.endswith('_sep')]

        # - Regexp to search for reference delimiters.
        refs_delims_rx = '|'.join([re.escape(d) for d in refs_delims])
        self._refs_delims_rx = re.compile(refs_delims_rx)

        # Regexp to match either an reference delimiter or an argument
        # separator in function call references, if function call reference is
        # enabled.
        if 'arg_sep' in self.syntax:
            # - Regexp to match an argument separator.
            args_sep_rx = r'\s*'
            args_sep_rx += re.escape(self.syntax['arg_sep'])
            args_sep_rx += r'\s*'
            # - Put everything together.
            fct_call_arg_rx = args_sep_rx
            fct_call_arg_rx += f"|{refs_delims_rx}"
            self._fct_call_arg_rx = re.compile(fct_call_arg_rx)

        # Syntax parameters strings mapped to their parameter names used as
        # kinds for these parameters.
        self._syntax_kinds = {s: c for (c, s) in self.syntax.items()}


    def parse_value(self, con_val, depth=0):
        '''
        Parse concrete string value `con_val`.

        Number `depth` measures the recursion depth.

        :return: an abstract variant of `con_val`.

        :raise: a :py:exc:`InterpolDictError` if parsing fails.
        '''
        recursive_debug_log(depth, '--B parsing string---------')
        pretty_value_debug_log(con_val)
        if self.syntax == {}:
            # No value interpolation is used at all -> make a single abstract
            # string value from the given concrete string value.
            abs_val = AbstractStr(con_val)
        else:
            # Value interpolation is used -> parse the concrete value.

            # - Initialize the parsing state.
            proc_state = {
                # -- Save the concrete value for parsing.
                'con_val': con_val,
                # -- List of reference begin delimiter tokens yet without
                #    matching end delimiters.
                'open_delim_toks': [],
                # -- Cursor for the part of the concrete string value whose
                #    parsing is already done.
                'done_pos': 0,
                # -- Abstract value from parsing of the part of the concrete
                #    string value whose parsing is already done.
                'done_val': None
            }

            # - Iterate over all reference delimiter strings in the concrete
            #   string value.
            for delim_match in re.finditer(self._refs_delims_rx, con_val):
                # Process the delimiter token for the current reference
                # delimiter match.
                self._process_ref_delim_match(delim_match, proc_state, depth)

            # - After processing all reference delimiters tokens in the
            #   concrete string no reference begin delimiter tokens without
            #   opposite reference end delimiter tokens should be left.
            open_delim_toks = proc_state['open_delim_toks']
            if len(open_delim_toks) > 0:
                raise MissingRefEndsError(
                    open_delim_toks, self.syntax, val=con_val)

            # - Process the remaining rest after the last reference
            #   delimiter.
            last_tok = self._calculate_StrToken(len(con_val), proc_state)
            self._process_str(last_tok, proc_state, add_empty=True)

            # - Now `con_val` is completely parsed.
            abs_val = proc_state['done_val']

        recursive_debug_log(depth, '--E parsing string---------')

        return abs_val


    def _process_ref_delim_match(self, curr_delim_match, proc_state, depth):
        '''
        Process reference delimiter for match `curr_delim_match` from within
        the given concrete string value with current parsing state
        `proc_state`.

        Number `depth` measures the recursion depth.

        :raise: a :py:exc:`InterpolDictError` if processing fails.
        '''
        # Make a token for the current delimiter.
        curr_delim_tok = self._calculate_SyntaxToken(curr_delim_match)
        recursive_debug_log(depth, '---B processing delimiter--')
        pretty_value_debug_log(curr_delim_tok)
        recursive_debug_log(depth, '---------------------------')

        if curr_delim_tok.is_beg_token():
            # The current ref delimiter is a ref beg delimiter.
            recursive_debug_log(depth, '--->Ref beg delimiter.')
            self._process_beg_ref_delim(curr_delim_tok, proc_state)
        elif curr_delim_tok.is_end_token():
            # The current ref delimiter is a ref end delimiter.
            recursive_debug_log(depth, '--->Ref beg delimiter.')
            self._process_end_ref_delim(curr_delim_tok, proc_state, depth)

        recursive_debug_log(depth, '---Process state-----------')
        pretty_value_debug_log(proc_state)
        recursive_debug_log(depth, '---E processing delimiter--')


    def _process_beg_ref_delim(self, beg_delim_tok, proc_state):
        '''
        Process reference begin delimiter token `beg_delim_tok` from within
        the given concrete string value with current parsing state
        `proc_state`.

        :raise: a :py:exc:`InterpolDictError` if processing fails.
        '''
        open_delim_toks = proc_state['open_delim_toks']
        if len(open_delim_toks) == 0:
            # There are no open ref beg delimiters -> add given ref beg
            # delimiter token to list of open ref beg delimiter tokens.
            open_delim_toks.append(beg_delim_tok)
        else:
            # There are open ref beg delimiters.
            last_delim_tok = open_delim_toks[-1]
            last_delim_type = last_delim_tok.reference_type()
            if last_delim_type == 'fct':
                # Last ref delimiter token is a function call ref delimiter
                # token -> add current ref delimiter token to list of open ref
                # beg delimiters tokens.
                open_delim_toks.append(beg_delim_tok)
            elif last_delim_type == 'key':
                # Last ref delimiter token is a key ref delimiter token ->
                # nested key ref error.
                con_val = proc_state['con_val']
                raise NestedKeyRefError(
                    last_delim_tok, beg_delim_tok, self.syntax, val=con_val)


    def _process_end_ref_delim(self, end_delim_tok, proc_state, depth):
        '''
        Process reference end delimiter token `end_delim_tok` from within
        the given concrete string value with current parsing state
        `proc_state`.

        Number `depth` measures the recursion depth.

        :raise: a :py:exc:`InterpolDictError` if processing fails.
        '''
        open_delim_toks = proc_state['open_delim_toks']
        if len(open_delim_toks) == 0:
            # There are no open ref beg delimiters -> a ref end delimiter
            # token has no opposite ref beg delimiter token.
            con_val = proc_state['con_val']
            raise MissingRefBegError(
                end_delim_tok, self.syntax, val=con_val)
        else:
            # There are open ref beg delimiters.
            last_delim_tok = open_delim_toks[-1]
            last_delim_type = last_delim_tok.reference_type()
            curr_delim_type = end_delim_tok.reference_type()
            con_val = proc_state['con_val']
            if not last_delim_type == curr_delim_type:
                # Ref delimiter types are different -> mismatch error.
                raise MismatchRefError(
                    last_delim_tok, end_delim_tok, self.syntax, val=con_val)
            elif curr_delim_type == 'key':
                # Ref delimiters are opposite key ref delimiters -> new key
                # reference found.
                key_ref_tok = open_delim_toks.pop()
                recursive_debug_log(
                    depth, '--->Key reference beg closed:')
                pretty_value_debug_log(key_ref_tok)
                # Key references cannot be nested itself, but nested within
                # function call references -> it is only stored if a
                # non-nested one.
                if len(open_delim_toks) == 0:
                    self._process_key_ref(
                        last_delim_tok, end_delim_tok, proc_state, depth)
            elif curr_delim_type == 'fct':
                # Ref delimiters are opposite function call ref delimiters ->
                # new function call reference found.
                fct_ref_tok = open_delim_toks.pop()
                recursive_debug_log(
                    depth, '--->Function call beg reference closed:')
                pretty_value_debug_log(fct_ref_tok)
                # Function call references can be nested -> it is only stored
                # if a non-nested one.
                if len(open_delim_toks) == 0:
                    self._process_fct_call_ref(
                        last_delim_tok, end_delim_tok, proc_state, depth)


    def _process_key_ref(self, beg_delim_tok, end_delim_tok, proc_state,
                         depth):
        '''
        Process the key reference string from within the given concrete string
        value delimited by key reference begin / end delimiters given by tokens
        `beg_delim_tok` / `end_delim_tok` with current parsing state
        `proc_state`.

        Number `depth` measures the recursion depth.
        '''
        recursive_debug_log(depth, '----B processing KeyRef------')
        pretty_value_debug_log(beg_delim_tok)
        recursive_debug_log(depth, '-----------------------------')
        pretty_value_debug_log(end_delim_tok)

        # Process the string before the ref begin delimiter.
        end_sub = beg_delim_tok.beg
        before_tok = self._calculate_StrToken(end_sub, proc_state)
        self._process_str(before_tok, proc_state, add_empty=False)

        # Process the key reference string.
        beg_ref = beg_delim_tok.end
        end_ref = end_delim_tok.beg
        con_val = proc_state['con_val']
        ref_str = con_val[beg_ref:end_ref]
        recursive_debug_log(depth, f"---->Ref string:\n  {ref_str}")
        keys = ref_str.split(self.syntax['key_sep'])
        recursive_debug_log(depth, f"---->Key sequence:\n  {keys}")
        abs_ref = AbstractKeyRef(keys)
        self._extend_done_value(abs_ref, proc_state)

        # Update position for done part.
        proc_state['done_pos'] = end_delim_tok.end


    def _process_fct_call_ref(self, beg_delim_tok, end_delim_tok, proc_state,
                              depth):
        '''
        Process the function call reference string from within the given
        concrete string value delimited by function call reference begin / end
        delimiters given by token `beg_delim_tok` / `end_delim_tok` with
        current parsing state `proc_state`.

        Number `depth` measures the recursion depth.
        '''
        recursive_debug_log(depth, '----B processing FctCallRef--')
        pretty_value_debug_log(beg_delim_tok)
        recursive_debug_log(depth, '-----------------------------')
        pretty_value_debug_log(end_delim_tok)

        # Process the string before the ref begin delimiter.
        end_sub = beg_delim_tok.beg
        before_tok = self._calculate_StrToken(end_sub, proc_state)
        self._process_str(before_tok, proc_state, add_empty=False)

        # Process the function call reference string.
        beg_ref = beg_delim_tok.end
        end_ref = end_delim_tok.beg
        con_val = proc_state['con_val']
        ref_str = con_val[beg_ref:end_ref]
        recursive_debug_log(depth, f"---->Ref string:\n  {ref_str}")
        abs_ref = self._parse_fct_call_ref(ref_str, depth)
        self._extend_done_value(abs_ref, proc_state)

        # Update position for done part.
        proc_state['done_pos'] = end_delim_tok.end


    def _process_str(self, str_tok, proc_state, add_empty=True):
        '''
        Process the string token with current parsing state `proc_state`.

        For a ``True`` switch `add_empty` even an empty abstract string value
        is added; otherwise such an abstract value is skipped.
        '''
        # Process the string token.
        if add_empty or len(str_tok.content) > 0:
            abs_str = AbstractStr(str_tok.content)
            self._extend_done_value(abs_str, proc_state)

        # Update position for done part.
        proc_state['done_pos'] = str_tok.end


    def _calculate_SyntaxToken(self, syntax_match):
        '''
        Calculate syntax token for match `syntax_match` for a concrete string
        representing such a token.

        :return: syntax token for `syntax_match`.
        '''
        syntax_str = syntax_match.group()
        # Whitespace around syntax parameter strings is ignored.
        syntax_str_no_ws = syntax_str.strip()
        syntax_kind = self._syntax_kinds[syntax_str_no_ws]
        syntax_tok = SyntaxToken(
            syntax_match.start(), syntax_match.end(), syntax_kind)

        return syntax_tok


    @staticmethod
    def _calculate_StrToken(end, proc_state):
        '''
        Calculate the string token for the concrete string and from the done
        position of current parsing state `proc_state` to end position `end`.

        :return: the calculated string token.
        '''
        beg = proc_state['done_pos']
        content = proc_state['con_val'][beg:end]

        return StrToken(beg, end, content)


    def _extend_done_value(self, abs_val, proc_state):
        '''
        Extend the done abstract part of the concrete string value by abstract
        value `abs_val` with current parsing state `proc_state`.

        :raise: a :py:exc:`InterpolDictError` if extension has failed.
        '''
        done_val = proc_state['done_val']
        if done_val is None:
            # No done part yet -> start with `abs_val` anew.
            proc_state['done_val'] = abs_val
        elif isinstance(abs_val, AbstractStr) and abs_val.parts == ['']:
            # If there is an already done part, and `abs_val` is an empty
            # abstract string value -> ignore it.
            pass
        elif isinstance(done_val, AbstractValue):
            # If there is already a done part, and `abs_val` is a non-trivial
            # abstract value -> try to add the latter to the former.
            proc_state['done_val'] = done_val + abs_val


    def _parse_fct_call_ref(self, fct_call_ref_str, depth):
        '''
        Parse function call reference given by string `fct_call_ref_str` for
        its content.

        Number `depth` measures the recursion depth.

        :return: a py:class:`AbstractFctCallRef` for `fct_call_ref_str`.

        :raise: a :py:exc:`InterpolDictError` if parsing of one if the
        concrete argument strings in `fct_call_ref_str` fails.
        '''
        # First we split the content string into the parts for a function
        # call.
        # - Split the function name from the arguments string, if there any
        #   arguments.
        fct_call_match = re.search(r'\s+', fct_call_ref_str)
        if fct_call_match is None:
            # No whitespace found -> call of a function without arguments.
            fct_name = fct_call_ref_str
            args_str = None
        else:
            # Whitespace found -> the function name is everything before the
            # whitespace, and the rest is the argument string.
            fct_name = fct_call_ref_str[:fct_call_match.start()]
            args_str = fct_call_ref_str[fct_call_match.end():]
        recursive_debug_log(depth, f"---->Function name:\n  {fct_name}")
        # - Split the arguments string into argument strings, and parse any of
        #   them in turn, if there any arguments at all.
        if args_str is not None:
            args_strs = self._split_fct_call_args(args_str)
            recursive_debug_log(depth, '---->Argument strings:')
            pretty_value_debug_log(args_strs)
            abs_args = [self.parse_value(a, depth + 1) for a in args_strs]
        else:
            abs_args = []

        abs_fct_ref = AbstractFctCallRef(fct_name, abs_args)

        return abs_fct_ref


    def _split_fct_call_args(self, args_str):
        '''
        Split the argument string `args_str` of a function call reference into
        arguments.

        Number `depth` measures the recursion depth.

        :return: list of argument strings from `args_str`.
        '''
        # Parse argument string for valid argument separator tokens,
        # i.e. those argument separator tokens that are neither within a key
        # reference or another nested function call reference.  For this we
        # keep track of the open reference begin delimiters.  Since it is
        # already ensured (by :py:meth:`_process_ref_delim_match` and
        # companions) that reference begin and end delimiters are pairwise
        # matching, we need only to count to open reference begin delimiters.
        # - Number of open ref beg delimiters.
        nr_open_delims = 0
        # - List of valid argument separator tokens.
        args_sep_toks = []
        for m in re.finditer(self._fct_call_arg_rx, args_str):
            tok_m = self._calculate_SyntaxToken(m)
            if tok_m.is_beg_token():
                nr_open_delims += 1
            elif tok_m.is_end_token():
                nr_open_delims -= 1
            elif tok_m.is_sep_token() and nr_open_delims == 0:
                args_sep_toks.append(tok_m)

        # Split the argument string at the valid argument separator tokens.
        # - Cursor within the argument string.
        done_pos = 0
        # - List of argument strings.
        args_strs = []
        for t in args_sep_toks:
            args_strs.append(args_str[done_pos:t.beg])
            done_pos = t.end
        if done_pos < len(args_str):
            args_strs.append(args_str[done_pos:])

        return args_strs


class ValueFormatter(object):
    '''
    Formatter to transform :py:class:`AbstractValue`s into concrete values.

    The keyword arguments that can be given here as `**syntax` controls the
    concrete syntax for value references.  For a description of these syntax
    parameters please consult the documentation of :py:class:`InterpolDict`.
    '''
    def __init__(self, **syntax):
        '''
        Initialize a value formatter.
        '''
        self.syntax = syntax


    def format_value(self, abs_val):
        '''
        Format abstract value `abs_val` as concrete value.

        :return: a concrete variant of `abs_val`.

        :raise: a :py:exc:`InterpolDictError` if formatting fails.
        '''
        debug_log('-Abstract value---------------------')
        pretty_value_debug_log(abs_val)
        debug_log('------------------------------------')

        # We distinguish the different types of abstract values.
        if isinstance(abs_val, colls.abc.Sequence):
            debug_log('->Sequence value.')
            con_val = self._format_seq_value(abs_val)
        elif isinstance(abs_val, colls.abc.Mapping):
            debug_log('->Map value.')
            con_val = self._format_map_value(abs_val)
        elif isinstance(abs_val, AbstractValue):
            debug_log('->Abstract value.')
            con_val = self._format_AbstractValue(abs_val)
        else:
            # Other types of abstract values are unknown.
            raise InterpolDictError('No known formatting', val=abs_val)

        debug_log('-Concrete value---------------------')
        pretty_value_debug_log(con_val)
        debug_log('------------------------------------')

        return con_val


    def _format_seq_value(self, abs_seq_val):
        '''
        Format abstract sequence value `abs_seq_val`.

        :return: a concrete variant of `abs_seq_val`

        :raise: a :py:exc:`InterpolDictError` if formatting fails.
        '''
        # For an abstract sequence value `abs_seq_val` we format the elements
        # of the sequence: the formatted sequence has the same number of the
        # elements like `abs_seq_val`, and the value for index ``i`` in the
        # former is the formatted value for ``i`` in `abs_seq_val`.
        debug_log('--B formatting sequence------------')
        pretty_value_debug_log(abs_seq_val)
        con_seq_val = [self.format_value(e) for e in abs_seq_val]
        debug_log('--E formatting sequence------------')

        return con_seq_val


    def _format_map_value(self, abs_map_val):
        '''
        Format abstract map value `abs_map_val`.

        :return: a concrete variant of `abs_map_val`

        :raise: a :py:exc:`InterpolDictError` if formatting fails.
        '''
        # For an abstract mapping value `abs_map_val` we format the values of
        # the mapping: the formatted dictionary has the same keys like
        # `abs_map_val`, and the value for key ``k`` in the former is the
        # formatting of the value for ``k`` in `abs_map_val`.
        debug_log('--B formatting map-----------------')
        pretty_value_debug_log(abs_map_val)
        con_map_val = {
            k: self.format_value(v) for (k, v) in abs_map_val.items()}
        debug_log('--E formatting map-----------------')

        return con_map_val


    def _format_AbstractValue(self, abs_val):
        '''
        Format a :py:class:`AbstractValue` `abs_val`.

        :return: a concrete variant of `abs_val`

        :raise: a :py:exc:`InterpolDictError` if formatting fails.
        '''
        # We distinguish the different types of py:class:`AbstractValue` we
        # know.
        if isinstance(abs_val, AbstractStr):
            debug_log('-->String value.')
            con_val = self._format_AbstractStr(abs_val)
        elif isinstance(abs_val, AbstractKeyRef):
            debug_log('-->Key reference value.')
            con_val = self._format_AbstractKeyRef(abs_val)
        elif isinstance(abs_val, AbstractFctCallRef):
            debug_log('-->Function call reference value.')
            con_val = self._format_AbstractFctCallRef(abs_val)
        else:
            # Other types of abstract values are unknown.
            raise InterpolDictError('No known formatting', val=abs_val)

        return con_val


    def _format_AbstractStr(self, abs_val):
        '''
        Format a :py:class:`AbstractStr` object `abs_val`.

        :return: a concrete variant of `abs_val`.

        :raise: a :py:exc:`InterpolDictError` if formatting fails.
        '''
        debug_log('---B formatting AbstractStr--------')
        # Format every part of the abstract string, and concatenate the
        # results.
        con_str = ''
        for p in abs_val.parts:
            if isinstance(p, AbstractValue):
                debug_log(f"--->Add formatted abstract value\n  {p}")
                con_str_p = self._format_AbstractValue(p)
                debug_log(f" with concrete value\n  {con_str_p}")
                con_str += con_str_p
            elif isinstance(p, str):
                debug_log(f"--->Add concrete string\n  {p}")
                con_str += p
            else:
                raise InterpolDictError(
                    f"Unknown AbstractStr part type for\n  {p}", val=abs_val)
        debug_log('---E formatting AbstractStr--------')

        return con_str


    def _format_AbstractKeyRef(self, abs_val):
        '''
        Format a :py:class:`AbstractKeyRef` object `abs_val`.

        :return: a concrete variant of `abs_val`.

        :raise: a :py:exc:`InterpolDictError` if formatting fails.
        '''
        debug_log('---B formatting AbstractKeyRef-----')
        # Build the keys sequence string, and surround it with key reference
        # delimiters.
        keys_str = self.syntax['key_sep'].join(abs_val.keys)
        # Concatenate the keys with the key separator in between.
        debug_log(f"--->Keys string '{keys_str}'")
        # Add begin and end delimiters for key references.
        con_key_ref = self.syntax['key_beg']
        con_key_ref += keys_str
        con_key_ref += self.syntax['key_end']
        debug_log('---E formatting AbstractKeyRef-----')

        return con_key_ref


    def _format_AbstractFctCallRef(self, abs_val):
        '''
        Format a :py:class:`AbstractFctCallRef` object `abs_val`.

        :return: a concrete variant of `abs_val`.

        :raise: a :py:exc:`InterpolDictError` if formatting fails.
        '''
        debug_log('---B formatting AbstractFctCallRef-')
        # First we reconstruct the function call string.
        # - Start with the function name.
        fct_call_str = abs_val.name
        # - Add the argument string, if there arguments at all.  Here we have
        #   to format the abstract argument values first.
        if not abs_val.args == []:
            args_strs = [self.format_value(a) for a in abs_val.args]
            debug_log(f"--->Argument strings\n  {args_strs}")
            args_str = f"{self.syntax['arg_sep']} ".join(args_strs)
            fct_call_str += f" {args_str}"
        else:
            debug_log('--->No arguments.')
        debug_log(f"--->Function call string\n  {fct_call_str}")
        # Then the function call string must be surrounded by function call
        # reference delimiters.
        con_fct_call_ref = self.syntax['fct_beg']
        con_fct_call_ref += fct_call_str
        con_fct_call_ref += self.syntax['fct_end']
        debug_log('---E formatting AbstractFctCallRef-')

        return con_fct_call_ref


class ValueToken(object):
    '''
    Token produced by tokenizing concrete string values.  Such a token has a
    begin / end positions `beg` / `end`.
    '''
    # Category for this sort of token.
    category = None


    def __init__(self, beg, end):
        self.beg = beg
        self.end = end


    def __repr__(self):
        cls = self.__class__
        # We need the name of the class.
        cls_name = cls.__name__
        # List of representations for the initialization arguments.
        args_reprs = [repr(v) for v in self.__dict__.values()]
        # Build the string for the initialization arguments.
        args_repr = ', '.join(args_reprs)
        # Put everything together.
        self_repr = f"{cls_name}({args_repr})"

        return self_repr


    def __str__(self):

        return self.__repr__()


    def __eq__(self, obj):
        # Intended ``==``: value token objects are equal if the have the same
        # attributes.
        if not isinstance(obj, ValueToken):
            is_eq = False
        elif not self.category == obj.category:
            is_eq = False
        else:
            is_eq = self.__dict__ == obj.__dict__

        return is_eq


class StrToken(ValueToken):
    '''
    Token produced by tokenizing concrete string values that represents a
    string that is no value reference delimiter.  Such a token has begin / end
    positions `beg` / `end`, and a concrete string `content` that is
    represented.
    '''
    category = 'StrToken'


    def __init__(self, beg, end, content):
        super().__init__(beg, end)

        self.content = content


class SyntaxToken(ValueToken):
    '''
    Token produced by tokenizing concrete string values that represents a
    syntax parameter.  Such a token has begin / end positions `beg` / `end`,
    and a specific syntax parameter kind `kind`.
    '''
    category = 'SyntaxToken'


    def __init__(self, beg, end, kind):
        super().__init__(beg, end)

        self.kind = kind


    def format_for(self, syntax, value=None, indent=2):
        '''
        Format this syntax token as string as string according to syntax
        information `syntax`.

        For a string value `value` the position of the syntax token is shown
        with markers in `value` with an indent of `indent`-many spaces instead
        of printing the position explicitly, if such markers are reasonable
        for `value`.

        :return: this syntax token formatted as string according to syntax
                 information `syntax`.
        '''
        if value is not None and isinstance(value, str):
            # We use the following heuristics to use markers in `value`:
            # + It has a maximal length.
            # + It contains neither ``'\t'``s nor ``'\n'``s.
            has_max_len = len(value) <= MAX_LINE_LENGTH
            is_std_spaced = '\n' not in value and '\t' not in value
            use_markers = has_max_len and is_std_spaced
        else:
            use_markers = False

        if not use_markers:
            syntax_str = syntax[self.kind]
            syntax_msg = f"'{syntax_str}' (from {self.beg} to {self.end})"
        else:
            indent_str = ' ' * indent
            marker_str = (' ' * self.beg) + ('^' * (self.end - self.beg))
            syntax_msg = '\n'
            syntax_msg += indent_str + value + '\n'
            syntax_msg += indent_str + marker_str
            syntax_msg += '\n'

        return syntax_msg


    def _kind_components(self):
        '''
        :return: pair of kind components for this syntax token.
        '''
        kind_comps = self.kind.split('_')

        return kind_comps


    def is_sep_token(self):
        '''
        :return: ``True`` if this syntax token is a separator token.
        '''
        (_, this_comp2) = self._kind_components()

        return (this_comp2 == 'sep')


    def opposite_token_kind(self):
        '''
        :return: kind for the reference delimiter token opposite to this
                 reference delimiter token, i.e. with the same reference type
                 but the opposite direction, or ``None`` if this syntax token
                 is not a reference delimiter token.
        '''
        (this_comp1, this_comp2) = self._kind_components()
        if this_comp2 == 'beg':
            oppo_kind = f"{this_comp1}_end"
        elif this_comp2 == 'end':
            oppo_kind = f"{this_comp1}_beg"
        else:
            oppo_kind = None

        return oppo_kind


    def reference_type(self):
        '''
        :return: type of reference this reference delimiter token is a
                 delimiter for, or ``None`` if this syntax token is not a
                 reference delimiter token.
        '''
        if self.is_sep_token():
            ref_type = None
        else:
            (ref_type, _) = self._kind_components()

        return ref_type


    def is_beg_token(self):
        '''
        :return: `True` iff this syntax token is a reference begin delimiter
        token.
        '''
        (_, this_comp2) = self._kind_components()

        return (this_comp2 == 'beg')


    def is_end_token(self):
        '''
        :return: `True` iff this syntax token is a reference end delimiter
        token.
        '''
        (_, this_comp2) = self._kind_components()

        return (this_comp2 == 'end')


class AbstractValue(object):
    '''
    Base class for abstract values in :py:class:`InterpolDict` dictionaries.
    '''
    # Category for this sort of abstract values.
    category = None


    def __repr__(self):
        # We need the nae of the class.
        cls_name = self.__class__.__name__
        # List of representations for the initialization arguments.
        args_reprs = [repr(v) for v in self.__dict__.values()]
        # Build the string for the initialization arguments.
        args_repr = ', '.join(args_reprs)
        # Put everything together.
        self_repr = f"{cls_name}({args_repr})"

        return self_repr


    def __str__(self):

        return self.__repr__()


    def __eq__(self, obj):
        # Intended ``==``: abstract value objects are equal if the have the
        # same category, and all their components / parts are equal.
        if not isinstance(obj, AbstractValue):
            is_eq = False
        elif not self.category == obj.category:
            is_eq = False
        else:
            is_eq = self.__dict__ == obj.__dict__

        return is_eq


    def __add__(self, obj):
        '''
        Add to this :py:class:`AbstractValue` a suitable object `obj`.

        :return: result of adding `obj` to this very abstract value.

        :raise: an :py:exc:`InterpolDictError` if the addition fails.
        '''
        self_AbstractStr = AbstractStr(self)
        self_AbstractStr += obj

        return self_AbstractStr


class AbstractStr(AbstractValue):
    '''
    Abstract string value consisting of parts in list `parts`.  It can be
    initialized either by another :py:class:`AbstractStr` `obj`, or by an
    object `obj` that can transformed into a part or parts of a
    :py:class:`AbstractStr`.
    '''
    category = 'Str'


    def __init__(self, obj):
        if isinstance(obj, AbstractStr):
            # `obj` is an abstract string itself -> create a copy of its parts.
            self.parts = obj.parts.copy()
        elif isinstance(obj, AbstractValue):
            # `obj` is another abstract value -> take it as single part.
            self.parts = [obj]
        elif isinstance(obj, str):
            # `obj` is a string -> take it as single part.
            self.parts = [obj]
        elif isinstance(obj, colls.abc.Sequence):
            # `obj` is another sequence -> try use every element of `obj` to
            # as another part.
            self.parts = []
            for e in obj:
                self += e
        else:
            # Otherwise an error is raised.
            raise InterpolDictError(
                f"Cannot initialize an abstract string from object\n  {obj}")


    def __iadd__(self, obj):
        '''
        Extend this :py:class:`AbstractStr` in-place by object `obj` being
        converted to a :py:class:`AbstractStr`, too.

        :return: the extended abstract string value.

        :raise: an :py:exc:`InterpolDictError` if the extension fails.
        '''
        # We try to transform `obj` into an abstract string value, and extend
        # the parts of this abstract string value with the parts of the latter.
        obj_AbstractStr = AbstractStr(obj)
        self.parts.extend(obj_AbstractStr.parts)

        return self


class AbstractKeyRef(AbstractValue):
    '''
    Abstract key reference value using the key sequence `keys`.
    '''
    category = 'KeyRef'


    def __init__(self, keys):
        self.keys = keys


class AbstractFctCallRef(AbstractValue):
    '''
    Abstract function call reference value using function with name `name` and
    list `args` of abstract value arguments.
    '''
    category = 'FctCallRef'


    def __init__(self, name, args):
        self.name = name
        self.args = args


class InterpolDictError(Exception):
    '''
    Errors specific for :py:class:`InterpolDict` objects with corresponding
    message `msg` and an optional location information in `kwargs`:
    + Keyword argument `val` with not-``None`` value gives a specific location
      information for this value.
    + A keyword argument `loc` with not-``None`` value gives an unspecific
      location information.
    '''
    def __init__(self, msg, **kwargs):
        '''
        Initialize a :py:class:`InterpolDictError` with corresponding message
        `msg` and an optional location information `loc`.
        '''
        self.msg = msg
        self.val = kwargs.get('val')
        if self.val is not None:
            self.loc = f"value '{self.val}'"
        else:
            self.loc = kwargs.get('loc')


    def __str__(self):
        '''
        :return: printable string representation of the exception.
        '''
        # Since we want to have full control whether the printable string for
        # the exception ends with a ``'.'`` or not we ensure first that there
        # is no ``'.'`` at the end.
        if self.msg.endswith('.'):
            ex_str = self.msg[:-1]
        else:
            ex_str = self.msg
        # When a location is given, we start a new line for it, but do not
        # want a ``'.'`` at the end.  Otherwise we ensure one.
        if self.loc is not None:
            ex_str += f" for:\n  {self.loc}"
        else:
            ex_str += '.'

        return ex_str


class MissingRefBegError(InterpolDictError):
    '''
    A specific error when parsing a concrete string value:

    Missing reference begin delimiter for a reference end delimiter token
    `end_tok` according to syntax information `syntax`.

    Location information can be given in `kwargs`.  See the documentation of
    :py:exc:`InterpolDictError` for details.
    '''
    def __init__(self, end_tok, syntax, **kwargs):
        end_tok_msg = end_tok.format_for(syntax, value=kwargs.get('val'))
        beg_delim_kind = end_tok.opposite_token_kind()
        beg_delim_str = syntax[beg_delim_kind]
        msg = f"Missing begin delimiter '{beg_delim_str}' for "
        msg += f"end delimiter {end_tok_msg}"

        super().__init__(msg, **kwargs)


class MissingRefEndsError(InterpolDictError):
    '''
    A specific error when parsing a concrete string value:

    Missing reference end delimiters for reference begin delimiter tokens
    'beg_toks` according to syntax information `syntax`.

    Location information can be given in `kwargs`.  See the documentation of
    :py:exc:`InterpolDictError` for details.
    '''
    def __init__(self, beg_toks, syntax, **kwargs):
        msg = 'Missing end delimiter(s):'
        for beg_tok in beg_toks:
            beg_tok_msg = beg_tok.format_for(syntax, value=kwargs.get('val'))
            end_delim_kind = beg_tok.opposite_token_kind()
            end_delim_str = syntax[end_delim_kind]
            msg += f"\n- '{end_delim_str}' for {beg_tok_msg}"

        super().__init__(msg, **kwargs)


class MismatchRefError(InterpolDictError):
    '''
    A specific error when parsing a concrete string value:

    Reference begin delimiter `beg_tok` and other reference delimiter
    `oth_tok` mismatch according to syntax information `syntax`.

    Location information can be given in `kwargs`.  See the documentation of
    :py:exc:`InterpolDictError` for details.
    '''
    def __init__(self, beg_tok, oth_tok, syntax, **kwargs):
        beg_tok_msg = beg_tok.format_for(syntax, value=kwargs.get('val'))
        oth_tok_msg = oth_tok.format_for(syntax, value=kwargs.get('val'))
        end_delim_kind = beg_tok.opposite_token_kind()
        end_delim_str = syntax[end_delim_kind]
        msg = f"Begin delimiter {beg_tok_msg} does not match "
        msg += f"value reference delimiter {oth_tok_msg} "
        msg += f"An end delimiter '{end_delim_str}' is expected instead."

        super().__init__(msg, **kwargs)


class NestedKeyRefError(InterpolDictError):
    '''
    A specific error when parsing a concrete string value:

    Within a key reference with reference begin delimiter token `key_beg_tok`
    another reference begin delimiter token `beg_tok` is used according to
    syntax information `syntax`.

    Location information can be given in `kwargs`.  See the documentation of
    :py:exc:`InterpolDictError` for details.
    '''
    def __init__(self, key_beg_tok, beg_tok, syntax, **kwargs):
        key_beg_tok_msg = key_beg_tok.format_for(
            syntax, value=kwargs.get('val'))
        key_end_kind = key_beg_tok.opposite_token_kind()
        key_end_delim_str = syntax[key_end_kind]
        beg_tok_msg = beg_tok.format_for(syntax, value=kwargs.get('val'))
        msg = f"Key reference begin delimiter {key_beg_tok_msg} "
        msg += f"needs an end delimiter '{key_end_delim_str}' instead of "
        msg += f"delimiter {beg_tok_msg}"

        super().__init__(msg, **kwargs)


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
