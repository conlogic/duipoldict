..
 =============================================================================
 Title          : A dictionary with value interpolation

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2021-09-11

 Description    : Read me file for the package.
 =============================================================================


=====================================================
``duipoldict``: A dictionary with value interpolation
=====================================================

This package provides a user dictionary :py:class:`InterpolDict` whose values
permit interpolation.  By the term "value interpolation" is meant that within
values one can use references to other values; and during interpolation such
value references are replaced by the values they refer to.

Two forms of such value references can be used:

+ Key references: a key reference is referring to the value nested within the
  dictionary navigated to by the sequence of referred keys.  An interpolation
  that uses only key references will be called "item interpolation" since the
  value that replaces a key reference during interpolation stems from an item
  of a dictionary object.

+ Function call references: a function call reference refers to the value of
  the respective function call.  An interpolation that uses at least one
  function call reference will be called "function call interpolation".

The form of value interpolation implemented here is inspired by the value
interpolation for :py:class:`configparser.ConfigParser` objects from the
Python Standard Library.  Compared to their implementation, our one is more
general and configurable.


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
