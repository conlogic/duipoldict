..
 =============================================================================
 Title          : A dictionary with value interpolation

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2021-12-29

 Description    : Change log for the package.
 =============================================================================


=========
Changelog
=========


Version 0.5.3 (2021-12-29)
==========================

- Fix some glitches in the usage documentation.

- Fix some glitches in the :py:class:`InterpolDict` documentation.

- Add tests for :py:meth:`format_value` errors.


Version 0.5.2 (2021-12-27)
==========================

- Add support for Python 3.10.


Version 0.5.1 (2021-10-01)
==========================

- Fix for mismatch error between function arity and number of arguments in
  function call references: now the list of raw concrete arguments is shown.


Version 0.5 (2021-09-29)
========================

- More tests for wrong function call references.

- Use an py:exc:`InterpolDictError` for a wrong number of arguments for the
  called function in a function call reference.

- Fix a bug in ``setup.cfg``.


Version 0.4 (2021-09-28)
========================

- Proof-read both code documentation, i.e. comments and doc strings, and
  Sphinx documentation for this package.

- (Re-)Generate all test data files, and complete a test for
  :py:meth:`parse_value`.

- Make :py:class:`StrValueParser` a standalone class.

- Make recursion depth logic consistent both for parsing and expanding values.

- Restructure the processing methods for parsing string values.


Version 0.3 (2021-09-27)
========================

- Add to :py:exc:`InterpolDictError` and all errors inheriting from it a way
  to provide location info by affected value.

- Add test tool ``show_exceptions.py`` to show (the messages of) some raised
  :py:exc:`InterpolDictError` exceptions.

- Use markers to show the position of value reference delimiters in errors, if
  reasonable.

- Use values for location info in :py:exc:`InterpolDictError`-related errors
  wherever possible, and use concrete values whenever possible.


Version 0.2 (2021-09-26)
========================

- Add tests for more :py:class:`InterpolDict` methods.

- Use :py:exc:`RecursionError` to detect loops during value expansion.

- Simplify and fix the recursion depth logic for value expansion, and use a
  different character to visualize recursion depth in debug logging messages.

- Add recursion depth logic for value parsing to visualize recursion depth in
  debug logging messages, and add more debug logging for value parsing.

- Improve terminology: abstract building blocks for string values are
  :py:class:`AbstractValue` objects.

- Use specific objects based on re-done :py:class:`ValueToken` class for the
  parts of concrete strings got in their tokenization.

- Fix bug when parsing the argument string in nested function call
  references.

- Fix some typos in the ``CHANGELOG`` file.


Version 0.1.1 (2021-09-12)
==========================

- Make formatting of the documentation more consistent.


Version 0.1 (2021-09-12)
========================

- Add useful documentation how to use the package.

- Improve terminology: the process to replace in a raw value all value
  references by the values referred to is called "value expansion" instead of
  "value interpolation".  Term "value interpolation" now only names a concrete
  replacement of a value reference by the value referred to.

- Move syntax parameter initialization to :py:class:`InterpolDict`, and make
  both :py:class:`ValueParser` and :py:class:`ValueFormatter` standalone.


Version 0.1dev5 (2021-09-07)
============================

- Finer-grained control of debug logging: provide variants of the value
  parsing and value formatting methods for :py:class:`InterpolDict` where
  debugging can be temporarily disabled.

- Performance optimization for :py:meth:`get`: avoid unnecessary parsing of
  the value.

- Isolate calculation of the components of a key as own function
  :py:func:`calc_key_components` for reusing in packages based on
  ``duipoldict``.


Version 0.1dev4 (2021-09-07)
============================

- Add method :py:meth:`get` to :py:class:`InterpolDict` to get a value for
  given key.

- Add standard mapping method :py:meth:`__getitem__` to
  :py:class:`InterpolDict`.

- Another clarification of value-related terminology:

  - For value variants: we have raw versus interpolated values, and raw values
    are either concrete or abstract.

  - Tokenization is, more accurately, called "parsing"; therefore
    :py:class:`ValueTokenizer` is renamed to :py:class:`ValueParser`.

  - Instead of a `tok` keyword argument for several methods to cause abstract
    (formerly: tokenized) values, there is now a `con` keyword argument with
    the opposite meaning (i.e. to cause concrete values).

- Require version >= 1.3 of dependency ``dubasiclog`` for improved debugging.

- Improve debug logging messages by harmonizing them and add pretty-printing
  of values where it is useful.


Version 0.1dev3 (2021-09-06)
============================

- Rename :py:class:`duipoldict` to :py:class:`InterpolDict`.

- Rename :py:class:`InterplationError` to :py:class:`InterpolDictError`.

- Add keyword arguments `raw` and `tok` for :py:class:`InterpolDict`
  initialization to control the default variant for values.

- Use the default value variant for :py:meth:`write_dict`, and rename its
  corresponding keyword argument from `tokenized` to `tok`.

- Improve code for the tests.


Version 0.1dev2 (2021-09-05)
============================

- Add value formatting by :py:class:`ValueFormatter`, including tests.

- Add a method :py:meth:`read_dict` to read data from a mapping.

- Add a method :py:meth:`write_dict` to write data as dictionary.

- Use formatted values for error messages.

- Clarify terminology: distinguish raw (i.e. non-interpolated) from concrete
  (i.e. non-tokenized) values.


Version 0.1dev1.1 (2021-08-27)
==============================

- Proper handling of depths measures for value interpolation.

- Use a more sensible value for maximal value lookup depth.


Version 0.1dev1 (2021-08-27)
============================

- Initial version, based on code of former package ``dumapinterpol``, and
  meant to be replacement for it.

- For now it is implemented:

  - (a first version of) value tokenization in :py:class:`ValueTokenizer`,
    including tests.

  - User dictionary :py:class:`duipoldict` with (a first version of) value
    interpolation, that is based on value tokenization.  Most of the tests
    needed for :py:class:`duipoldict` are missing yet.


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
